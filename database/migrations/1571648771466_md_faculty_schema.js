'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdFacultySchema extends Schema {
  up () {
    this.create('md_faculties', (table) => {
      table.uuid('id').primary()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_faculties')
  }
}

module.exports = MdFacultySchema
