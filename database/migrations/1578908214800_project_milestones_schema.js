'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestonesSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE project_milestones
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('project_milestones', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectMilestonesSchema
