'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserBankAccountsSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_bank_accounts
    MODIFY COLUMN bank_address LONGTEXT`)
  }

  down () {
    this.table('user_bank_accounts', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserBankAccountsSchema
