'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherExperienceSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_researcher_experiences
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('user_researcher_experiences', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserResearcherExperienceSchema
