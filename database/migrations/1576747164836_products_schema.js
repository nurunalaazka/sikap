'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE products ADD COLUMN 
    required_fund BIGINT`)
  }

  down () {
    this.raw(`ALTER TABLE products DROP COLUMN 
    required_fund`)
  }
}

module.exports = ProductsSchema
