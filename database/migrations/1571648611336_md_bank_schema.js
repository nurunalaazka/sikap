'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdBankSchema extends Schema {
  up () {
    this.create('md_banks', (table) => {
      table.uuid('id').primary()
      table.string('short_name').unique()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_banks')
  }
}

module.exports = MdBankSchema
