'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdRoleSchema extends Schema {
  up () {
    this.create('md_roles', (table) => {
      table.string('id').primary()
      table.string('name').unique()
      table.string('type')      
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_roles')
  }
}

module.exports = MdRoleSchema
