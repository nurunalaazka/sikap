'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResearchNeedSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE research_needs
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('research_needs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ResearchNeedSchema
