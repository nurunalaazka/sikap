'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    // this.raw(`CREATE VIEW users_view
    //   AS SELECT id, name, username, email, password, role_id, type, status, 
    //     verified_at, created_by, modified_by, created_at, updated_at
    //     FROM users WHERE status <> 'inactive'`)
  }

  down () {
    // this.raw(`
    // DROP VIEW users_view`)
  }
}

module.exports = UserSchema
