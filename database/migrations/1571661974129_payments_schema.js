'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsSchema extends Schema {
  up () {
    this.raw("ALTER TABLE payments ADD CONSTRAINT FK_project_id \
    FOREIGN KEY (project_id) REFERENCES projects(id)");

    this.raw(`ALTER TABLE payments ADD CONSTRAINT FK_milestone 
    FOREIGN KEY (project_milestone_id) REFERENCES project_milestones(id)`)
  }

  down () {
    this.raw(`ALTER TABLE payments
    DROP FOREIGN KEY FK_project_id`)

    this.raw(`ALTER TABLE payments
    DROP FOREIGN KEY FK_milestone`)
  }
}

module.exports = PaymentsSchema
