'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectFundingSchema extends Schema {
  up () {
    this.create('project_fundings', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').references('id').inTable('projects')
      table.uuid('investor_id').nullable().references('id').inTable('users')
      table.uuid('chat_id').nullable()
      table.string('status')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_fundings')
  }
}

module.exports = ProjectFundingSchema
