'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdMajorSchema extends Schema {
  up () {
    this.create('md_majors', (table) => {
      table.uuid('id').primary()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_majors')
  }
}

module.exports = MdMajorSchema
