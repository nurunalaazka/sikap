'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneProgressFileSchema extends Schema {
  up () {
    this.create('project_milestone_progress_files', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').references('id').inTable('projects')
      table.uuid('project_milestone_id').references('id').inTable('project_milestones')
      table.uuid('project_milestone_progress_id')
      table.string('file')
      table.string('type')
      table.integer('size').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_milestone_progress_files')
  }
}

module.exports = ProjectMilestoneProgressFileSchema
