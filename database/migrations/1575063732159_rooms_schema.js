'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoomsSchema extends Schema {
  up () {
    this.create('chat.rooms', (table) => {
      table.uuid('id')
      table.timestamps()
    })

    this.raw(`
      ALTER TABLE chat.rooms ADD CONSTRAINT PK_Rooms PRIMARY KEY (id)
    `)
  }

  down () {
    this.drop('chat.rooms')
  }
}

module.exports = RoomsSchema
