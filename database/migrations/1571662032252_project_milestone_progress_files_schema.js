'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneProgressFilesSchema extends Schema {
  up () {
    this.raw("ALTER TABLE project_milestone_progress_files ADD CONSTRAINT FK_milestone_progress \
    FOREIGN KEY (project_milestone_progress_id) REFERENCES project_milestone_progresses(id)")
  }

  down () {
    this.raw(`ALTER TABLE project_milestone_progress_files
    DROP FOREIGN KEY FK_milestone_progress`)
  }
}

module.exports = ProjectMilestoneProgressFilesSchema
