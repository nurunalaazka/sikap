'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductCollaborationOffersSchema extends Schema {
  up () {
    this.create('product_collaboration_offers', (table) => {
      table.uuid('id').primary()
      table.uuid('product_id').references('id').inTable('products')
      table.uuid('investor_id').references('id').inTable('users')
      table.uuid('chat_id').references('id').inTable('chats')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_collaboration_offers')
  }
}

module.exports = ProductCollaborationOffersSchema
