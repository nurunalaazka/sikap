'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherEducationSchema extends Schema {
  up () {
    this.create('user_researcher_educations', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('university').nullable()
      table.string('faculty').nullable()
      table.string('major').nullable()
      table.string('degree_id').nullable().references('id').inTable('md_degrees')
      table.string('gpa').nullable()
      table.string('max_gpa').nullable()
      table.string('location').nullable()
      table.date('date_start').nullable()
      table.date('date_end').nullable()
      table.string('description').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_researcher_educations')
  }
}

module.exports = UserResearcherEducationSchema
