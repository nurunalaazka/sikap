'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatContentsSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE chat.chat_contents DROP receiver_status`)
  }

  down () {
    this.raw(`ALTER TABLE chat.chat_contents ADD COLUMN receiver_status VARCHAR(20)`)
  }
}

module.exports = ChatContentsSchema
