'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMemberSchema extends Schema {
  up () {
    this.create('project_members', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').nullable().references('id').inTable('projects')
      table.uuid('researcher_id').nullable().references('id').inTable('users')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_members')
  }
}

module.exports = ProjectMemberSchema
