'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.uuid('id').primary()
      table.string('title').unique()
      table.string('sub_title').nullable()
      table.integer('duration').nullable()
      table.string('duration_unit').nullable()
      table.date('date_start').nullable()
      table.uuid('research_field_id').nullable().references('id').inTable('md_research_fields')
      table.uuid('study_area_id').nullable().references('id').inTable('md_study_areas')
      table.string('cover_image').nullable()
      table.string('description').nullable()
      table.uuid('researcher_id').nullable().references('id').inTable('users')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
