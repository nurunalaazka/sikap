'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdDistrictSchema extends Schema {
  up () {
    this.create('md_districts', (table) => {
      table.uuid('id').primary()
      table.uuid('country_id').references('id').inTable('md_countries')
      table.uuid('province_id').references('id').inTable('md_provinces')
      table.uuid('city_id').references('id').inTable('md_cities')
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_districts')
  }
}

module.exports = MdDistrictSchema
