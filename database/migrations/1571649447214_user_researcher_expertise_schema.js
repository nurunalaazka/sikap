'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherExpertiseSchema extends Schema {
  up () {
    this.create('user_researcher_expertises', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('expertise')
      table.integer('value')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_researcher_expertises')
  }
}

module.exports = UserResearcherExpertiseSchema
