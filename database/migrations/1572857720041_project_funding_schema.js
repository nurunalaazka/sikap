'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectFundingSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE project_fundings
    ADD UNIQUE unique_index(project_id, investor_id)`)
  }

  down () {
    this.table('project_fundings', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectFundingSchema
