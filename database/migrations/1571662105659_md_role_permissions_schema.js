'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdRolePermissionsSchema extends Schema {
  up () {
    this.raw("ALTER TABLE md_role_permissions ADD CONSTRAINT FK_permission_id \
    FOREIGN KEY (permission_id) REFERENCES md_permissions(id)")
  }

  down () {
    this.raw(`ALTER TABLE md_role_permissions
    DROP FOREIGN KEY FK_permission_id`)
  }
}

module.exports = MdRolePermissionsSchema
