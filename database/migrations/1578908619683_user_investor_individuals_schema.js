'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserInvestorIndividualsSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_investor_individuals
    MODIFY COLUMN address LONGTEXT`)
  }

  down () {
    this.table('user_investor_individuals', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserInvestorIndividualsSchema
