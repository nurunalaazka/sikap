'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsSchema extends Schema {
  up () {
    this.raw(`
      ALTER TABLE payments 
      ADD UNIQUE unique_project_milestone(project_id, project_milestone_id)
    `)
  }

  down () {
    this.raw(`
      ALTER TABLE payments 
      DROP INDEX unique_project_milestone
    `)
  }
}

module.exports = PaymentsSchema
