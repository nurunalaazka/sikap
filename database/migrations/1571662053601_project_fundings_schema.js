'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectFundingsSchema extends Schema {
  up () {
    this.raw("ALTER TABLE project_fundings ADD CONSTRAINT FK_chat \
    FOREIGN KEY (chat_id) REFERENCES chats(id)")
  }

  down () {
    this.raw(`ALTER TABLE project_fundings
    DROP FOREIGN KEY FK_chat`)
  }
}

module.exports = ProjectFundingsSchema
