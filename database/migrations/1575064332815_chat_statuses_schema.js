'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatStatusesSchema extends Schema {
  up () {
    this.create('chat.chat_statuses', (table) => {
      table.uuid('id')
      table.uuid('chat_content_id').references('id').inTable('chat.chat_contents')
      table.uuid('chat_member_id').references('id').inTable('chat.chat_members')
      table.string('read_status')
      table.timestamps()
    })

    this.raw(`
      ALTER TABLE chat.chat_statuses ADD CONSTRAINT PK_Chat_Statuses PRIMARY KEY (id)
    `)
  }

  down () {
    this.drop('chat.chat_statuses')
  }
}

module.exports = ChatStatusesSchema
