'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherSchema extends Schema {
  up () {
    this.create('user_researchers', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('gender').nullable()
      table.string('phone').nullable()
      table.string('institution').nullable()
      table.string('position').nullable()
      table.uuid('research_field_id').nullable().references('id').inTable('md_research_fields')
      table.uuid('study_area_id').nullable().references('id').inTable('md_study_areas')
      table.string('scopus_index').nullable()
      table.string('google_scholar_link').nullable()
      table.string('profile_picture').nullable()
      table.string('id_card').nullable()
      table.string('status').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_researchers')
  }
}

module.exports = UserResearcherSchema
