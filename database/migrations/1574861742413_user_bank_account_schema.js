'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserBankAccountSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_bank_accounts ADD COLUMN 
    status VARCHAR(15)`)
  }

  down () {
    this.raw(`ALTER TABLE user_bank_accounts DROP COLUMN status`)
  }
}

module.exports = UserBankAccountSchema
