'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdCitySchema extends Schema {
  up () {
    this.create('md_cities', (table) => {
      table.uuid('id').primary()
      table.uuid('country_id').references('id').inTable('md_countries')
      table.uuid('province_id').references('id').inTable('md_provinces')
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_cities')
  }
}

module.exports = MdCitySchema
