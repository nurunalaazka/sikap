'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserInvestorCompanySchema extends Schema {
  up () {
    this.create('user_investor_companies', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('chairman').nullable()
      table.string('pic').nullable()
      table.string('phone').nullable()
      table.uuid('country_id').nullable().references('id').inTable('users')
      table.uuid('province_id').nullable().references('id').inTable('users')
      table.uuid('city_id').nullable().references('id').inTable('users')
      table.uuid('district_id').nullable().references('id').inTable('users')
      table.string('address').nullable()
      table.string('profile_picture').nullable()
      table.string('status').nullable()
      table.uuid('industry_id').nullable().references('id').inTable('users')
      table.uuid('industry_category_id').nullable().references('id').inTable('users')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_investor_companies')
  }
}

module.exports = UserInvestorCompanySchema
