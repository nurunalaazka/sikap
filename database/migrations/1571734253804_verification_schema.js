'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VerificationSchema extends Schema {
  up () {
    this.create('verifications', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('token', 255).notNullable().unique().index()
      table.string('type', 80).notNullable()
      table.boolean('is_revoked').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('verifications')
  }
}

module.exports = VerificationSchema
