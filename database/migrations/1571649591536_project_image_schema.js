'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectImageSchema extends Schema {
  up () {
    this.create('project_images', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').references('id').inTable('projects')
      table.string('name')
      table.string('type').nullable()
      table.integer('size').nullable()
      table.string('location').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_images')
  }
}

module.exports = ProjectImageSchema
