'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserInvestorCompaniesSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_investor_companies
    MODIFY COLUMN address LONGTEXT`)
  }

  down () {
    this.table('user_investor_companies', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserInvestorCompaniesSchema
