'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ForgotPasswordSchema extends Schema {
  up () {
    this.create('forgot_passwords', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('token').notNullable().unique()
      table.timestamp('expired_at').notNullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('forgot_passwords')
  }
}

module.exports = ForgotPasswordSchema
