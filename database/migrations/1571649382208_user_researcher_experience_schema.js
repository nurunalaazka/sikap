'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherExperienceSchema extends Schema {
  up () {
    this.create('user_researcher_experiences', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('institution').nullable()
      table.string('position').nullable()
      table.uuid('industry_id').nullable().references('id').inTable('md_industries')
      table.uuid('industry_category_id').nullable().references('id').inTable('md_industry_categories')
      table.string('location').nullable()
      table.date('date_start').nullable()
      table.date('date_end').nullable()
      table.string('description').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_researcher_experiences')
  }
}

module.exports = UserResearcherExperienceSchema
