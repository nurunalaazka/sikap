'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneSchema extends Schema {
  up () {
    this.create('project_milestones', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').references('id').inTable('projects')
      table.string('title').nullable()
      table.integer('percentage').nullable()
      table.integer('duration').nullable()
      table.string('duration_unit').nullable()
      table.integer('required_fund').nullable()
      table.string('description').nullable()
      table.string('status').nullable()
      table.integer('order').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_milestones')
  }
}

module.exports = ProjectMilestoneSchema
