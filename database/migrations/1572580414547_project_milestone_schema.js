'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE project_milestones
    MODIFY COLUMN required_fund BIGINT`)
  }

  down () {
    this.raw(`ALTER TABLE project_milestones
    MODIFY COLUMN required_fund INT`)
  }
}

module.exports = ProjectMilestoneSchema
