'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {  
    this.raw(`
    ALTER TABLE chats DROP FOREIGN KEY chats_sender_id_foreign, DROP FOREIGN KEY chats_receiver_id_foreign`)

    this.raw(`ALTER TABLE chats DROP sender_id, 
    DROP receiver_id, DROP receiver_status, 
    DROP sender_status`)

  }

  down () {
    this.table('chats', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChatSchema
