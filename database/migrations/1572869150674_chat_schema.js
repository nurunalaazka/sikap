'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE chats
    CHANGE content content_id varchar(36)`)
  }

  down () {
    this.table('chats', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChatSchema
