'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneProgressSchema extends Schema {
  up () {
    this.create('project_milestone_progresses', (table) => {
      table.uuid('id').primary()
      table.uuid('project_id').nullable().references('id').inTable('projects')
      table.uuid('project_milestone_id').nullable().references('id').inTable('project_milestones')
      table.string('title').nullable()
      table.integer('percentage').nullable()
      table.string('description').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('project_milestone_progresses')
  }
}

module.exports = ProjectMilestoneProgressSchema
