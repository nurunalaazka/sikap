'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdStudyAreaSchema extends Schema {
  up () {
    this.create('md_study_areas', (table) => {
      table.uuid('id').primary()
      table.uuid('research_field_id').references('id').inTable('md_research_fields')
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_study_areas')
  }
}

module.exports = MdStudyAreaSchema
