'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdResearchFieldSchema extends Schema {
  up () {
    this.create('md_research_fields', (table) => {
      table.uuid('id').primary()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_research_fields')
  }
}

module.exports = MdResearchFieldSchema
