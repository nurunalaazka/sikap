'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatContentsSchema extends Schema {
  up () {
    this.create('chat.chat_contents', (table) => {
      table.uuid('id')
      table.uuid('room_id').references('id').inTable('chat.rooms')
      table.uuid('sender_id').references('id').inTable('chat.chat_members')
      table.text('content','longtext').nullable()
      table.string('sender_status',20)
      table.string('receiver_status',20)
      table.timestamps()
    })

    this.raw(`
    ALTER TABLE chat.chat_contents ADD CONSTRAINT PK_Chat PRIMARY KEY (id)
    `)
  }

  down () {
    this.drop('chat.chat_contents')
  }
}

module.exports = ChatContentsSchema
