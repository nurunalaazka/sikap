'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdPermissionSchema extends Schema {
  up () {
    this.create('md_permissions', (table) => {
      table.string('id').primary()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_permissions')
  }
}

module.exports = MdPermissionSchema
