'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserBankAccountSchema extends Schema {
  up () {
    this.create('user_bank_accounts', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('number').nullable()
      table.string('holder').nullable()
      table.string('bank_address').nullable()
      table.uuid('bank_id').nullable().references('id').inTable('md_banks')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_bank_accounts')
  }
}

module.exports = UserBankAccountSchema
