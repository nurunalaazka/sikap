'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdIndustryCategorySchema extends Schema {
  up () {
    this.create('md_industry_categories', (table) => {
      table.uuid('id').primary()
      table.uuid('industry_id').references('id').inTable('md_industries')
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_industry_categories')
  }
}

module.exports = MdIndustryCategorySchema
