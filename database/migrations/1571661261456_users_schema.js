'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.raw("ALTER TABLE users ADD CONSTRAINT FK_to_role \
    FOREIGN KEY (role_id) REFERENCES md_roles(id)")
  }

  down () {
    this.raw(`ALTER TABLE users
    DROP FOREIGN KEY FK_to_role`)
  }
}

module.exports = UsersSchema
