'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserInvestorCompanySchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_province_id_foreign`)

    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_industry_id_foreign`)

    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_industry_category_id_foreign`)

    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_district_id_foreign`)

    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_country_id_foreign`)

    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_city_id_foreign`)    

    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_province \
    FOREIGN KEY (province_id) REFERENCES md_provinces(id)")
    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_country \
    FOREIGN KEY (country_id) REFERENCES md_countries(id)")
    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_city \
    FOREIGN KEY (city_id) REFERENCES md_cities(id)")
    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_district \
    FOREIGN KEY (district_id) REFERENCES md_districts(id)")
    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_industry \
    FOREIGN KEY (industry_id) REFERENCES md_industries(id)")
    this.raw("ALTER TABLE user_investor_companies ADD CONSTRAINT FK_industry_category \
    FOREIGN KEY (industry_category_id) REFERENCES md_industry_categories(id)")
    
  }

  down () {
    this.raw(`ALTER TABLE user_investor_companies
    DROP FOREIGN KEY user_investor_companies_province_id_foreign`)
  }
}

module.exports = UserInvestorCompanySchema
