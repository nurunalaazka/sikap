'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResearchNeedSchema extends Schema {
  up () {
    this.create('research_needs', (table) => {
      table.uuid('id').primary()
      table.uuid('investor_id').references('id').inTable('users')  
      table.string('title')
      table.uuid('research_field_id').references('id').inTable('md_research_fields')
      table.uuid('study_area_id').references('id').inTable('md_study_areas')
      table.string('description')
      table.string('status')
      table.integer('research_budget')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('research_needs')
  }
}

module.exports = ResearchNeedSchema
