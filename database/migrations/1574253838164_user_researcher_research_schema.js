'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherResearchSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_researcher_researches
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('user_researcher_researches', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserResearcherResearchSchema
