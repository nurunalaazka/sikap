'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE products
    MODIFY COLUMN description LONGTEXT`)

    this.raw(`ALTER TABLE products ADD COLUMN 
    status VARCHAR(15)`)
  }

  down () {
    this.raw(`ALTER TABLE products
    MODIFY COLUMN description VARCHAR(255)`)

    this.raw(`ALTER TABLE products DROP COLUMN 
    status`)
  }
}

module.exports = ProductSchema
