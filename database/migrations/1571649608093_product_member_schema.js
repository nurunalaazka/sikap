'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductMemberSchema extends Schema {
  up () {
    this.create('product_members', (table) => {
      table.uuid('id').primary()
      table.uuid('product_id').references('id').inTable('products')
      table.uuid('researcher_id').references('id').inTable('users')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_members')
  }
}

module.exports = ProductMemberSchema
