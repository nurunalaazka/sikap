'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdProvinceSchema extends Schema {
  up () {
    this.create('md_provinces', (table) => {
      table.uuid('id').primary()
      table.uuid('country_id').references('id').inTable('md_countries')
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_provinces')
  }
}

module.exports = MdProvinceSchema
