'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmailMessageSchema extends Schema {
  up () {
    this.create('email_messages', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('email')
      table.string('title')
      table.string('content')
      table.string('thumbnail').nullable()
      table.string('action_link').nullable()
      table.string('action_text').nullable()
      table.string('status')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('email_messages')
  }
}

module.exports = EmailMessageSchema
