'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductCollaborationOffersSchema extends Schema {
  up () {
    this.raw(`
      ALTER TABLE product_collaboration_offers 
      ADD UNIQUE unique_product_offer(product_id, investor_id)
    `)
  }

  down () {
    this.raw(`
      ALTER TABLE product_collaboration_offers 
      DROP INDEX unique_product_offer
    `)
  }
}

module.exports = ProductCollaborationOffersSchema
