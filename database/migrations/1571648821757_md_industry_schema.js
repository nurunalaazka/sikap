'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdIndustrySchema extends Schema {
  up () {
    this.create('md_industries', (table) => {
      table.uuid('id').primary()
      table.string('name').unique()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_industries')
  }
}

module.exports = MdIndustrySchema
