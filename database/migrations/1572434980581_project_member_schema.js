'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMemberSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE project_members
    ADD UNIQUE unique_index(project_id, researcher_id)`)
  }

  down () {
    this.table('project_members', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectMemberSchema
