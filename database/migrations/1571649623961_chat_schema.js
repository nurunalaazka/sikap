'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {
    this.create('chats', (table) => {
      table.uuid('id').primary()
      table.uuid('sender_id').references('id').inTable('users')
      table.uuid('receiver_id').references('id').inTable('users')
      table.string('content')
      table.string('receiver_status')
      table.string('sender_status')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('chats')
  }
}

module.exports = ChatSchema
