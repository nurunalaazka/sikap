'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentHistorySchema extends Schema {
  up () {
    this.create('payment_histories', (table) => {
      table.uuid('id').primary()
      table.uuid('payment_id').references('id').inTable('payments')
      table.string('status').nullable()
      table.string('receipt').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('payment_histories')
  }
}

module.exports = PaymentHistorySchema
