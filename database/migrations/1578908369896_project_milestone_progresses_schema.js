'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectMilestoneProgressesSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE project_milestone_progresses
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('project_milestone_progresses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectMilestoneProgressesSchema
