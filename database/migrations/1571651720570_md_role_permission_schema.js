'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MdRolePermissionSchema extends Schema {
  up () {
    this.create('md_role_permissions', (table) => {
      table.uuid('id').primary()
      table.string('permission_id')
      table.string('role_id').references('id').inTable('md_roles')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('md_role_permissions')
  }
}

module.exports = MdRolePermissionSchema
