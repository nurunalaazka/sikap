'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatMembersSchema extends Schema {
  up () {
    this.create('chat.chat_members', (table) => {
      table.uuid('id')
      table.uuid('room_id').references('id').inTable('chat.rooms')
      table.uuid('user_id')
      table.timestamps()
    })

    this.raw(`
      ALTER TABLE chat.chat_members ADD CONSTRAINT PK_Chat_Member PRIMARY KEY (id),
      ADD UNIQUE unique_index(room_id, user_id)
    `)
  }

  down () {
    this.drop('chat.chat_members')
  }
}

module.exports = ChatMembersSchema
