'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.uuid('id').primary()
      table.string('title').unique()
      table.string('sub_title').nullable()
      table.integer('duration').nullable()
      table.string('duration_unit').nullable()
      table.date('date_start').nullable()
      table.uuid('research_field_id').nullable().references('id').inTable('md_research_fields')
      table.uuid('study_area_id').nullable().references('id').inTable('md_study_areas')
      table.integer('required_fund').nullable()
      table.string('cover_image').nullable()
      table.string('abstract').nullable()
      table.uuid('researcher_id').references('id').inTable('users')
      table.string('status').nullable()
      table.uuid('investor_id').nullable().references('id').inTable('users')
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
