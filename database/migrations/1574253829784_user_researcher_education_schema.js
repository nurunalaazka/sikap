'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherEducationSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE user_researcher_educations
    MODIFY COLUMN description LONGTEXT`)
  }

  down () {
    this.table('user_researcher_educations', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UserResearcherEducationSchema
