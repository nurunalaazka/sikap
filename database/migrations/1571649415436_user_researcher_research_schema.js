'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserResearcherResearchSchema extends Schema {
  up () {
    this.create('user_researcher_researches', (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users')
      table.string('title').nullable()
      table.string('position').nullable()
      table.uuid('research_field_id').nullable().references('id').inTable('md_research_fields')
      table.uuid('study_area_id').nullable().references('id').inTable('md_study_areas')
      table.string('location').nullable()
      table.date('date_start').nullable()
      table.date('date_end').nullable()
      table.string('description').nullable()
      table.uuid('created_by').nullable().references('id').inTable('users')
      table.uuid('modified_by').nullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_researcher_researches')
  }
}

module.exports = UserResearcherResearchSchema
