'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.raw(`ALTER TABLE projects
    MODIFY COLUMN required_fund BIGINT`)
  }

  down () {
    this.raw(`ALTER TABLE projects
    MODIFY COLUMN required_fund INT`)
  }
}

module.exports = ProjectSchema
