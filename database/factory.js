'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

// Factory.blueprint('App/Models/User', (faker) => {
//   return {
//     username: faker.username()
//   }
// })

Factory.blueprint('App/Models/MdBank', (faker) => {
    return {
        name: faker.company(),
        short_name: faker.username(),
    }
})

Factory.blueprint('App/Models/MdResearchField', (faker) => {
    return {
        name: faker.name()
    }
})

Factory.blueprint('App/Models/MdStudyArea', (faker) => {
    return {
        name: faker.name()
    }
})

Factory.blueprint('App/Models/MdCountry', (faker) => {
    return {
        name: faker.country({full:true})
    }
})
Factory.blueprint('App/Models/MdProvince', (faker) => {
    return {
        name: faker.province({full:true})
    }
})
Factory.blueprint('App/Models/MdCity', (faker) => {
    return {
        name: faker.city()
    }
})
Factory.blueprint('App/Models/MdDistrict', (faker) => {
    return {
        name: faker.name()
    }
})