'use strict'

/*
|--------------------------------------------------------------------------
| CountrySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class CountrySeeder {
  async run () {
    // let i = 10
    // // await Factory
    // while(i--){
    //   const country = await Factory.model('App/Models/MdCountry').create()
    //   const province = await Factory.model('App/Models/MdProvince').make()
    // //   const city = await Factory.model('App/Models/MdCity').make()
    // //   const district = await Factory.model('App/Models/MdDistrict').make()

    //   await country.provinces().save(province)
    // }
  }
}

module.exports = CountrySeeder
