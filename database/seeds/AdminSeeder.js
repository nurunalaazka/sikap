'use strict'

/*
|--------------------------------------------------------------------------
| AdminSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdRole = use('App/Models/MdRole')
const { v4: uuid } = require('uuid');

class AdminSeeder {
  async run () {

    await MdRole.createMany([ 
      {id : 'admin', name:'admin', type:'admin'}
    ])

    await User.createMany([{                
      name:'admin researcher',
      username:'admin_researcher',
      email:'researcher@admin.com',
      password:'123456',
      type:'researcher',
      role_id:'admin',
      status:'verified'
    },{                
      name:'admin investor individual',
      username:'admin_individual',
      email:'individual@admin.com',
      password:'123456',
      type:'investor individual',
      role_id:'admin',
      status:'verified'
    },{                
      name:'admin investor company',
      username:'admin_company',
      email:'company@admin.com',
      password:'123456',
      type:'investor company',
      role_id:'admin',
      status:'verified'
    }]) 
  }
}

module.exports = AdminSeeder
