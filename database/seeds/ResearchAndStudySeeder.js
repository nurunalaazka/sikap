'use strict'

/*
|--------------------------------------------------------------------------
| ResearchAndStudySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ResearchAndStudySeeder {
  async run () {
    // let i = 5
    // while(i>0){
    //   const researchField = await Factory.model('App/Models/MdResearchField').create()
    //   const studyArea = await Factory.model('App/Models/MdStudyArea').make()

    //   await researchField.studyAreas().save(studyArea)
    //   i--
    // }
  }
}

module.exports = ResearchAndStudySeeder
