'use strict'

/*
|--------------------------------------------------------------------------
| MdRoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const MdRole = use('App/Models/MdRole')

class MdRoleSeeder {
  async run () {
    // await MdRole.createMany([ 
    //   {id : 'admin', name:'admin', type:'admin'},
    //   {id : 'member', name:'member', type:'member'},
    // ])
  }
}

module.exports = MdRoleSeeder
