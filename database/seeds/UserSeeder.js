'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserBankAccount = use('App/Models/UserBankAccount')
const { v4: uuid } = require('uuid');

class UserSeeder {
  async run () {
    // try {
    //   const user = new User();
    //   const id_comp = uuid()
    //   const id_ind = uuid()

    //   console.log(id_comp, id_ind)

    //   const array = [{
    //     id:id_comp,
    //     name:'investor company',
    //     username:'company',
    //     email:'company@comp.com',
    //     password:'123',
    //     type:'investor company',
    //     status:'verified'
    //   },{
    //     id:id_ind,
    //     name:'investor individual',
    //     username:'individual',
    //     email:'individual@ind.com',
    //     password:'123',
    //     type:'investor individual',
    //     status:'verified'
    //   }]     

    //   await User.createMany(array)

    //   await this.inv(id_comp, id_ind)
    //   await this.bank(id_comp, id_ind)
      
    // } catch (error) {
    //   console.log(error)
    // }    
        

  }

  async inv(id_comp, id_ind){

    const comp = await User.find(id_comp)
    const ind = await User.find(id_ind)

    const cdata = new UserInvestorCompany()
    const idata = new UserInvestorIndividual()

    cdata.fill({
      chairman:'mr. chairman',
      phone:'08997960770'
    })

    idata.fill({
      gender:'male',
      phone:'087858001293'
    })

    await comp.userInvestorCompany().save(cdata)
    await ind.userInvestorIndividual().save(idata)
  }

  async bank(id_comp, id_ind){
    const icomp = await User.find(id_comp)
    const iind = await User.find(id_ind)

    const cdata = new UserBankAccount()
    const idata = new UserBankAccount()

    cdata.fill({
      number:'08891273',
      holder:'chairman',
      bank_address:'jl. dododo'
    })

    idata.fill({
      number:'0568002',
      holder:'individu',
      bank_address:'jl. dododossss'
    })

    await icomp.userBankAccount().save(cdata)
    await iind.userBankAccount().save(idata)
  }
}

module.exports = UserSeeder
