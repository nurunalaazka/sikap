
$( document ).ready(function() {    
    $('[name="country_id').on('change',function(){
        let id = $(this).val();
        setProvince([])
        setCity([])

        $.ajax({
            url:"/ajax/getProvinces",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setProvince(response.data)
            }
        });
        
    })

    $('[name="province_id').on('change',function(){
        let id = $(this).val();
        setCity([])
        
        $.ajax({
            url:"/ajax/getCities",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setCity(response.data)
            }
        });
        
    })

    $('[name="city_id').on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getDistricts",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setDistrict(response.data)
            }
        });
        
    })
    
    function setProvince(data){
        $('[name="province_id"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="province_id"]').html(html)
    }

    function setCity(data){
        $('[name="city_id"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="city_id"]').html(html)
    }

    function setDistrict(data){
        $('[name="district_id"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="district_id"]').html(html)
    }



});

         


