$( document ).ready(function() {
    let pw_field = $('[name="password"]')
    let rpw_field = $('[name="repeat_password"]')
    $('[name="password"], [name="repeat_password"]').on('change',function(){
        if(pw_field.val() === rpw_field.val() && (pw_field.val() && rpw_field.val()) ){
            
            setWarning('#warning','Password mismatch!',false)
        }else {
            
            setWarning('#warning','Password mismatch!',true)
        }

        console.log(pw_field.val(), rpw_field.val())
    })

    function setWarning (el, msg, showMsg) {
        if(showMsg){
            $(el).text(msg)
            $('#btnSubmit').attr('disabled',true)  
        }else{
            $(el).text('')
            $('#btnSubmit').attr('disabled',false)  
        }
    }   


})