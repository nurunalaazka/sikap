
$( document ).ready(function() {
    let tmp = $('[name="research_field_id"]').val() 
      
    console.log(filter_study_area_id)
    if(tmp !== 'All Research Fields') {
        $.ajax({
            url:"/ajax/getStudyAreas",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:tmp},
            success:function(response){
                console.log(response)
                setStudyArea(response.data)
            }
        }); 
    }
    
    $('[name="research_field_id"]').on('change',function(){
        let id = $(this).val();
        
        $.ajax({
            url:"/ajax/getStudyAreas",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setStudyArea(response.data)
            }
        });
        
    })

    function setStudyArea (data) {
        $('[name="study_area_id"]').removeAttr('disabled')

        let html = '<option>All Study Areas</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="study_area_id"]').html(html)

        if(filter_study_area_id) $('[name="study_area_id"]').val(filter_study_area_id)
         
    }

    $('#apply-filter, #search-key').on('click',function(event){
        console.log(event.target.id)

        let research_field_id = $('[name="research_field_id"]').val()
        let study_area_id = $('[name="study_area_id"]').val()
        let type = $('[name="type"]').val()
        let status = $('[name="status"]').val() 
        let keyword = $('[name="keywords"]').val()     
        
        let params = `${url}?filter=true`

        if(research_field_id !== 'All Research Fields'){
            params += `&research_field_id=${research_field_id}`            
        }

        if(study_area_id !== 'All Study Areas') {
            params += `&study_area_id=${study_area_id}`
        }

        if(type !== 'All Types') {
            params += `&type=${type}`
        }

        if(status !== 'All Statuses') {
            params += `&status=${status}`
        }        

        if(keyword && !isBlank(keyword)) {
            params += `&keywords=${encodeURIComponent(keyword)}`
        }

        if(event.target.id === 'apply-filter') {
            $(this).attr('href',params)
        }
        else {
            if(!keyword) {
                $(this).attr('href',params)
            }
            else if(keyword.length >= 3 && !isBlank(keyword)) {
                $(this).attr('href',params)
            } else {
                alert('min 3 characters and not blank')
            }
        }
        // $(this).attr('href',params)

        // $(this).closest('form').submit();
    })

    // $('.search-key').on('click', function(){
        
    // })
    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }    

});

         


