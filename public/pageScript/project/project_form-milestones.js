var n=0;
$( document ).ready(function() {
    console.log(required_fund)
    n = $('#milestones').find('.milestone-form-item').length;
    if(n){
        $('#formNavigation').show();
    }
});

$('#required_fund_percentage').on('change textInput input', function(){
    let percent = $(this).val();

    let temp = (percent/100)*required_fund;

    // console.log('fund', temp)
    
    $('#required_fund').val(temp);
})

$('#required_fund').on('change textInput input',function(){
    let nominal = $(this).val();

    let percent = nominal/required_fund*100;

    $('#required_fund_percentage').val(percent);
})

$('#formEditMilestone [name="fund_percentage"]').on('change textInput input', function(){
    let percent = $(this).val();

    let temp = (percent/100)*required_fund;
    
    $('#formEditMilestone [name="required_fund"]').val(temp);
})

$('#formEditMilestone [name="required_fund"]').on('change textInput input',function(){
    let nominal = $(this).val();

    let percent = nominal/required_fund*100;

    $('#formEditMilestone [name="fund_percentage"]').val(percent);
})

$('#btn-add').on('click',function () {        
    let milestonesHTML = $('#milestones').html();
    var parent = $('#formMilestone');
    let title = $('#title').val();
    let percentage = $('#percentage').val();
    let duration = $('#duration').val();
    let duration_unit = $('#duration_unit').val();
    let this_required_fund = $('#required_fund').val();
    let description = $('#description').val();

    if(title && percentage && duration && duration_unit && this_required_fund && description){
    
    n+=1;
    let html = `<div class="card progress-item milestone-form-item" id="milestone${n}" data-index="${n}">
        <input type="text" class="form-control" name="title[]" hidden value="${title}">
        <input type="number" max="100" min="0" class="form-control" name="percentage[]" hidden value="${percentage}" >
        <input type="text" class="form-control" name="duration[]" hidden value="${duration}">
        <input hidden type="text" class="form-control" name="duration_unit[]" value="${duration_unit}" hidden>
        <input type="number" class="form-control" name="required_fund[]" hidden value="${this_required_fund}">
        <input type="number" class="form-control" name="fund_percentage[]" hidden value="${this_required_fund*100/parseInt(required_fund)}">
        <input type="text" class="form-control" name="description[]" hidden value="${description}">
        <input type="hidden" name="milestone_id[]" value="">
        <input type="hidden" name="state[]" value="new">
        <div class="progress-item-header">
            <div class="header-left">
                <i class="material-icons-outlined">expand_more</i>
                <h5 class="progress-title" style="font-weight:normal" id="title${n}">${title}</h5>
                
            </div>
            <div class="header-right" style="align-items:center">
                <div class="item">
                    <span>
                        <i class="material-icons-outlined">hourglass_empty</i>
                    </span>
                    <div>
                        <span class="item-label">Duration</span>
                        <h5 class="item-content" id="duration${n}">${duration} ${duration_unit}(s)</h5>
                    </div>
                </div>
                <div class="mr-5" id="percentage${n}">
                    <div class="progress mb-2" style="width:7rem;">
                        <div class="progress-bar bg-success" role="progressbar" style="width: ${percentage}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        
                    </div>
                    <small>
                        ${percentage}% of Total Milestones
                    </small>
                </div>
                
                <div class="d-flex">

                    <a class="btn-icon btn-edit" data-id=""><i class="material-icons-outlined">edit</i></a>
                    <a class="btn-icon btn-delete" data-id=""><i class="material-icons-outlined">delete</i></a>
                </div>
            </div>
            
            
        </div>
        <div class="progress-item-body">
        <div class="row milestone-detail">
            <div class="col-md-5">
            <h3 class="percentage">
                <span id="fund_percentage${n}">${this_required_fund*100/parseInt(required_fund)}</span> %
                <small class="desc">required from total funding</small>
            </h3>
            <h2 class="amount">
                Rp <span id="required_fund${n}">${convertToMoney(this_required_fund)}</span>
            </h2>
            </div>
            <div class="col-md-7">
                <p class="mb-0" id="description${n}">${description}                             
                </p>
            </div>
        </div>      
        </p>
        </div>
    </div>`;

    $('#milestones').html(milestonesHTML+html);
    $('#formNavigation').show();
    parent.find("[name='title']").val('');
    parent.find("[name='percentage']").val('');
    parent.find("[name='duration']").val('');
    parent.find("[name='required_fund']").val('');
    parent.find("[name='fund_percentage']").val('');
    parent.find("[name='description']").val('');
    parent.find("[name='id']").val('');
    parent.find("[name='state']").val('');
    parent.modal('hide'); 
    }       

    else{
        alert('You have to fill all the form inputs');
    }
})

$(document).on('click','.milestone-form-item .btn-edit',function(){
    console.log('yo masook');
    parent = $(this).parents('.milestone-form-item');
    parentId = parent.attr('id');

    var title = parent.find("[name='title[]']").val();
    var percentage = parent.find("[name='percentage[]']").val();
    var duration = parent.find("[name='duration[]']").val();
    var duration_unit = parent.find("[name='duration_unit[]']").val();
    var required_fund = parent.find("[name='required_fund[]']").val();
    var fund_percentage = parent.find("[name='fund_percentage[]']").val();
    var description = parent.find("[name='description[]']").val();
    var id = parent.find("[name='id[]']").val();
    var state = parent.find("[name='state[]']").val();

    var formEdit = $('#formEditMilestone');
    formEdit.find("[name='title']").val(title);
    formEdit.find("[name='percentage']").val(percentage);
    formEdit.find("[name='duration']").val(duration);
    formEdit.find("[name='duration_unit']").val(duration_unit);
    formEdit.find("[name='required_fund']").val(required_fund);
    formEdit.find("[name='fund_percentage']").val(fund_percentage);
    formEdit.find("[name='description']").val(description);
    formEdit.find("[name='id']").val(id);
    formEdit.find("[name='state']").val(state);
    formEdit.find(".masked-money").html('Rp '+convertToMoney(required_fund));
    formEdit.find('#btnSaveEdit').data('id','#'+parentId);
    formEdit.modal('show');
});

$(document).on('click','#btnSaveEdit',function(){
    console.log('miauosuk');
    var target = $('#milestones').find($(this).data('id'));
    console.log(target);
    console.log('data id:'+$(this).data('id'));
    var parent = $('#formEditMilestone');
    var index =  target.data('index');
    console.log(index);

    var title = parent.find("[name='title']").val();
    var percentage = parent.find("[name='percentage']").val();
    console.log('persentasinya adalah:'+percentage);
    var duration = parent.find("[name='duration']").val();
    var duration_unit = parent.find("[name='duration_unit']").val();
    var required_fund = parent.find("[name='required_fund']").val();
    var fund_percentage = parent.find("[name='fund_percentage']").val();
    var description = parent.find("[name='description']").val();
    var id = parent.find("[name='id']").val();
    var state = parent.find("[name='state']").val();

    if(title && percentage && duration && duration_unit && required_fund && description){
        console.log('masuk ke if');
        console.log('persentasinya masuk lho yaitu:'+percentage);
        target.find("[name='title[]']").val(title).trigger('change');
        target.find("[name='percentage[]']").val(percentage).trigger('change');
        target.find("[name='duration[]']").val(duration).trigger('change');
        target.find("[name='duration_unit[]']").val(duration_unit).trigger('change');
        target.find("[name='required_fund[]']").val(required_fund).trigger('change');
        target.find("[name='description[]']").val(description).trigger('change');
        target.find("[name='id[]']").val(id);
        target.find("[name='state[]']").val(state);

        target.find("#title"+index).html(title);
        console.log(target.find("#title"+index).html());
        target.find("#percentage"+index).find('small').html(percentage+'% of Total Milestones');
        target.find("#percentage"+index).find('.progress-bar').css({width:percentage+'%'});
        target.find("#duration"+index).html(duration+' '+duration_unit+'(s)');
        // target.find("#duration_unit"+index).html(duration_unit);
        target.find("#required_fund"+index).html(convertToMoney(required_fund));
        target.find("#fund_percentage"+index).html(fund_percentage);
        target.find("#description"+index).html(description);
        target.find("#id"+index).html(id);
        target.find("#state"+index).html(state);
        console.log(target.find("[name='title[]']").val());
        console.log(target.find("[name='percentage[]']").val());
        console.log(target.find("[name='duration[]']").val());
        console.log(target.find("[name='duration_unit[]']").val());
        console.log(target.find("[name='required_fund[]']").val());
        console.log(target.find("[name='description[]']").val());
        
        parent.modal('hide');

    }       

    else{
        alert('You have to fill all the form inputs');
    }
});

$(document).on('click','.milestone-form-item .btn-delete',function(){
    console.log('yo masook');
    parent = $(this).parents('.milestone-form-item');
    parentId = parent.attr('id');
    var title = parent.find("[name='title[]']").val();
    $('.delete-item-title').html(title);
    $('.delete-item-type').html('This Milestone');
    $('#btnDeleteItem').data('id','#'+parentId);
    $('#modalDelete').modal('show');
});

$(document).on('click','#btnDeleteItem',function(){
    console.log('yo masook');
    target =  $('#milestones').find($(this).data('id'));
    target.addClass('deleted');
    target.find("[name='state[]']").val('deleted').trigger('change');
    $('#modalDelete').modal('hide');
});

function convertToMoney(plainNumber){
    var money = '';
    var numberRev = plainNumber.toString().split('').reverse().join('');
    for(var i = 0; i < numberRev.length; i++) if(i%3 == 0) money += numberRev.substr(i,3)+'.';
    return money.split('',money.length-1).reverse().join('');
}