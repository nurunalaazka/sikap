
$( document ).ready(function() {
    let studyArea = $('[name="study_area"]')
    let researchField = $('[name="research_field"]')

    researchField.on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getStudyAreas",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setStudyArea(response.data)
            }
        });
        
    })

    function setStudyArea (data) {
        studyArea.removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        studyArea.html(html)
    }

    $('[name="province').on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getCities",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setCity(response.data)
            }
        });
        
    })

    $('[name="city').on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getDistricts",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setDistrict(response.data)
            }
        });
        
    })

    function setCity(data){
        $('[name="city"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="city"]').html(html)
    }

    function setDistrict(data){
        $('[name="district"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="district"]').html(html)
    }



});

         


