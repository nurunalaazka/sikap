$( document ).ready(function() {
    $('[name="email"]').on('change',function(){
        $.ajax({
            url:"/ajax/isExists",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{email:$(this).val()},
            success:function(response){
                console.log(response)                
                setWarning('#emailFound','Email is not registered!',response.found)
            }
        });
    })

    function setWarning (el, msg, hideMsg) {
        if(hideMsg){
            $(el).text('')
            $('#btnSubmit').attr('disabled',false)  
        }else{
            $(el).text(msg)
            $('#btnSubmit').attr('disabled',true)  
        }
    }
})