$( document ).ready(function() {
    let pw_field = $('[name="password"]')
    let rpw_field = $('[name="repeat_password"]')
    $('[name="password"], [name="repeat_password"]').on('change',function(){
        if(pw_field.val()!==rpw_field.val() && pw_field.val() && rpw_field.val()){
            
            setWarning('#mismatch','Password mismatch!',true)
        }else{
            
            setWarning('#mismatch','Password mismatch!',false)
        }
    })

    function setWarning (el, msg, showMsg) {
        if(showMsg){
            $(el).text(msg)
            $('#btnSubmit').attr('disabled',true)  
        }else{
            $(el).text('')
            $('#btnSubmit').attr('disabled',false)  
        }
    }
})