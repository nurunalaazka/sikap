$( document ).ready(function() {
    let pw_field = $('[name="password"]')
    let rpw_field = $('[name="repeat_password"]')
    let pw_valid = false
    let username_valid = false
    let email_valid = false    

    $('[name="password"], [name="repeat_password"]').on('change',function(){
        if(pw_field.val() && rpw_field.val() && pw_field.val()==rpw_field.val()){
            pw_valid = true
            setWarning('#mismatch','Password mismatch!',false)
        }else{
            pw_valid = false
            setWarning('#mismatch','Password mismatch!',true)
        }
    })
    
    $('[name="username"]').on('change',function(){
        
        $.ajax({
            url:"/ajax/isExists",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{username:$(this).val()},
            success:function(response){
                console.log(response)
                username_valid = !response.found
                setWarning('#usernameFound','Username is already exists!',response.found)
            }
        });
    })
    
    $('[name="email"]').on('change',function(){
        $.ajax({
            url:"/ajax/isExists",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{email:$(this).val()},
            success:function(response){
                console.log(response)
                email_valid = !response.found
                setWarning('#emailFound','Email is already Registered!',response.found)
            }
        });
    })

    function setWarning (el, msg, showMsg) {
        if(showMsg){
            $(el).text(msg)
            checkValid()
        }else{
            $(el).text('')
            checkValid()
        }
    }

    function checkValid () {
        if(email_valid && username_valid && pw_valid ){
            $('#btnSubmit').attr('disabled',false);
            $('#btnSubmit').attr('type','submit');
            console.log('masook pak eko');            
        }else{
            $('#btnSubmit').attr('disabled',true)
        }
    }
    
    $('[name="register_as"]').on('change',function(){
        console.log($(this).val())
        if($(this).val()==="individual"){
            $('#labelName').text('Name')
            $('[name="account_type"]').val('investor individual')
        }else{
            $('#labelName').text('Company Name')
            $('[name="account_type"]').val('investor company')
        }
    })

});