$( document ).ready(function() {
    $('[name="research_field_id"]').on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getStudyAreas",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setStudyArea(response.data)
            }
        });
        
    })

    function setStudyArea (data) {
        $('[name="study_area_id"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="study_area_id"]').html(html)
    }
})