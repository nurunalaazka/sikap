$( document ).ready(function() {
    $('[name="keyword"]').on('keyup',function(){        
        
        if( ($(this).val().length > 2) && !isBlank($(this).val()) ){
            $.ajax({
                url:"/ajax/findAccount",
                headers: {'x-csrf-token': $('[name=_csrf]').val()},
                type:'POST',
                data:{keyword:$(this).val()},
                success:function(response){
                    
                    if(response.account&&response.account.length){
                        setResults(response.account)
                    }else{
                        setResults()
                    }                    
                }
            });
        }else{
            setResults()
        }
        
    })   

    function setResults(data){
        if(data){
            let html = ''

            data.forEach(item => {
                html += `<p>${item}</p>`
            })

            $('#results').html(html)            
            setButton(data)
        }else{
            $('#results').html('')
            $('#btn-add').attr('disabled',true)
        }
        
    }

    function setButton (data) {
        let keyword = $('[name="keyword"]').val()
        if(data.includes(keyword)){
            $('#btn-add').attr('disabled',false)
            
        }else{
            $('#btn-add').attr('disabled',true)
        }
        
    }

    $('#btn-add').on('click',function(){
        let member = $('[name="keyword"]').val();

        $.ajax({
            url:"/ajax/getAccount",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{key:member},
            success:function(response){
                if(response.account){                   
                    addMember(response.account)
                }            
            }
        });
        

    })

    function addMember (data) {


        let html = `<div>
        <div>
            <label for="">${data.name}</label>                
        </div>
        <div>
            <label for="">${data.position} at ${data.institution}</label>
        </div>
        <label for="">Member</label>                
        <input type="hidden" name="member[]" value="${data.id}">
        <hr>
        </div>`

        let memberHTML = $('#members').html();

        $('#members').html(memberHTML+html);
    }
    
    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }
})