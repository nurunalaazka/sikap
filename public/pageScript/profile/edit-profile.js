let studyArea, researchField

$( document ).ready(function() {    
    
    studyArea = $('[name="study_area_id"]')
    researchField = $('[name="research_field_id"]')

    researchField.on('change',function(){
        let id = $(this).val();
    
        $.ajax({
            url:"/ajax/getStudyAreas",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setStudyArea(response.data)
            }
        });
        
    })
    
    function setStudyArea (data) {
        studyArea.removeAttr('disabled')
    
        let html = '<option value="">-select-</option>'
    
        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }
    
        studyArea.html(html)
    }

    $('[name="industry_id"]').on('change',function(){
        let id = $(this).val();
    
        $.ajax({
            url:"/ajax/getIndustryCategories",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{industry_id:id},
            success:function(response){
                console.log(response)
                let html = '<option value="">Select Category</option>'
    
                for (const item of response.data) {
                    html+=`<option value="${item.id}">${item.name}</option>`
                }
            
                $('[name="industry_category_id"]').html(html)
            }
        });
    })
    
    

    /**
     * get/set address
     * 
     */    


    $('[name="province').on('change',function(){
        let id = $(this).val();

        getCities(id);        
    })

    $('[name="city').on('change',function(){
        let id = $(this).val();

        $.ajax({
            url:"/ajax/getDistricts",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:id},
            success:function(response){
                console.log(response)
                setDistrict(response.data)
            }
        });
        
    })

    function getCities (prov_id) {
        $.ajax({
            url:"/ajax/getCities",
            headers: {'x-csrf-token': $('[name=_csrf]').val()},
            type:'POST',
            data:{id:prov_id},
            success:function(response){
                console.log(response)
                setCity(response.data)
            }
        });
    }

    function setCity(data){
        $('[name="city"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="city"]').html(html)
    }

    function setDistrict(data){
        $('[name="district"]').removeAttr('disabled')

        let html = '<option value="">-select-</option>'

        for (const item of data) {
            html+=`<option value="${item.id}">${item.name}</option>`
        }

        $('[name="district"]').html(html)
    }



});



$(document).on('click','.btn-edit-experience',function(){
    console.log('edit-experience');
    var parent = $(this).parents('.item-experience');

    var id = parent.children("[name='id[]']").val();
    var institution = parent.find("[name='institution[]']").val();
    var position = parent.find("[name='position[]']").val();
    var industry_id = parent.find("[name='industry_id[]']").val();
    var industry_category_id = parent.find("[name='industry_category_id[]']").val();

    var location = parent.find("[name='location[]']").val();
    var work_start = parent.find("[name='work_start[]']").val();
    var work_end = parent.find("[name='work_end[]']").val();
    var description = parent.find("[name='description[]']").val();

    console.log(id);
    console.log(institution);

    var targetModal = $('#editExperience');

    targetModal.find("[name='id']").val(id).trigger('change');
    targetModal.find("[name='institution']").val(institution).trigger('change');
    targetModal.find("[name='position']").val(position).trigger('change');
    targetModal.find("[name='industry_id']").val(industry_id).trigger('change');
    targetModal.find("[name='industry_category_id']").val(industry_category_id).trigger('change');

    targetModal.find("[name='location']").val(location).trigger('change');
    targetModal.find("[name='work_start']").val(work_start).trigger('change');
    targetModal.find("[name='work_end']").val(work_end).trigger('change');
    targetModal.find("[name='description']").val(description).trigger('change');


});
$(document).on('click','.btn-edit-research',function(){
    console.log('edit-research');

    var parent = $(this).parents('.item-research');

    var id = parent.children("[name='id[]']").val();
    var title = parent.find("[name='title[]']").val();
    var position = parent.find("[name='position[]']").val();
    var research_field_id = parent.find("[name='research_field_id[]']").val();
    var study_area_id = parent.find("[name='study_area_id[]']").val();

    var location = parent.find("[name='location[]']").val();
    var research_start = parent.find("[name='research_start[]']").val();
    var research_end = parent.find("[name='research_end[]']").val();
    var description = parent.find("[name='description[]']").val();

    console.log(id);
    console.log(study_area_id);

    var targetModal = $('#editResearch');

    targetModal.find("[name='id']").val(id).trigger('change');
    targetModal.find("[name='title']").val(title).trigger('change');
    targetModal.find("[name='position']").val(position).trigger('change');
    targetModal.find("[name='research_field_id']").val(research_field_id).trigger('change');
    setTimeout(function(){
        targetModal.find("[name='study_area_id']").val(study_area_id).trigger('change');
    },2000);
    

    targetModal.find("[name='location']").val(location).trigger('change');
    targetModal.find("[name='research_start']").val(research_start).trigger('change');
    targetModal.find("[name='research_end']").val(research_end).trigger('change');
    targetModal.find("[name='description']").val(description).trigger('change');
});
$(document).on('click','.btn-edit-education',function(){
    console.log('edit-education');

    
    var parent = $(this).parents('.item-education');

    var id = parent.children("[name='id[]']").val();
    var university = parent.find("[name='university[]']").val();
    var faculty = parent.find("[name='faculty[]']").val();
    var major = parent.find("[name='major[]']").val();
    var degree_id = parent.find("[name='degree_id[]']").val();
    var gpa = parent.find("[name='gpa[]']").val();
    var max_gpa = parent.find("[name='max_gpa[]']").val();
    var location = parent.find("[name='location[]']").val();
    var study_start = parent.find("[name='study_start[]']").val();
    var study_end = parent.find("[name='study_end[]']").val();
    var description = parent.find("[name='description[]']").val();

    console.log(id);
    console.log(university);

    var targetModal = $('#editEducation');

    targetModal.find("[name='id']").val(id).trigger('change');
    targetModal.find("[name='university']").val(university).trigger('change');
    targetModal.find("[name='faculty']").val(faculty).trigger('change');
    targetModal.find("[name='major']").val(major).trigger('change');
    targetModal.find("[name='degree_id']").val(degree_id).trigger('change');
    targetModal.find("[name='gpa']").val(gpa).trigger('change');
    targetModal.find("[name='max_gpa']").val(max_gpa).trigger('change');
    targetModal.find("[name='location']").val(location).trigger('change');
    targetModal.find("[name='study_start']").val(study_start).trigger('change');
    targetModal.find("[name='study_end']").val(study_end).trigger('change');
    targetModal.find("[name='description']").val(description).trigger('change');


});
         
$(document).on('click','.btn-delete-research',function(){
    var modal = $('#modalDelete');
    modal.find('.delete-item-title').html($(this).data('title'));
    modal.find('#btnDeleteItem').attr('href',$(this).data('href'));
    modal.find('.delete-item-type').html($(this).data('type'));
});

$(document).on('click','.btn-delete-experience',function(){
    var modal = $('#modalDelete');
    modal.find('.delete-item-title').html($(this).data('title'));
    modal.find('#btnDeleteItem').attr('href',$(this).data('href'));
    modal.find('.delete-item-type').html($(this).data('type'));
});

$(document).on('click','.btn-delete-education',function(){
    var modal = $('#modalDelete');
    modal.find('.delete-item-title').html($(this).data('title'));
    modal.find('#btnDeleteItem').attr('href',$(this).data('href'));
    modal.find('.delete-item-type').html($(this).data('type'));
});