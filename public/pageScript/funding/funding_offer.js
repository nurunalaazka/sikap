$( document ).ready(function() {
    $('#required_fund_percentage').on('change textInput input', function(){
        let percent = $(this).val();

        let temp = (percent/100)*required_fund;
        
        $('#required_fund').val(temp);
    })

    $('#required_fund').on('change textInput input',function(){
        let nominal = $(this).val();

        let percent = nominal/required_fund*100;

        $('#required_fund_percentage').val(percent);
    })    
})