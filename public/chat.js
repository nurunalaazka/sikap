let ws = null

$(function () {
  // Only connect when username is available
  if (window.username) {
    startChat()
  }
})

function startChat () {
  ws = adonis.Ws().connect()

  ws.on('open', () => {
    // $('.connection-status').addClass('connected')    
    subscribeToChannel()
  })

  ws.on('close', () => {
    
  })  

  ws.on('error', () => {
    // $('.connection-status').removeClass('connected')
  })
}

function subscribeToChannel () {
    const chat = ws.subscribe('chat')

    // let room_id = $('#chatDetail').data('room')
    // let member_id = $('#chatDetail').data('id')
  
    chat.on('error', () => {
      $('.connection-status').removeClass('connected')
    })

    // chat.on('ready', () => {
    //   chat.emit('message', {
    //     user_id:user_id,
    //     member_id: member_id,
    //     body: 'Hello!',
    //     room_id: room,
    //     timestamp: new Date(),           
    //   })
    // })

    // chat.on('close', () => {
    //   chat.emit('message', {
    //     user_id:user_id,
    //     member_id: member_id,
    //     body: 'Offline',
    //     room_id: room,
    //     timestamp: new Date(),           
    //   })
    // })
    // chat.on('new:user', (e) => {console.log(e)})
  
    chat.on('message', (message) => {
        let as_sender = message.sender == user_id
        room = $('#chatDetail').data('room')

        console.log('ada pesan..')

        if(room === message.room) {
            $('.chat').append(`
            <div class="chat-item only-message  ${as_sender?'reversed':''}">
                <div class="chat-item-message">
                    <p class="content">${message.message}</p>
                    <div class="item item-sm">
                        <i class="material-icons-outlined">timelapse</i>
                        <span>${message.created_at}</span>
                    </div>
                </div> 
            </div>`)

            window.scrollTo(0,document.querySelector(".widget-chat-body").scrollHeight);
        }
        
        if(chat_is_active && !room_is_active) {
          refreshRooms ()
        }

    })

    setInterval (function () {
      if(room_is_active) {
        loadChat(room)
      }

      if(chat_is_active && !room_is_active) {
        refreshRooms ()
      }
    }, 30000);     
}


$('#typeChat').keyup(function (e) {
    if (e.which === 13) {
        e.preventDefault()

        const message = $(this).val()
        $(this).val('')

        let room_id = $('#chatDetail').data('room')
        let member_id = $('#chatDetail').data('id')

        ws.getSubscription('chat').emit('message', {
            user_id:user_id,
            member_id: member_id,
            body: message,
            room_id: room_id,
            timestamp: new Date(),           
        })
        return
    }
})

function btnChatHandler (el) {
  chat_is_active = true
  refreshRooms ()
}

function btnSendHandler () {
  const message = $('#typeChat').val()
  $('#typeChat').val('')

  let room_id = $('#chatDetail').data('room')
  let member_id = $('#chatDetail').data('id')

  ws.getSubscription('chat').emit('message', {
      user_id:user_id,
      member_id: member_id,
      body: message,
      room_id: room_id,
      timestamp: new Date(),           
  })

}

function chatItemHandler (el) {
  room = el.data('room');    
  let memberId = el.data('id');    
  status = el.data('status');    
  $('#chatDetail').data('room',room);
  $('#chatDetail').data('id',memberId);
  $('#chatDetail').data('status',status);
  room_is_active = true

  loadChat(room)

  console.log(status)
  if(!status) setStatus()
  
}

function btnBackHandler (el) {
  room_is_active = false

  refreshRooms ()
}
 
function btnCloseHandler (el) {
  chat_is_active = false
  room_is_active = false
}

function loadChat (room_id) {
  console.log('load chat ..')
  let member_id = $('#chatDetail').data('id')
  $('.chat').html('')

  $.ajax({
    url:"/ajax/getChatContents",
    headers: {'x-csrf-token': $('[name=_csrf]').val()},
    type:'POST',
    data:{id:room_id, member_id:member_id},
    success:function(response){
        console.log(response)  
        if(response.status == 200) {
          setMessages(response)
        }      
    }
  });
}

function setMessages(response) {
  $('#otherUserImg').attr('src',response.userInformation.profile_picture)
  $('#otherUserName').text(response.userInformation.name)
  let i = response.data.length

  while(i--) {    
    showMessages(response.data[i])
  }
  window.scrollTo(0,document.querySelector(".widget-chat-body").scrollHeight);
}

function showMessages (message) {
  
  $('.chat').append(`
    <div class="chat-item only-message  ${message.as_sender?'reversed':''}">
        <div class="chat-item-message">
            <p class="content">${message.message}</p>
            <div class="item item-sm">
                <i class="material-icons-outlined">timelapse</i>
                <span>${message.created_at}</span>
            </div>
        </div> 
    </div>`)
}

function refreshRooms () {
  console.log('refresh')
  $.ajax({
    url:"/ajax/refreshRooms",
    headers: {'x-csrf-token': $('[name=_csrf]').val()},
    type:'POST',
    data:{user_id},
    success:function(response){
        console.log(response)  
        if(response.status == 200) {
          setRooms (response.data)
        }      
    }
  });
}

function setRooms (data) {
  let field = $('#roomList')
  field.html('') 

  let html = ''

  data.forEach(item => {
    html +=`
    <a class="widget-chat-item ${item.read?'':'active'}" data-id="${ item.member_id }" data-room="${ item.room_id }" data-status="${item.read}">
      <div class="user-avatar user-avatar-xl">
          <img src="${item.img}">
      </div>
      <div class="chat-item-message">
          <h5 class="title">${item.from}

              <div class="item item-sm float-right mr-0">
                  <i class="material-icons-outlined">timelapse</i>
                  <span>${item.created_at}</span>
              </div>

          </h5>
          <p class="content">${item.message}</p>
          
      </div>
      
  </a>`
  })

  field.html(html)

}

function setStatus () {  
  $.ajax({
    url:"/ajax/setStatus",
    headers: {'x-csrf-token': $('[name=_csrf]').val()},
    type:'POST',
    data:{ user_id, room },
    success:function(response){
        console.log(response)  
        if(response.status == 200) {
          // setRooms (response.data)
        }      
    }
  });
}
