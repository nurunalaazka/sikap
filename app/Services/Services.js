const Helpers = use('Helpers')
const User = use('App/Models/User')
const UserResearcher = use('App/Models/UserResearcher')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const Chat = use('App/Models/Chat');
const Room = use('App/Models/Room');
const Notification = use('App/Models/Notification');
const ChatMember = use('App/Models/ChatMember');
const ChatContent = use('App/Models/ChatContent');
const ChatStatus = use('App/Models/ChatStatus');

const Database = use('Database')
const fs = Helpers.promisify(require('fs'))
const removeFile = Helpers.promisify(fs.unlink)
// const isFileExists = Helpers.promisify(fs.existsSync)

class Services {

    async toSqlDate (date) {
        try {
            if(!date) return null
            
            let temp = date.split('/');
    
            let formattedDate = new Date(`${temp[2]}-${temp[1]}-${temp[0]}`);
    
            return formattedDate
        } catch (error) {
            throw error
        }
    }

    async savePic (file, path, filename) {
        try {           

            await file.move(Helpers.publicPath(path), {
                name: filename,
                overwrite: true
            })

            path = `${path}/${filename}`
            // console.log(path)
            return path
        } catch (error) {
            throw error
        }
    }    

    async deleteFile (path) {
        try {
            if(!path) return false

            if(await fs.existsSync(Helpers.publicPath(path))) {
                await fs.unlink(Helpers.publicPath(path)) 
            }      

            return true
            
        } catch (error) {
            throw error
        }
    }

    async uploadMultiple (files, path, isUnique) {
        try{            
            if(files){
        
                await files.moveAll(Helpers.publicPath(path), (file, index) => {
                    
                    if(isUnique) {
                        return {
                            name: `${new Date().getTime()}_${Math.floor((Math.random() * 1000) + 1)}-${file.clientName}`,
                            overwrite: true
                        }
                    } else {
                        return {
                            name: file.clientName,
                            overwrite: true
                        }
                    }
                                  
                  
                })
        
                let movedFiles = files.movedList()     
                
                // console.log(movedFiles)
                
                if (!files.movedAll()) {
                
                  await Promise.all(movedFiles.map((file) => {
                    return removeFile(path.join(file._location, file.fileName))
                  }))
                
                  return files.errors()
                }

                return movedFiles
        
            } else {
                return null
            }

        }catch(error) {
            throw error
        }
    }    

    async elapsedTime (date) {
        let elapsed = (new Date() - new Date(date))
    
        let unit = 'seconds'                

        if(elapsed <= (60*1000)-1) {
            //sec
            elapsed = Math.floor(elapsed / 1000)
            if(elapsed == 1) unit = 'second'
        }
        
        if((elapsed >= (60*1000)) && (elapsed <= (60*60*1000)-1)) {
            //min
            unit = 'minutes'
            elapsed = Math.floor(elapsed / (60*1000))
            if(elapsed == 1) unit = 'minute'
        }

        if((elapsed >= (60*60*1000)) && (elapsed <= (24*60*60*1000)-1)) {
            //h
            unit = 'hours'
            elapsed = Math.floor(elapsed / (60*60*1000))
            if(elapsed == 1) unit = 'hour'
        }

        if((elapsed >= (24*60*60*1000)) && (elapsed <= (7*24*60*60*1000)-1)) {
            // d
            unit = 'days'
            elapsed = Math.floor(elapsed / (24*60*60*1000))
            if(elapsed == 1) unit = 'day'
        }

        if((elapsed >= (7*24*60*60*1000)) && (elapsed <= (4*7*24*60*60*1000)-1)) {
            //w
            unit = 'weeks'
            elapsed = Math.floor(elapsed / (7*24*60*60*1000))
            if(elapsed == 1) unit = 'week'
        }

        if((elapsed >= (4*7*24*60*60*1000)) && (elapsed <= (12*4*7*24*60*60*1000)-1)) {
            //m
            unit = 'months'
            elapsed = Math.floor(elapsed / (4*7*24*60*60*1000))
            if(elapsed == 1) unit = 'month'
        }

        if(elapsed >= (12*4*7*24*60*60*1000)) {
            // y
            unit = 'years'
            elapsed = Math.floor(elapsed / (12*4*7*24*60*60*1000))
            if(elapsed == 1) unit = 'year'
        } 
        
        if(elapsed < 1 && unit == 'seconds') return `just now`
            
        return `${elapsed} ${unit} ago`
    }

    async getInterval(miliseconds) {
        let elapsed = miliseconds
    
        let unit = 'seconds'                

        if(elapsed <= (60*1000)-1) {
            //sec
            elapsed = Math.floor(elapsed / 1000)
            if(elapsed == 1) unit = 'second'
        }
        
        if((elapsed >= (60*1000)) && (elapsed <= (60*60*1000)-1)) {
            //min
            unit = 'minutes'
            elapsed = Math.floor(elapsed / (60*1000))
            if(elapsed == 1) unit = 'minute'
        }

        if((elapsed >= (60*60*1000)) && (elapsed <= (24*60*60*1000)-1)) {
            //h
            unit = 'hours'
            elapsed = Math.floor(elapsed / (60*60*1000))
            if(elapsed == 1) unit = 'hour'
        }

        if((elapsed >= (24*60*60*1000)) && (elapsed <= (365*24*60*60*1000)-1)) {
            // d
            unit = 'days'
            elapsed = Math.floor(elapsed / (24*60*60*1000))
            if(elapsed == 1) unit = 'day'
        }

        if(elapsed >= (365*24*60*60*1000)) {
            // y
            unit = 'years'
            elapsed = Math.floor(elapsed / (365*24*60*60*1000))
            if(elapsed == 1) unit = 'year'
        } 
            
        return `${elapsed} ${unit}`
    }

    async getUserInformation (user_id) {
        try {
            const user = await User.find(user_id)      

            if(!user) return {status: 404}

            let user_type = user.type
            let user_information = null;
            let userData = null;

            if(user_type === 'researcher') {
                userData = await UserResearcher.findBy('user_id',user_id)
            } 
            else if(user_type === 'investor company') {
                userData = await UserInvestorCompany.findBy('user_id', user_id)
            }
            else if(user_type === 'investor individual') {
                userData = await UserInvestorIndividual.findBy('user_id', user_id)
            }
           
            if(userData) {
                user_information = userData.toJSON()
                user_information.name = user.name
            }                

            return {status:200, user_type, user_information}
        } catch (error) {
            throw error
        }
        
    }

    async isLoggedIn ({auth}) {
        try {
            await auth.check()
            return true
        } catch (error) {
            return false
        }
    }

    async saveUserToSession({auth, session}) {
        try {
            const user = await auth.getUser();

            const userData = await this.getUserInformation(user.id)                

            let userInfo = JSON.stringify(userData.user_information)
            
            session.put('user',JSON.stringify(user))
            session.put('user_info',userInfo)
            
            return {user, userData:userData.user_information}

        } catch (error) {
            throw error
        }
    }

    async isChatExist (users) {
        try {
            /* 
                SELECT room_id FROM `chat_members` 
                WHERE user_id IN ('b486625d-3a3b-4388-b29e-6a937372fe29','061f5f95-b9ae-44c6-bc0b-d7a7a8100417') 
                group by room_id having count(room_id)>1
            */ 
           
            if(!users) return false

            const room = (
                await Database.connection('chat')
                .raw(`SELECT room_id FROM chat_members WHERE user_id IN (?) group by room_id having count(room_id)>1 limit 1`, [users])
            )[0]

            let chat = null

            if(room.length) {
                chat = await Chat.findBy('content_id', room[0].room_id)

                if(chat) chat = chat.toJSON()
            }            

            return chat
            
        } catch (error) {
            throw error
        }
    }

    async deleteChats (chatIds) {
        try {
            //delete chat
            if(!chatIds.length) {
                return false
            }

            const chats = (
                await Chat.query()
                .whereIn('id',chatIds)
                .fetch()
            ).toJSON();

            let roomIds = chats.map(item=>{
                return item.content_id
            });

            await Chat.query()
            .whereIn('id', chatIds)
            .delete();

            let contentIds = []

            const chatContents = (
                await ChatContent.query()
                .whereIn('room_id',roomIds)
                .fetch()
            ).toJSON()

            contentIds = chatContents.map(item=>{
                return item.id
            })

            await ChatStatus.query()
            .whereIn('chat_content_id', contentIds)
            .delete()
            
            await ChatContent.query()
            .whereIn('room_id',roomIds)
            .delete();

            await ChatMember.query()
            .whereIn('room_id',roomIds)
            .delete();

            await Room.query()
            .whereIn('id',roomIds)
            .delete();

            return true

            /* const chat = await Chat.find(chat_id)
            let roomId = chat.content_id

            await chat.delete()

            const room = await Room.find(roomId)

            const chatContents = (
                await ChatContent.query()
                .where('room_id',roomId)
                .fetch()
            ).toJSON()

            let ids = []

            ids = chatContents.map(item=>{
                return item.id
            })

            await ChatStatus.query()
            .whereIn('chat_content_id', ids)
            .delete()

            await room.chatContents().delete();
            await room.chatMembers().delete(); 
            await room.delete();*/
            
        } catch (error) {
            throw error
        }
    }

}

module.exports = Services