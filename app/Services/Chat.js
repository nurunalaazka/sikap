const User = use('App/Models/User')
const ModelChat = use('App/Models/Chat')
const Room = use('App/Models/Room')
const ChatContent = use('App/Models/ChatContent')
const ChatMember = use('App/Models/ChatMember')
const UserResearcher = use('App/Models/UserResearcher')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const Services = new (use('App/Services/Services'))

class Chat {
    async loadChat (user_id) {
        try {            
            // console.log(user_id)
            const chatMember = (
                await ChatMember.query()
                .where('user_id',user_id)
                .with('chatStatuses',(item)=>{
                    item.where('read_status','unread')
                })
                .with('room')
                .orderBy('created_at','asc')
                .fetch()
            ).toJSON()            

            let i = chatMember.length

            let chat_list = []

            while(i--) {
                let chatItem = chatMember[i]
                let room_id = chatItem.room.id
                let read = true
                let chatContents = []

                /* 
                    recent chat
                */
                let recent_chat = (
                    await ChatContent.query()
                    .where('room_id',room_id)
                    .orderBy('created_at','desc')
                    .limit(1)
                    .fetch()
                ).toJSON()

                /*
                    elapsed time
                */
                
                let j = recent_chat.length

                while(j--) {
                    let item = recent_chat[j]
                    let elapsed_time = await Services.elapsedTime(item.created_at)
                    item.elapsed_time = elapsed_time
                    
                    chatContents.push(item)
                }                

                recent_chat = chatContents
                                
                /* 
                    read-status
                */               

                if(chatItem.chatStatuses.length) {
                    read = false
                }

                /* 
                    members data
                */

                const otherMembers = (
                    await ChatMember.query()
                    .where('room_id',room_id)
                    .whereNot('user_id',user_id)
                    .fetch()
                ).toJSON()
                
                let members = []

                for(const item of otherMembers) {
                    const temp = await User.find(item.user_id);

                    let user_type = temp.type
                    let userData;

                    if(user_type === 'researcher') {
                        userData = await UserResearcher.findBy('user_id',item.user_id)
                    } 
                    else if(user_type === 'investor company') {
                        userData = await UserInvestorCompany.findBy('user_id', item.user_id)
                    }
                    else if(user_type === 'investor individual') {
                        userData = await UserInvestorIndividual.findBy('user_id', item.user_id)
                    }

                    temp.userData = userData

                    members.push(temp)
                }


                let chat_teaser = 'Clik here to start chat..'
                if(recent_chat.length) chat_teaser = recent_chat[0].content

                let elapsed = 'just now'
                if(recent_chat.length) {
                    elapsed = recent_chat[0].elapsed_time
                } else {
                    elapsed = await Services.elapsedTime(chatItem.created_at)
                }                
                
                let chat_room = {
                    img: members[0].userData.profile_picture,
                    message: chat_teaser,
                    from: members[0].name,
                    created_at: elapsed,
                    read: read, 
                    room_id:room_id,
                    member_id:chatItem.id
                }

                chat_list.push(chat_room)

            }    
            // console.log(chatMember)     

            return  chat_list
        } catch (error) {
            throw error
        }
    }
}

module.exports = Chat