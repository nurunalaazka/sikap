const MdRole = use('App/Models/MdRole')
const MdRolePermission = use('App/Models/MdRolePermission')
const MdPermission = use('App/Models/MdPermission')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')
const MdIndustry = use('App/Models/MdIndustry')
const MdIndustryCategory = use('App/Models/MdIndustryCategory')
const MdResearchField = use('App/Models/MdResearchField')
const MdStudyArea = use('App/Models/MdStudyArea')
const MdBank = use('App/Models/MdBank')
const MdDegree = use('App/Models/MdDegree')


class Master {
    async getRoles (forHTML) {
        try {
            let roles = (
                await MdRole.all()
            ).toJSON()

            if(forHTML && roles.length) {
                roles = roles.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return roles

        } catch (error) {
            throw error
        }
    }    

    async getCountries (forHTML) {
        try {
            let countries = (
                await MdCountry.all()
            ).toJSON();

            if(forHTML && countries.length) {
                countries = countries.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return countries
        } catch (error) {
            throw error
        }
    }

    async getProvinces (forHTML) {
        try {
            let provinces = (
                // await MdProvince.all()
                await MdProvince.query().with('country').fetch()
            ).toJSON();

            if(forHTML && provinces.length) {
                provinces = provinces.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return provinces
        } catch (error) {
            throw error
        }
    }

    async getCities (forHTML) {
        try {
            let cities = (
                // await MdCity.all()
                await MdCity.query()
                .with('province')
                .with('country')
                .fetch()

            ).toJSON();

            if(forHTML && cities.length) {
                cities = cities.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return cities
        } catch (error) {
            throw error
        }
    }

    async getDistricts (forHTML) {
        try {
            let districts = (
                // await MdDistrict.all()
                await MdDistrict.query()
                .with('city')
                .with('province')
                .with('country')
                .fetch()

            ).toJSON();

            if(forHTML && districts.length) {
                districts = districts.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return districts
        } catch (error) {
            throw error
        }
    }

    async getIndustries (forHTML) {
        try {
            let industries = (
                await MdIndustry.all()
            ).toJSON();

            if(forHTML && industries.length) {
                industries = industries.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return industries
        } catch (error) {
            throw error
        }
    }

    async getIndustryCategories (forHTML) {
        try {
            let industryCategories = (
                // await MdIndustryCategory.all()
                await MdIndustryCategory.query()
                .with('industry').fetch()
            ).toJSON();

            if(forHTML && industryCategories.length) {
                industryCategories = industryCategories.map(item => {
                    return {value:item.id, label:item.name}
                })
            }

            return industryCategories
        } catch (error) {
            throw error
        }
    }

    async getResearchFields (forHTML) {
        try {
            let researchFields = (
                await MdResearchField.all()
            ).toJSON();
            
            if(forHTML) {
                researchFields = researchFields.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return researchFields
        } catch (error) {
            throw error
        }
    }

    async getStudyAreas (forHTML) {
        try {
            let studyAreas = (
                // await MdStudyArea.all()
                await MdStudyArea.query().with('researchField').fetch()
            ).toJSON(); 

            if(forHTML) {
                studyAreas = studyAreas.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return studyAreas
        } catch (error) {
            throw error
        }
    }

    async getPermissions (forHTML) {
        try {
            let permissions = (
                await MdPermission.all()
            ).toJSON(); 

            if(forHTML) {
                permissions = permissions.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return permissions 
        } catch (error) {
            throw error
        }
    }

    async getRolePermissions (forHTML) {
        try {
            let rolePermissions = (
                await MdRolePermission.all()
            ).toJSON(); 

            if(forHTML) {
                rolePermissions = rolePermissions.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return rolePermissions 
        } catch (error) {
            throw error
        }
    }

    async getBanks (forHTML) {
        try {
            let banks = (
                await MdBank.all()
            ).toJSON(); 

            if(forHTML) {
                banks = banks.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return banks 
            
        } catch (error) {
            throw error
        }
    }

    async getDegrees (forHTML) {
        try {
            let degrees = (
                await MdDegree.all()
            ).toJSON(); 

            if(forHTML) {
                degrees = degrees.map(item=>{
                    return {value:item.id, label:item.name}
                })
            }            

            return degrees 
            
        } catch (error) {
            throw error
        }
    }
}

module.exports = Master

