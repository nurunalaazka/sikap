class Props {
    async duration_unit () {
        const duration_unit = [
            {value:"day" ,label:'Day(s)'},
            {value:"week" ,label:'Week(s)'},
            {value:"month",label:'Month(s)'},
            {value:"year",label:'Year(s)'},
            {value:"decade",label:'Decade(s)'} ,
            {value:"centurie",label:'Centurie(s)'} ,
            {value:"millenium",label:'Millenium(s)'},
            {value:"eon",label:'Eon(s)'}
        ]

        return duration_unit
    }

    async accountTypes () {
        const accountTypes = [
            {value:"investor company",label:"Investor Company"},
            {value:"investor individual",label:"Investor Individual"},
            {value:"researcher",label:"Researcher"},
        ]

        return accountTypes
    }

    async userStatus () {
        const userStatus = [
            {value:"verified",label:"Verified"},
            {value:"unverified",label:"Unverified"},
            {value:"inactive",label:"Inactive"},
        ]

        return userStatus
    }

    async userInformationStatus () {
        const userInformationStatus = [
            {value:"verified",label:"Verified"},
            {value:"unverified",label:"Unverified"},
            {value:"rejected",label:"Rejected"},
        ]

        return userInformationStatus
    }
    
} 

module.exports = Props

