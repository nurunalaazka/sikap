'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response, session, view }) {
    // console.log('error name: '+error.name)
    // if(error.code !== 'E_ROUTE_NOT_FOUND') 
    console.log(error)

    if (error.name === 'ValidationException') {
      session.withErrors(error.messages).flashAll()
      
    }
    else if (error.name === 'PasswordMisMatchException') {      
      // session.withErrors(error.messages).flashAll()
      session.flash({ 
        content: 'The password mismatch',
        title: 'Password Mismatch',
        type: 'danger'
      })
      
    }
    else if(error.name ==='UserNotFoundException'){
      // session.flash({message:'User not found!'});
      session.flash({ 
        content: 'no user data match with this user',
        title: 'User not Found',
        type: 'danger'
      })
      
    }
    else if(error.name ==='InvalidSessionException'){
      // session.flash({message:'Your session has expired!'});
      if(request.ajax()) {
        return response.json({status:401, error:true, redirect:'/auth/login', message: 'unauthenticated user'})
      }

      session.flash({ 
        content: 'Your session has expired!',
        title: 'Session Expired',
        type: 'danger'
      })
      await session.commit()
      return response.redirect('/auth/login')
    }
    else if(error.code ==='E_GUEST_ONLY') {
      session.flash({ 
        content: 'You are already logged in!',
        title: 'Forbidden Access',
        type: 'warning'
      })
    }
    else if(error.code === 'E_ROUTE_NOT_FOUND') {
      // console.log(request.originalUrl())
      return response.redirect('/error/no-page')
    }
    else if(error.code === 'ER_ROW_IS_REFERENCED') {
      
      session.flash({content:'Cannot delete or update, the data is being used on another table', title:'Action Failed', type:'danger'})      
    }
    else if(error.code === 'ER_ROW_IS_REFERENCED_2') {
      
      session.flash({content:'Cannot delete or update, the data is being used on another table', title:'Action Failed', type:'danger'})      
    }
    else if(error.code === 'ER_DUP_ENTRY'){
      // session.flash({message:'Duplicate Entry'});
      session.flash({ 
        content: 'The data, that you are trying to save already exists',
        title: 'Duplicate Entry',
        type: 'danger'
      })
    }
    else if(error.code === 'EENVELOPE') {
      session.flash({ 
        content: 'No recipient email defined to receive this message.',
        title: 'No Recipient',
        type: 'danger'
      })
    }
    else{
      // session.flash({message:error});
      
      return super.handle(...arguments)
    }

    await session.commit()
    response.redirect('back')
    return    
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
