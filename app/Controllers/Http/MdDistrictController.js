'use strict'
const MdCity = use('App/Models/MdCity')
const MdProvince = use('App/Models/MdProvince')
const MdDistrict = use('App/Models/MdDistrict')

class MdDistrictController {
  async getDistricts ({ request, auth, response }) {

    const { id } = request.all()

    const city = await MdCity.find(id)
    const districts = await city.districts().fetch()
    
    return response.json({status:200, data:districts.toJSON()})
  }

  async create ({ request, view, response, auth, session }) {
    try {
      const { name, country_id, province_id, city_id } = request.all();

      await MdDistrict.create({ name, country_id, province_id, city_id })

      session.flash({ 
        content: 'New District has been successfully added',
        title:'Add Success',
        type: 'success'
      })
      return response.redirect('/admin/districts')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { district, name, country_id, province_id, city_id } = request.all()

      district.merge({name, country_id, province_id, city_id})

      await district.save()

      session.flash({ 
        content: 'The District has been successfully updated',
        title:'Update Success',
        type: 'success'
      })
      return response.redirect('/admin/districts')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { district } = request.all()

      await district.delete()

      session.flash({ 
        content: 'The district has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })
      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdDistrictController
