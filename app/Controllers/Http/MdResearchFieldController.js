'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const MdStudyArea = use('App/Models/MdStudyArea');

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')

class MdResearchFieldController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name } = request.all();

      await MdResearchField.create({ name })

      session.flash({ 
        content: 'New research field has been successfully added',
        title:'Add Success',
        type: 'success'
      })

      return response.redirect('/admin/research-fields')
      
    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { researchField, name } = request.all()

      researchField.merge({name})

      await researchField.save()

      session.flash({ 
        content: 'The research field has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/research-fields')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { researchField } = request.all()

      await researchField.delete()

      session.flash({ 
        content: 'The research field has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
    } catch (error) {
      
      throw error
    }
  }
  
}

module.exports = MdResearchFieldController
