'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdResearchField = use('App/Models/MdResearchField')
const MdBank = use('App/Models/MdBank')
const User = use('App/Models/User')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')
const Services = use('App/Services/Services')
const savePic = new Services().savePic

const Database = use('Database')
const Helpers = use('Helpers')

class UserInvestorIndividualController {
  async update ({ params, request, response, session, auth }) {
    try {
      const admin = await auth.getUser()
      const { 
        name, username, email, role_id, user_status, user_information_status, profile_picture_old,
        phone, province_id, city_id, district_id, address, industry_id, industry_category_id, gender,
        id, user
      } = request.all();

      let profile_picture = profile_picture_old || null     

      const profile_pic = request.file('profile_picture')      

      if(profile_pic) profile_picture = await savePic(profile_pic, '/uploads/profile_picture', `${id}_prof-pic.jpg`)

      let userInvestorIndividual = await user.userInvestorIndividual().fetch();

      if(userInvestorIndividual) {
        userInvestorIndividual.merge({
          phone, province_id, city_id, district_id, address, industry_id, industry_category_id, gender, status: user_information_status,
          profile_picture, modified_by:admin.id
        })
      }else{
        userInvestorIndividual = new UserInvestorIndividual()

        userInvestorIndividual.fill({
          phone, province_id, city_id, district_id, address, industry_id, industry_category_id, gender, status: user_information_status,
          profile_picture, created_by: admin.id
        })
      }
            
      user.merge({
        name, username, email, role_id, status: user_status, modified_by:admin.id
      })      

      const trx = await Database.beginTransaction();

      await user.save(trx);
      await user.userInvestorIndividual().save(userInvestorIndividual, trx)

      trx.commit();

      session.flash({content:'User data has been updated', type:'success', title:'Update Success'})
      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }
}

module.exports = UserInvestorIndividualController
