'use strict'

const MdRole = use('App/Models/MdRole');
const Encryption = use('Encryption')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')

class MdRoleController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { id, name, type } = request.all()

      await MdRole.create({
        id, name, type
      })

      session.flash({ 
        content: 'New role has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/roles')
    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { role_id, name, type, id } = request.all()

      const affectedRows = await Database
        .table('md_roles')
        .where('id', id)
        .update({ name: name, type:type, id:role_id })

      session.flash({ 
        content: 'The role has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/roles')
      
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { role } = request.all();

      await role.delete();

      session.flash({ 
        content: 'The role has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }

}

module.exports = MdRoleController
