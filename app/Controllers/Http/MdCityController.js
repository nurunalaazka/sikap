'use strict'
const MdCity = use('App/Models/MdCity')
const MdProvince = use('App/Models/MdProvince')

class MdCityController {
  async getCities ({ request, auth, response }) {

    const { id } = request.all()

    const province = await MdProvince.find(id)
    const cities = await province.cities().fetch()
    
    return response.json({status:200, data:cities.toJSON()})
  }

  async create ({ request, view, response, auth, session }) {
    try {
      const { name, country_id, province_id } = request.all();

      await MdCity.create({ name, country_id, province_id })

      session.flash({ 
        content: 'New city has been successfully added',
        title: 'Add City Success',
        type: 'success'
      })

      return response.redirect('/admin/cities')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { city, name, country_id, province_id } = request.all()

      city.merge({name, country_id, province_id})

      await city.save()

      session.flash({ 
        content: 'The city has been successfully updated',
        title: 'Update City Success',
        type: 'success'
      })

      return response.redirect('/admin/cities')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { city } = request.all()

      await city.delete()

      session.flash({ 
        content: 'The city has been successfully deleted',
        title: 'Delete City Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdCityController
