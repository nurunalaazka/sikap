'use strict'

const Project = use('App/Models/Project');
const ProjectMilestone = use('App/Models/ProjectMilestone');
const User = use('App/Models/User');
const Chat = use('App/Models/Chat');
const ProjectFunding = use('App/Models/ProjectFunding');
const Payment = use('App/Models/Payment');
const PaymentHistory = use('App/Models/PaymentHistory');
const Notification = use('App/Models/Notification');


const { v4: uuid } = require('uuid');;
const Database = use('Database');
const Encryption = use('Encryption');
const Helpers = use('Helpers');


class PaymentController {
  
  async index ({ request, response, view, auth }) {
    try{
      const user = await auth.getUser();

      const payments = async function(){
        if(user.type === 'researcher'){

          let temp = await User
          .query().where('users.id',user.id)          
          .with('researcherPayments', (builder) => {          
            builder.with('projectMilestone',(milestone) => {
              milestone.with('project')
            }).orderBy('created_at','desc')  
  
          }).fetch();

          return temp;
         
        }else{
  
          let temp = await User
          .query().where('users.id',user.id)          
          .with('investorPayments', (payment) => {          
            payment.with('projectMilestone', (milestone) => {
              milestone.with('project')
            }).orderBy('created_at','desc')  
  
          }).fetch();

          return temp;
         
        }
      } 

      const dataPayments = (await payments()).toJSON()
      

      // console.log(dataPayments[0].investorPayments[0].projectMilestone.project)
      // console.log(dataPayments[0].researcherPayments[0])

      return view.render('payment.index',{payments:dataPayments, user:user})

    }catch(e){
      throw e
    }
  }

  async showDetail ({ request, view, response, auth }) {
    try {
      const user = await auth.getUser();

      const { pay_id } = request.all();

      let ref = encodeURIComponent(Encryption.encrypt(pay_id));

      if(user.type === 'researcher') {
        return response.redirect('payments/pending?ref='+ref)

      }else{
        return response.redirect('payments/unpaid?ref='+ref)
      }
      
    } catch (error) {
      throw error
    }
  }

  
  async create ({ request, response, view, session, params }) {
    try {
      const { oref } = params;

      let offer_id = Encryption.decrypt(decodeURIComponent(oref));
      
      const projectFunding = await ProjectFunding.find(offer_id)
      // console.log(projectFunding.toJSON())

      const project = await Project.find(projectFunding.project_id)
      // console.log(project.toJSON())
      
      const milestones = await project.projectMilestones().fetch()

      // console.log(milestones.toJSON())

      let temp = milestones.toJSON()

      const data = temp.map(item => {
        return {
          id:uuid(), 
          project_id:project.id,
          project_milestone_id:item.id,
          status: 'unpaid',
          created_at: new Date(),
          updated_at: new Date()
        }
      })

      await Database
      .from('payments')
      .insert(data)

      // session.flash({ message: 'Transfer segera' });

      session.flash({ 
        content: 'Please transfer the fund before the time limit',
        title:'Invoice created',
        type: 'success'
      })
      return response.redirect('/funding/funding-offers');
      
    } catch (error) {
      throw error
    }    
  }
  
  async store ({ request, response }) {
  }

  
  async showUnpaid ({ params, request, response, view, auth }) {
    try {
      const user = await auth.getUser();

      const { ref } = request.all();

      const paymentId = Encryption.decrypt(ref);

      const payment = await Payment.find(paymentId);

      const project = await Project
      .query().where('projects.id',payment.project_id)
      .with('projectMilestones', (milestone) => {
        milestone.where('id',payment.project_milestone_id)
      })      
      .with('researchField')
      .with('studyArea')
      .with('researcher',(item)=>{
        item.with('userResearcher')
      })
      .fetch();

      let researcher_id = (project.toJSON())[0].researcher.id
      
      const researcher = await User.find(researcher_id)
      
      const bankAccount = await researcher.userBankAccount().fetch();
      const bank = await bankAccount.bank().fetch()

      // console.log(project.toJSON())

      return view.render('payment.payment-unpaid',{
        project:project.toJSON(), user:user,
        bankAccount:bankAccount.toJSON(),
        bank:bank.toJSON()
      });
      
    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser();

      const { ref } = request.all();

      const payment_id = Encryption.decrypt(ref);

      //get payment receipt picture
      const receiptPic = request.file('receipt');

      const receiptName = new Date()-0 +'_'+ receiptPic.toJSON().clientName
      const receiptPath = `/uploads/payment-receipt/${payment_id}`
      
      await receiptPic.move(Helpers.publicPath(receiptPath), {
        name: receiptName,
        overwrite: true
      })

      const receipt = `${receiptPath}/${receiptName}`

      //insert payment history
      const payment = await Payment.find(payment_id);
      const milestone = await payment.projectMilestone().fetch()
      const project = await milestone.project().fetch()
      
      const paymentHistory = new PaymentHistory();

      payment.merge({
        status: 'pending',
        receipt
      })      

      paymentHistory.fill({
        status:'pending',
        receipt
      });

      const trx = await Database.beginTransaction();

      await payment.paymentHistories().save(paymentHistory, trx);
      await payment.save(trx);

      await Notification.create({
        user_id : project.researcher_id,
        title:`${user.name} has transferred the payment term ${milestone.order} for the following research:`,
        content:project.title,
        thumbnail:'/img/notifications/payment_received.svg',
        action_link:'/payments/pending?ref='+encodeURIComponent(ref),
        action_text:'confirm payment here',
        status:'unread',
        created_by:user.id
      },trx)

      trx.commit();

      session.flash({ 
        content: 'The payment has been successfully made',
        title:'Payment Success',
        type: 'success'
      })

      return response.redirect('pending?ref='+encodeURIComponent(ref))
      //update status payment

    } catch (error) {
      throw error
    }
  }

  async showPending ({ request, view, response, auth }) {
    try {
      const user = await auth.getUser();

      const { ref } = request.all()

      const payment_id = Encryption.decrypt(ref);

      const payment = await Payment.find(payment_id);

      const project = await Project
      .query().where('projects.id',payment.project_id)
      .with('projectMilestones', (milestone) => {
        milestone.where('id',payment.project_milestone_id)
      })      
      .with('researchField')
      .with('studyArea')
      .with('investor')
      .with('researcher',(item)=>{
        item.with('userResearcher')
      })
      .fetch();

      // console.log(project.toJSON())
      return view.render('payment.payment-pending',{project:project.toJSON(), user:user, payment})
      
    } catch (error) {
      throw error
    }
  }

  async updateToPaid ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser();

      const { ref } = request.all()

      const payment_id = Encryption.decrypt(ref);

      const payment = await Payment.find(payment_id);

      const milestone = await payment.projectMilestone().fetch()
      const project = await payment.project().fetch()

      payment.merge({
        status: 'paid'
      });

      const paymentHistory = new PaymentHistory();

      paymentHistory.fill({
        status:'paid',
        receipt:payment.receipt
      });

      milestone.merge({
        status: 'on going'
      })

      const trx = await Database.beginTransaction();

      await payment.paymentHistories().save(paymentHistory, trx);
      await payment.save(trx);
      await milestone.save(trx)

      await Notification.create({
        user_id : project.investor_id,
        title:`${user.name} has received your payment for term ${milestone.order} on the following research:`,
        content:project.title,
        thumbnail:'/img/notifications/payment_approved.svg',
        action_link:'/payments/paid?ref='+encodeURIComponent(ref),
        action_text:'view detail',
        status:'unread',
        created_by:user.id
      },trx)
      

      trx.commit();

      session.flash({ 
        content: 'You have received the payment',
        title:'Receive Payment Success',
        type: 'success'
      })

      return response.redirect('paid?ref='+encodeURIComponent(ref))

    } catch (error) {
      throw error
    }
  }

  async showPaid ({ request, view, response, auth }) {
    try {
      const user = await auth.getUser();

      const { ref } = request.all();

      const payment_id = Encryption.decrypt(ref);

      const payment = await Payment.find(payment_id);

      const project = await Project
      .query().where('projects.id',payment.project_id)
      .with('projectMilestones', (milestone) => {
        milestone.where('id',payment.project_milestone_id)
      })      
      .with('researchField')
      .with('studyArea')
      .with('investor')
      .with('researcher',(item)=>{
        item.with('userResearcher')
      })
      .fetch();      

      return view.render('payment.payment-paid',{user:user, project:project.toJSON(), payment})

    } catch (error) {
      throw error
    }
  }

  async redirectToUnpaid ({ request, view, response, auth }) {
    try {
      const { milestone } = request.all();

      const id = Encryption.decrypt(milestone);

      const payment = await Payment.findBy('project_milestone_id',id);

      const ref = encodeURIComponent(Encryption.encrypt(payment.id));

      return response.redirect('/payments/unpaid?ref='+ref);
      
    } catch (error) {
      throw error
    }
  }

  async redirectToDetail ({ request, view, response, auth }) {
    try {
      const { payment } = request.all()
      console.log(payment);
      if(!payment) return response.redirect('back')
      
      const pay_detail = await Payment.find(payment)

      if(!pay_detail) return response.redirect('back')

      const ref = encodeURIComponent(Encryption.encrypt(pay_detail.id))

      if(pay_detail.status === 'unpaid') {
        return response.redirect('/payments/unpaid?ref='+ref);

      } else if( pay_detail.status === 'pending') {
        return response.redirect('/payments/pending?ref='+ref);

      } else if( pay_detail.status === 'paid') {
        return response.redirect('/payments/paid?ref='+ref);

      }

    } catch (error) {
      throw error
    }
  }

  async filterStatus ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()

      const { status } = request.all()  
      
      let payments = []
      let data = []

      if(user.type === 'researcher') {
        payments = (
          await User.query()
          .where('id',user.id)
          .with('researcherPayments',(item)=>{
            item.where('payments.status',status)
          })
          .fetch()
        ).toJSON()

        if(payments.length) data = payments[0].researcherPayments

      }else{
        payments = (
          await User.query()
          .where('id',user.id)
          .with('investorPayments',(item)=>{
            item.where('payments.status',status)
          })
          .fetch()
        ).toJSON()

        if(payments.length) data = payments[0].investorPayments

      }      
      
      return response.json({status:200, data})
    } catch (error) {
      throw error
    }
  }

  
}

module.exports = PaymentController
