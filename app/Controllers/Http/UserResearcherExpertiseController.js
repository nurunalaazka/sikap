'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserResearcherResearch = use('App/Models/UserResearcherResearch')
const UserResearcherEducation = use('App/Models/UserResearcherEducation')
const UserResearcherExperience = use('App/Models/UserResearcherExperience')
const UserResearcherExpertise = use('App/Models/UserResearcherExpertise')
const Services = new (use('App/Services/Services'))

const Database = use('Database')
const Helpers = use('Helpers')
const { v4: uuid } = require('uuid');
const Encryption = use('Encryption')
const Hash = use('Hash')

class UserResearcherExpertiseController {
  async delete ({ request, view, response, auth, session }) {
    try {
      const { expertise } = request.all()

      let id = Encryption.decrypt(expertise)

      await UserResearcherExpertise
      .query()
      .where('id',id)
      .delete()

      session.flash({ 
        content: 'Your expertise information has been successfully deleted.', title:'Delete Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
}

module.exports = UserResearcherExpertiseController
