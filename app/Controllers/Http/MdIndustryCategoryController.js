'use strict'
const MdIndustryCategory = use('App/Models/MdIndustryCategory')

class MdIndustryCategoryController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name, industry_id } = request.all();

      await MdIndustryCategory.create({ name, industry_id })

      session.flash({ 
        content: 'New industry has been successfully created',
        title:'Create Success',
        type: 'success'
      })
      return response.redirect('/admin/industry-categories')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { industryCategory, name, industry_id } = request.all()

      industryCategory.merge({name, industry_id})

      await industryCategory.save()

      session.flash({ 
        content: 'The industry has been successfully updated',
        title:'Update Success',
        type: 'success'
      })
      return response.redirect('/admin/industry-categories')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { industryCategory } = request.all()

      await industryCategory.delete()

      session.flash({ 
        content: 'The industry has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })
      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }

  async getIndustryCategories ({ request, view, response, auth }) {
    try {
      const { industry_id } = request.all()

      if(!industry_id) return response.json({error:true, status:400, message: 'bad request'})

      const data = (
        await MdIndustryCategory.query()
        .where('industry_id', industry_id)
        .fetch()

      ).toJSON()

      return response.json({status:200, data})
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdIndustryCategoryController
