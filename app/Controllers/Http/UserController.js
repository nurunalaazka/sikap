'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdResearchField = use('App/Models/MdResearchField')
const MdBank = use('App/Models/MdBank')
const User = use('App/Models/User')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')

const Database = use('Database')
const Helpers = use('Helpers')

class UserController {
    async userInformationForm ({auth, view, request, response}) {
        try{
            const user = await auth.getUser();

            const { account } = request.all()

            const banks = await MdBank.all()  

            if(account == 'researcher'){
                const researchFields = await MdResearchField.all()                         
                
                return view.render('auth.register-information',{
                    user:user, 
                    researchFields:researchFields.toJSON(),
                    banks:banks.toJSON()
                })
            }
            else if(account == 'investor-individual'){
                const countries = await MdCountry.all()
                const provinces = await MdProvince.all()
                return view.render('auth.register-information',{
                    user:user, 
                    countries:countries.toJSON(),
                    provinces:provinces.toJSON(),
                    banks:banks.toJSON()
                })
            }
            else if(account == 'investor-company'){
                const countries = await MdCountry.all()
                const provinces = await MdProvince.all()
                return view.render('auth.register-information',{
                    user:user, 
                    countries:countries.toJSON(),
                    provinces:provinces.toJSON(),
                    banks:banks.toJSON()
                })
            }else{
                console.log('ini')
                return response.redirect('back')
            }
            
            

        }catch(e){            
            throw e
        }        
    }

    async createUserInformation ({ response, auth, request, view }) {
        try{
            const user = await auth.getUser();
            
            let pp_path = null
            let id_path = null            

            const pp_name = `${user.id}_prof-pic.jpg`
            const id_name = `${user.id}_id-card.jpg`

            const profile_picture = request.file('profile_pic')
            
            const id_card = request.file('id_card')
            
            if(profile_picture){
                pp_path = '/uploads/profile_picture'

                await profile_picture.move(Helpers.publicPath(pp_path), {
                    name: pp_name,
                    overwrite: true
                })

                pp_path = `${pp_path}/${pp_name}`
            }

            if(id_card){
                id_path = '/uploads/id_card'

                await id_card.move(Helpers.publicPath(id_path), {
                    name: id_name,
                    overwrite: true
                })

                id_path = `${id_path}/${id_name}`
            }            
            //.publicPath or .tmpPath        

            // console.log(Helpers.publicPath('uploads/idcard'))

            request.pp_path = pp_path
            request.id_path = id_path

            if(user.type == 'researcher'){
                return this.createResearcher(...arguments)
            }
            else if(user.type == 'investor individual'){
                return this.createInvestorIndividual(...arguments)
            }
            else if(user.type == 'investor company'){
                return this.createInvestorCompany(...arguments)
            }            

        }catch(e){
            throw e            
        }    
    }

    async createResearcher ({ response, auth, request, view }) {
        try{
            const user = await auth.getUser(); 

            const { gender, institution, position, scopus_index, google_scholar_link,
                acc_number, acc_holder, bank_address, bank, research_field, study_area } = request.all()

            const pp_path = request.pp_path
            const id_path = request.id_path

            let userResearcher = await UserResearcher.findBy('user_id',user.id)
            let userBankAccount = await UserBankAccount.findBy('user_id',user.id)            
            
            if(userResearcher){                
                userResearcher.merge({
                    gender, institution, position,
                    scopus_index, google_scholar_link, research_field_id: research_field,
                    study_area_id:study_area, profile_picture:pp_path, id_card:id_path,
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.merge({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }else{
                userResearcher = new UserResearcher();
                userBankAccount = new UserBankAccount();
                
                userResearcher.fill({
                    gender, institution, position,
                    scopus_index, google_scholar_link, research_field_id: research_field,
                    study_area_id:study_area, profile_picture:pp_path, id_card:id_path,
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.fill({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }
            

            const trx = await Database.beginTransaction();

            await user.userResearcher().save(userResearcher, trx)
            await user.userBankAccount().save(userBankAccount, trx)

            trx.commit();

            return response.redirect('/signup/register-success');

        }catch(e){
            throw e
        }
        
    }

    async createInvestorIndividual ({ response, auth, request, view }) {
        try{
            const user = await auth.getUser();
            // console.log(request.all()) 

            const { gender, phone, abroad, province, city, district, address,
                acc_number, acc_holder, bank_address, bank } = request.all()

            const pp_path = request.pp_path

            let userInvestorIndividual = await UserInvestorIndividual.findBy('user_id',user.id)
            let userBankAccount = await UserBankAccount.findBy('user_id',user.id)            
            
            if(userInvestorIndividual){                
                userInvestorIndividual.merge({
                    gender, phone, province_id:province, city_id:city, district_id:district, address, profile_picture:pp_path, 
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.merge({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }else{
                userInvestorIndividual = new UserInvestorIndividual();
                userBankAccount = new UserBankAccount();
                
                userInvestorIndividual.fill({
                    gender, phone, province_id:province, city_id:city, district_id:district, address, profile_picture:pp_path, 
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.fill({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }
            

            const trx = await Database.beginTransaction();

            await user.userInvestorIndividual().save(userInvestorIndividual, trx)
            await user.userBankAccount().save(userBankAccount, trx)

            trx.commit();

            return response.redirect('/signup/register-success');

        }catch(e){
            throw e
        }
    }

    async createInvestorCompany ({ response, auth, request, view }) {
        try{
            const user = await auth.getUser(); 
            console.log(request.all())

            const { chairman, pic, phone, abroad, province, city, district, address,
                acc_number, acc_holder, bank_address, bank } = request.all()

            const pp_path = request.pp_path

            let userInvestorCompany = await UserInvestorCompany.findBy('user_id',user.id)
            let userBankAccount = await UserBankAccount.findBy('user_id',user.id)            
            
            if(userInvestorCompany){                
                userInvestorCompany.merge({
                    chairman, pic, phone, province_id:province, city_id:city, district_id:district, address, profile_picture:pp_path, 
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.merge({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }else{
                userInvestorCompany = new UserInvestorCompany();
                userBankAccount = new UserBankAccount();
                
                userInvestorCompany.fill({
                    chairman, pic, phone, province_id:province, city_id:city, district_id:district, address, profile_picture:pp_path, 
                    created_by:user.id, status: 'unverified'
                })

                userBankAccount.fill({
                    number:acc_number, holder:acc_holder,
                    bank_address, bank_id:bank,
                    created_by:user.id
                })
            }
            

            const trx = await Database.beginTransaction();

            await user.userInvestorCompany().save(userInvestorCompany, trx)
            await user.userBankAccount().save(userBankAccount, trx)

            trx.commit();

            return response.redirect('/signup/register-success');

        }catch(e){
            throw e
        }
    }

    async delete ({ request, view, response, auth, session }) {
        try {
            const { user } = request.all()

            user.merge({
                status: 'inactive'
            })

            await user.save()

            session.flash({ 
                content: 'The user has been successfully inactivated',
                title: 'Delete Success',
                type: 'success'
            })

            return response.redirect('back')
        } catch (error) {
            throw error
        }
    }

    async isExists ({ request, response }) {
        try{
            const { username, email } = request.all()
            let col = 'username'
            let data = username || email            
            if(!username){
                col = 'email'
            }

            const checkData = await User.findBy(col,data)

            const found = checkData?true:false


            return response.json({status:200, found:found})
        }catch(e){
            throw e
        }        
    }

    async findAccount ({ request, response }) {
        try{
            const { keyword } = request.all();                       
            
            let checkEmail;
            let checkUsername;
            let account;            

            if(keyword.includes('@')){
                checkEmail = await User.query().where('email', 'like', `%${keyword}%`).limit(5).fetch()
                
                if(checkEmail){
                    account = checkEmail.toJSON().map(item=>{
                        return item.email
                    })
                }
            }

            if( (checkEmail && !checkEmail.toJSON().length) || !checkEmail){
                checkUsername = await User.query().where('username', 'like', `%${keyword}%`).limit(5).fetch()                

                if(checkUsername.toJSON().length){                    
                    account = checkUsername.toJSON().map(item=>{
                        return item.username
                    })

                }else{                    
                    checkEmail = await User.query().where('email', 'like', `%${keyword}%`).limit(5).fetch()
                
                    if(checkEmail){
                        account = checkEmail.toJSON().map(item=>{
                            return item.email
                        })
                    }
                }

            }
            
            
            if(!checkEmail&&!checkUsername){
                return response.json({status:200, account:null})
            }else{
                
                return response.json({status:200, account:account})
            }            
            
        }catch(e){
            throw e
        }     
    }

    async getAccount ({ request, response }) {
        try{
            const { key } = request.all();           
            
            let account;            

            if(key.includes('@')){
                account = await User.findBy('email',key);                
            }

            if( (account && !account.toJSON().length) || !account){
                account = await User.findBy('username',key);                
            }
            
            if(!account){
                return response.json({status:200, account:null})
            }else{
                let accData = await account.userResearcher().fetch();
                account.position = accData.position
                account.institution = accData.institution
                
                return response.json({status:200, account:account.toJSON()})
            }            
            
        }catch(e){
            throw e
        }     
    }

}

module.exports = UserController
