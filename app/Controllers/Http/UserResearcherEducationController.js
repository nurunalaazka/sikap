'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserResearcherResearch = use('App/Models/UserResearcherResearch')
const UserResearcherEducation = use('App/Models/UserResearcherEducation')
const UserResearcherExperience = use('App/Models/UserResearcherExperience')
const UserResearcherExpertise = use('App/Models/UserResearcherExpertise')
const Services = new (use('App/Services/Services'))


const Database = use('Database')
const Helpers = use('Helpers')
const { v4: uuid } = require('uuid');
const Encryption = use('Encryption')
const Hash = use('Hash')

class UserResearcherEducationController {
  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()
      const { 
        id, university, faculty, major, degree_id, gpa, max_gpa, location, study_start, study_end, description
      } = request.all()

      let dateStart = await Services.toSqlDate(study_start)
      let dateEnd = await Services.toSqlDate(study_end)

      const education = await UserResearcherEducation.find(id)

      education.merge({
        university, faculty, major, degree_id, gpa, max_gpa,
        location, date_start: dateStart, date_end: dateEnd, description,
        modified_by: user.id
      })

      await education.save()

      session.flash({ 
        content: 'Your education information has been successfully edited.', title:'Edit Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { education } = request.all()

      let id = Encryption.decrypt(education)

      await UserResearcherEducation
      .query()
      .where('id',id)
      .delete()

      session.flash({ 
        content: 'Your education information has been successfully deleted.', title:'Delete Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
}

module.exports = UserResearcherEducationController
