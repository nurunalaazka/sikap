'use strict'

const User = use('App/Models/User')
const ProjectFunding = use('App/Models/ProjectFunding')
const ChatContent = use('App/Models/ChatContent')
const Chat = use('App/Models/Chat')
const ChatMember = use('App/Models/ChatMember')
const ChatStatus = use('App/Models/ChatStatus')
const Room = use('App/Models/Room')
const { v4: uuid } = require('uuid');
const Services = new (use('App/Services/Services'))
const ChatService = new (use('App/Services/Chat'))

const Database = use('Database')

class ChatController {
  async refreshRooms ({ request, view, response, auth }) {
    try {
      const { user_id } = request.all()

      const chat_list = await ChatService.loadChat(user_id)

      return response.json({status:200, data:chat_list})
    } catch (error) {
      throw error
    }
  }

  async getChatContents ({ request, view, response, auth }) {
    try {
      const { id, member_id } = request.all()
      let messages = []

      const room = await Room.find(id)
      if(!room) return response.json({status:404})

      //name
      const member = (
        await ChatMember.query()
        .where('room_id',id)
        .whereNot('id',member_id)
        .fetch()
      ).toJSON()[0]      

      const userInfo = await Services.getUserInformation(member.user_id)
      
      if(userInfo.status !== 200) return response.json({status: 404})

      const userInformation = userInfo.user_information

      //chat
      const chats = (
        await room.chatContents().orderBy('created_at','asc').fetch()
      ).toJSON()

      let i = chats.length

      while(i--) {
        let chat = chats[i]
        let as_sender = false        

        if(chat.sender_id == member_id) as_sender = true

        let elapsed = await Services.elapsedTime(chat.created_at);

        messages.push({
          message:chat.content,
          created_at:elapsed,
          as_sender:as_sender
        })

      }

      // console.log(messages)      
      
      return response.json({status:200, data:messages, userInformation})

    } catch (error) {
      throw error
    }
  }

  async setStatus ({ request, view, response, auth }) {
    const { user_id, room } = request.all()    

    const member = (
      await ChatMember.query()
      .where('room_id',room)
      .where('user_id',user_id)
      .fetch()
    ).toJSON()

    if(!member.length) return response.json({status:404})

    const member_id = member[0].id

    await Database.table('chat.chat_statuses')
    .where('chat_member_id',member_id)
    .where('read_status','unread')
    .update({read_status:'read'})

    return response.json({status:200})
  }

  async getRoomId ({ request, view, response, auth }) {
    try {
      const { projectId } = request.all()

      if(!projectId) return response.json({status:400, error:true, message:'bad request, missing id.'})

      const fundingOffer = (
        await ProjectFunding.query()
        .where('project_id',projectId).andWhere('status','completed')
        .fetch()
      ).toJSON()

      if(!fundingOffer.length) return response.json({status:404, error:true, message:'chat room not found'})

      let chat = null
      let roomId = ''

      if(fundingOffer.length) chat = await Chat.find(fundingOffer[0].chat_id)

      if(chat) roomId = chat.content_id

      const data = {
        roomId
      }

      return response.json({status:200, data})
    } catch (error) {
      throw error
    }
  }
  
}

module.exports = ChatController
