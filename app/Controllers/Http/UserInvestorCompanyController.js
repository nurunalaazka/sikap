'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdResearchField = use('App/Models/MdResearchField')
const MdBank = use('App/Models/MdBank')
const User = use('App/Models/User')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')
const Services = use('App/Services/Services')
const savePic = new Services().savePic

const Database = use('Database')
const Helpers = use('Helpers')

class UserInvestorCompanyController {
  async update ({ request, view, response, auth, session }) {
    try {
      const admin = await auth.getUser()
      const {
        name, username, email, role_id, user_status, user_information_status, profile_picture_old,
        chairman, pic, phone, province_id, city_id, district_id, address, industry_id, industry_category_id,
        user, id
      } = request.all();

      let profile_picture = profile_picture_old || null  

      const profile_pic = request.file('profile_picture')      

      if(profile_pic) profile_picture = await savePic(profile_pic, '/uploads/profile_picture', `${id}_prof-pic.jpg`)  
      
      let userInvestorCompany = await user.userInvestorCompany().fetch(); 
      
      if(userInvestorCompany) {
        userInvestorCompany.merge({
          chairman, pic, phone, province_id, city_id, district_id, address, industry_id, industry_category_id,
          profile_picture, status: user_information_status, modified_by:admin.id
        })
      } else {
        userInvestorCompany = new UserInvestorCompany()

        userInvestorCompany.fill({
          chairman, pic, phone, province_id, city_id, district_id, address, industry_id, industry_category_id,
          profile_picture, status: user_information_status, created_by: admin.id
        })
      }
      
      user.merge({
        name, username, email, role_id, status: user_status, modified_by:admin.id
      })      

      const trx = await Database.beginTransaction();

      await user.save(trx);
      await user.userInvestorCompany().save(userInvestorCompany, trx)

      trx.commit();
      
      /* 
        status: success, warning, info, danger
      */
      session.flash({content:'User data has been updated', type:'success', title:'Update Success'})
      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }
}

module.exports = UserInvestorCompanyController
