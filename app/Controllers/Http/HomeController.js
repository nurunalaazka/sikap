'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const MdStudyArea = use('App/Models/MdStudyArea');
const ResearchNeed = use('App/Models/ResearchNeed')
const Project = use('App/Models/Project');
const Product = use('App/Models/Product')
const User = use('App/Models/User');
const ProjectMember = use('App/Models/ProjectMember')
const Encryption = use('Encryption')
const ChatContent = use('App/Models/ChatContent')
const ChatService = new (use('App/Services/Chat'))
const Services = new (use('App/Services/Services'))
const Mail = use('Mail');


const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Env = use('Env')

class HomeController {
    async index ({ auth, params, response, request, view, session}) {        
        try {
            // const { filter, research_field_id, study_area_id, status, type, keywords } = request.all(); 
            // console.log('homee', request.headers())
            let user, userInfo;  

            let research_field_id,study_area_id,status,type,keywords = "";
            
            // console.log(session.all())
            
            // const chat_list = await ChatService.loadChat(user.id)

            let projects = []
            let products = []
            let fundedProjects = [];


            projects = (await Project.query()
            .where('status','published')
            // .with('researchField')
            // .with('studyArea')
            .with('investor')
            .with('researcher')
            .fetch()).toJSON();

            fundedProjects = (await Project.query()
            .where('status','ongoing')
            // .with('researchField')
            // .with('studyArea')
            .with('investor')
            .with('researcher')
            .fetch()).toJSON();

            products = (await Product.query()
            .whereNot('status','draft')
            // .with('investor')
            .with('researcher')
            .fetch()).toJSON();

            const researchFields = (await MdResearchField.all()).toJSON();
            const studyAreas = (await MdStudyArea.all()).toJSON();
            const types = ['projects','products']
            const statuses = ['published','ongoing','completed']
            

            return view.render('home',{
                projects, products, researchFields, studyAreas, types, statuses, 
                research_field_id, study_area_id, status, type, keywords, user, fundedProjects
                // , chat_list
            });

        } catch (error) {
            throw error
        }          
        
    }

    async explore ({ request, view, response, auth, session }) {
        try {
            const { filter, research_field_id, study_area_id, status, type, keywords } = request.all();

            let user, userInfo;            

            // const chat_list = await ChatService.loadChat(user.id)
            let projects = []
            let products = []
            let fundedProjects = [];
            // let research_field_id,study_area_id,status,type,keywords = "";           
            

            if(filter) {
                
                const tmp = await this.filter(...arguments)
                projects = tmp.projects
                products = tmp.products
                
            } else {
                projects = (await Project.query()
                .whereNot('status','draft')
                // .with('researchField')
                // .with('studyArea')
                .with('investor')
                .with('researcher')
                .fetch()).toJSON();

                products = (await Product.query()
                .whereNot('status','draft')
                // .with('investor')
                .with('researcher')
                .fetch()).toJSON();
            }
            
            const researchFields = (await MdResearchField.all()).toJSON();
            const studyAreas = (await MdStudyArea.all()).toJSON();
            const types = ['projects','products']
            const statuses = ['published','ongoing','completed']
            

            return view.render('research.explore',{
                projects,fundedProjects, products, researchFields, studyAreas, types, statuses, 
                research_field_id, study_area_id, status, type, keywords, user
                // , chat_list
            });

        } catch (error) {
            throw error
        }
    }

    async filter ({ request, view, response, auth }) {
        try {
            const { research_field_id, study_area_id, status, type, keywords} = request.all();
            let tmp_projects = []
            let tmp_products = []

            if(keywords && keywords.length >= 3) {
                
                let searchResults = await this.search(...arguments);
                tmp_projects = searchResults.projects;
                tmp_products = searchResults.products;
            }
            else {
                tmp_projects = (await Project.query()
                .whereNot('status','draft')
                .with('researchField')
                .with('studyArea')
                .with('investor')
                .with('researcher')
                .fetch()).toJSON();

                tmp_products = (await Product.query()
                .whereNot('status','draft')
                .with('researchField')
                .with('studyArea')                
                .with('researcher')
                .fetch()).toJSON();
            }
            

            let projects = []
            let products = []            

            if(type && type == 'products') {
                await filterProducts(tmp_products)

            } else if(type && type == 'projects'){
                await filterProjects(tmp_projects)
            } else {
                await filterProducts(tmp_products) 
    
                await filterProjects(tmp_projects)
            }

            async function filterProducts (data) {
                data.forEach(item=>{
                    if(research_field_id && (item.research_field_id !== research_field_id)) return false
                    if(study_area_id && (item.study_area_id !== study_area_id)) return false
                    if(status && (item.status !== status)) return false                
                    products.push(item)
                })

                return true
            }

            async function filterProjects (data) {
                data.forEach(item=>{
                    if(research_field_id && (item.research_field_id !== research_field_id)) return false
                    if(study_area_id && (item.study_area_id !== study_area_id)) return false
                    if(status && (item.status !== status)) return false                
                    projects.push(item)
                }) 

                return true
            }           
            
            return {projects, products}
            
        } catch (error) {
            throw error
        }
    }


    async search ({ request, view, response, auth }) {
        try {
            const { keywords } = request.all();

            let products = []
            let projects = []

            let attemptTitle = []
            let attemptTitleProd = []
            let attemptResearch = []
            let attemptStudy = []
            let attemptResearcher = []

            const results = async () => {

                attemptTitle = await Project.query()                
                .where('title','LIKE',`%${keywords}%`).andWhereNot({'status':'draft'})
                .orWhere('sub_title','LIKE',`%${keywords}%`).andWhereNot({'status':'draft'})                
                .with('researchField')
                .with('studyArea')
                .with('investor')
                .with('researcher')
                .fetch();

                attemptTitleProd = await Product.query()                
                .where('title','LIKE',`%${keywords}%`).andWhereNot({'status':'draft'})
                .orWhere('sub_title','LIKE',`%${keywords}%`).andWhereNot({'status':'draft'})                
                .with('researchField')
                .with('studyArea')
                .with('researcher')
                .fetch();

                if(attemptTitle.toJSON().length || attemptTitleProd.toJSON().length) {
                    console.log('title/subtitle',keywords)
                    console.log(attemptTitle.toJSON())
                    return { projects: attemptTitle.toJSON(), products: attemptTitleProd.toJSON() }
                } 

                if(!attemptTitle.toJSON().length){
                    attemptResearch = await MdResearchField.query()
                    .where('name','like',`%${keywords}%`)
                    .with('projects',(item)=>{
                        item
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('investor')
                        .with('researcher')
                    }) 
                    .with('products',(product)=>{
                        product
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('researcher')
                    })               
                    .fetch();

                    if(attemptResearch.toJSON().length) {
                        console.log('research field',keywords)

                        return { projects: attemptResearch.toJSON()[0].projects, products: attemptResearch.toJSON()[0].products }
                    } 
                } 

                if(!attemptTitle.toJSON().length) {
                    attemptStudy = await MdStudyArea.query()
                    .where('name','like',`%${keywords}%`)
                    .with('projects', (item)=>{
                        item
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('investor')
                        .with('researcher')
                    })      
                    .with('products',(product)=>{
                        product
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('researcher')
                    })          
                    .fetch();

                    if(attemptStudy.toJSON().length) {
                        console.log('study area',keywords)

                        return { projects: attemptStudy.toJSON()[0].projects, products: attemptStudy.toJSON()[0].products }
                    } 
                }

                if(!attemptTitle.toJSON().length) {
                    attemptResearcher = await User.query()
                    .where('name','like',`%${keywords}%`)
                    .with('projects', (item)=>{
                        item
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('investor')
                        .with('researcher')
                    })       
                    .with('products',(product)=>{
                        product
                        .whereNot('status','draft')
                        .with('researchField')
                        .with('studyArea')
                        .with('researcher')
                    })          
                    .fetch();

                    if(attemptResearcher.toJSON().length) {
                        console.log('researcher',keywords)

                        return { projects: attemptResearcher.toJSON()[0].projects, products: attemptResearcher.toJSON()[0].products }
                    } 
                }

                return null
            }            
            
            const temp = await results();

            if(temp) {
                projects = temp.projects
                products = temp.products
            }

            return {products,projects}
            
        } catch (error) {
            throw error
        }
    }

    async searchNavbar ({ request, view, response, auth }) {
        try {
            const { keywords } = request.all()

            return response.redirect('/explore?filter=true&keywords='+encodeURIComponent(keywords))
        } catch (error) {
            throw error
        }
    }

    async redirectToDetail ({ request, view, response, auth }) {
        try {
            const { type, id } = request.all();            

            if(type == 'project') {
                const project = await Project.find(id);

                if(!project) return response.json({status:404, error: 'Project not found'})

                return response.redirect('/project/detail?ref='+encodeURIComponent(Encryption.encrypt(id)))

            } 
            else if (type == 'product') {
                const product = await Product.find(id);

                if(!product) return response.json({status:404, error: 'Product not found'})

                return response.redirect('/product/detail?ref='+encodeURIComponent(Encryption.encrypt(id)))

            }
            else {
                return response.json({status:404, error: 'Invalid research type'})
            }
            
        } catch (error) {
            throw error
        }
    }

    async sendContactUs ({ request, view, response, auth, session }) {
        try {
            const { name, email, institution, message } = request.all()

            const content = {
                name, email, institution, message
            }
            
            const target_mail = Env.get('MAIL_USERNAME')            

            const mail = await Mail.send('emails.contact-us', content, (message) => {
                message
                  .to(target_mail)
                  .from(email)
                  .subject('Contact Us')
            })

            // console.log(mail)

            session.flash({ content: 'Your message has been successfully sent.', title:'Email Sent', type:'success' })
            return response.redirect('back')
            
        } catch (error) {
            throw error
        }
    }
}

module.exports = HomeController
