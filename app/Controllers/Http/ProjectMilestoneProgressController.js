'use strict'

const Project = use('App/Models/Project');
const ProjectMilestone = use('App/Models/ProjectMilestone');
const ProjectMilestoneProgress = use('App/Models/ProjectMilestoneProgress');
const ProjectMilestoneProgressFile = use('App/Models/ProjectMilestoneProgressFile');
const User = use('App/Models/User');
const Chat = use('App/Models/Chat');
const ProjectFunding = use('App/Models/ProjectFunding');
const Payment = use('App/Models/Payment');
const PaymentHistory = use('App/Models/PaymentHistory');
const Notification = use('App/Models/Notification')


const { v4: uuid } = require('uuid');;
const Database = use('Database');
const Encryption = use('Encryption');
const Helpers = use('Helpers')

const fs = Helpers.promisify(require('fs'))

const removeFile = Helpers.promisify(fs.unlink)

class ProjectMilestoneProgressController {
  
  async index ({ request, response, view }) {
  }

  
  async create ({ request, response, view, session, auth }) {
    try {
      const user = await auth.getUser()
      const { title, description, ref, milestone_id } = request.all();

      const project = request.project

      const project_id = Encryption.decrypt(decodeURIComponent(ref));      

      const milestone_progress_id = uuid();

      let movedFiles = null
      let files = []

      let file_path = '/uploads/milestone_progress_files/'+milestone_progress_id
      
      const progress_files = request.file('file', {
        // types: [''],
        size: '10mb'
      })

      if(progress_files){        
        
        await progress_files.moveAll(Helpers.publicPath(file_path), (file) => {
          
          return {
            name: `${new Date().getTime()}_${Math.floor((Math.random() * 1000) + 1)}-${file.clientName}`,
            overwrite: true
          }
        })

        movedFiles = progress_files.movedList()     
        
        // console.log(movedFiles)
        
        if (!progress_files.movedAll()) {
        
          await Promise.all(movedFiles.map((file) => {
            return removeFile(path.join(file._location, file.fileName))
          }))
        
          return progress_files.errors()
        }

      }

      const projectMilestone = await ProjectMilestone.find(milestone_id);

      if(movedFiles && movedFiles.length) {
        files = movedFiles.map((item)=>{
          return {
            id:uuid() ,project_id: projectMilestone.project_id, project_milestone_id: milestone_id,
            project_milestone_progress_id: milestone_progress_id,
            file:`${file_path}/${item.fileName}`, type: item.subtype, size: item.size, 
            created_by: user.id, created_at: new Date(), updated_at: new Date()
          }
        })
      } 

      let projectMilestoneProgress = new ProjectMilestoneProgress();      

      projectMilestoneProgress.fill({
        id:milestone_progress_id, project_id, title, description, created_by: user.id
      })

      const trx = await Database.beginTransaction();

      await projectMilestone.projectMilestoneProgressess().save(projectMilestoneProgress, trx); 
      
      await trx
      .from('project_milestone_progress_files')
      .insert(files)

      await Notification.create({
        user_id : project.investor_id,
        title:`${user.name} has updated the milestones's progress for the following research:`,
        content:project.title,
        thumbnail:'/img/notifications/progress_updated.svg',
        action_link:`/project/detail?ref=${ref}&tab=progress`,
        action_text:'view detail',
        status:'unread',
        created_by:user.id
      },trx)

      trx.commit();

      session.flash({ content: 'New progress has been successfully added', title:'Add Progress Success', type:'success' })
      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()
      const { progress_id, title, description } = request.all()
      
      let movedFiles = null
      let files = []

      let file_path = '/uploads/milestone_progress_files/'+progress_id
      
      const progress_files = request.file('file', {
        // types: [''],
        size: '10mb'
      })

      if(progress_files){        
        
        await progress_files.moveAll(Helpers.publicPath(file_path), (file) => {
          
          return {
            name: `${new Date().getTime()}_${Math.floor((Math.random() * 1000) + 1)}-${file.clientName}`,
            overwrite: true
          }
        })

        movedFiles = progress_files.movedList()     

        // console.log(movedFiles)
        
        if (!progress_files.movedAll()) {
        
          await Promise.all(movedFiles.map((file) => {
            return removeFile(path.join(file._location, file.fileName))
          }))
        
          return progress_files.errors()
        }

      } 

      let projectMilestoneProgress = await ProjectMilestoneProgress.find(progress_id)
      
      if(movedFiles.length) {
        files = movedFiles.map((item, i)=>{
          return {
            id:uuid() ,project_id: projectMilestoneProgress.project_id, project_milestone_id: projectMilestoneProgress.project_milestone_id,
            project_milestone_progress_id: progress_id,
            file:`${file_path}/${item.fileName}`, type: item.subtype, size: item.size, 
            created_by: user.id, created_at: new Date(), updated_at: new Date()
          }
        })
      } 
      
      // console.log(files)
      // console.log(files.length)

      projectMilestoneProgress.merge({
        title, description, modified_by: user.id
      })      

      const trx = await Database.beginTransaction();

      await projectMilestoneProgress.save(trx); 

      await trx
      .from('project_milestone_progress_files')
      .insert(files)

      trx.commit();

      session.flash({ content: 'The progress has been successfully updated', title:'Update Progress Success', type:'success' })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { progress } = request.all()

      let id = Encryption.decrypt(progress)

      await ProjectMilestoneProgressFile.query()
      .where('project_milestone_progress_id',id)
      .delete()

      await ProjectMilestoneProgress.query()
      .where('id',id)
      .delete()

      session.flash({ content: 'The progress has been successfully deleted', title:'Delete Progress Success', type:'success' })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
  
  
}

module.exports = ProjectMilestoneProgressController
