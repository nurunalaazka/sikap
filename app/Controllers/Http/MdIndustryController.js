'use strict'
const MdIndustry = use('App/Models/MdIndustry')


class MdIndustryController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name } = request.all();

      await MdIndustry.create({ name })

      session.flash({ 
        content: 'New industry category has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/industries')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { industry, name } = request.all()

      industry.merge({name})

      await industry.save()

      session.flash({ 
        content: 'The industry category has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/industries')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { industry } = request.all()

      await industry.delete()

      session.flash({ 
        content: 'The industry category has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdIndustryController
