'use strict'
const Project = use('App/Models/Project');
const User = use('App/Models/User');
const Chat = use('App/Models/Chat');
const Room = use('App/Models/Room');
const Notification = use('App/Models/Notification');
const ChatMember = use('App/Models/ChatMember');
const ChatContent = use('App/Models/ChatContent');
const ChatStatus = use('App/Models/ChatStatus');
const ProjectFunding = use('App/Models/ProjectFunding')
const ProjectMilestone = use('App/Models/ProjectMilestone')
const Services = new (use('App/Services/Services'))


const { v4: uuid } = require('uuid');
const Database = use('Database')
const Encryption = use('Encryption')

class ProjectFundingController {
  
  async index ({ request, response, view, auth }) {
    try{
      const user = await auth.getUser();

      if(user.type === 'researcher'){
        return this.researcherAccount(...arguments)
      }else{
        return this.investorAccount(...arguments)
      }    
      
      
    }catch(e){
      throw e
    }

  }

  async researcherAccount ({ request, response, view, auth }) {
    try {
      const user = await auth.getUser()

      const projects = await user.projects().orderBy('created_at','desc').fetch();
         
      const data = []
      
      for (const item of projects.toJSON()) { 
        
        const project = await Project.find(item.id)
        const fundingOffer = await project.projectFundings().orderBy('created_at','desc').fetch();
        let temp = fundingOffer.toJSON();

        let remaining = new Date(project.date_start) - new Date()
        remaining = Math.round(remaining/1000/60/60/24)
        
        if(remaining < 0) remaining = '0'

        project.remaining = remaining

        if(temp.length) {

          const researchField = await project.researchField().fetch();
          const studyArea = await project.studyArea().fetch();
          
          let i = temp.length

          while(i--){
            let id = temp[i].id
            const projectFunding = await ProjectFunding.find(id);
            const investor = await projectFunding.investor().fetch()

            const fundingCount = await investor.investorIn().where('status','ongoing').getCount()
            const fundedCount = await investor.investorIn().where('status','finished').getCount()

            let elapsedTime = await Services.elapsedTime(projectFunding.created_at)

            let acc_type = investor.type
            let investorData;

            if(acc_type === 'investor company'){
              investorData = await investor.userInvestorCompany().fetch()

            }else if(acc_type === 'investor individual'){
              investorData = await investor.userInvestorIndividual().fetch()
            }
            temp[i].investor = investor.toJSON()
            temp[i].investorData = investorData.toJSON()
            temp[i].elapsedTime = elapsedTime
            temp[i].fundingCount = fundingCount
            temp[i].fundedCount = fundedCount
            
          }

          project.research_field_name = researchField.name
          project.study_area_name = studyArea.name
          project.fundingOffers = temp
          // project.offersData = temp2

          data.push(project.toJSON())
        }

        
      }

      // console.log(data[0].fundingOffers)

      return view.render('funding.funding_offers',{projects:data, user:user})
      
    } catch (error) {
      throw error
    }
    
  }

  async investorAccount ({ request, response, view, auth }) {
    try {
      const user = await auth.getUser()

      const projectFundings = await user.projectFundings().orderBy('created_at','desc').fetch();      

      let temp = projectFundings.toJSON()
      let i = temp.length

      while(i--){
        let project_id = temp[i].project_id

        const project = await Project.find(project_id)
        const researchField = await project.researchField().fetch();
        const studyArea = await project.studyArea().fetch();

        project.research_field_name = researchField.name
        project.study_area_name = studyArea.name

        const researcherData = (
          await User.query()
          .where('id',project.researcher_id)
          .with('userResearcher')
          .fetch()
        ).toJSON()[0]

        project.researcher_data = researcherData

        temp[i].project = project.toJSON()
      }      
            
      return view.render('funding.funding_offers',{projects:temp, user:user})

    } catch (error) {
      throw error
    }
    
  }

  async show ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser()
      const project = request.project
      
      let fetchData = await Services.getUserInformation(user.id)   
      let userData = fetchData.user_information

      if(userData && userData.status !== 'verified') {
        session.flash({ content: 'Please fill in your information before offering fund for a research. Then wait while we verify your information.', type:'success', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile')         
      } else if(!userData) {
        session.flash({ content: 'Please fill in your information before offering fund for a research. Then wait while we verify your information.', type:'success', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile') 
      }

      const checkFunding = (
        await ProjectFunding.query()
        .where('project_id',project.id)
        .andWhere('investor_id',user.id)
        .fetch()
      ).toJSON()

      if(checkFunding.length) {
        session.flash({content:'Your offer for this project already sent', title:'Offer already sent', type:'info'})
        return response.redirect('back')
      }

      const milestones = await project.projectMilestones().orderBy('order','asc').fetch();
      const researchField = await project.researchField().fetch();
      const studyArea = await project.studyArea().fetch();
      // const researcher = await project.researcher().fetch();

      const researcher = (
        await User.query()
        .where('id',project.researcher_id)
        .with('userResearcher')
        .fetch()
      ).toJSON()[0]      

      let remaining = new Date(project.date_start) - new Date()
      remaining = Math.round(remaining/1000/60/60/24)
      
      if(remaining < 0) remaining = '0'
      
      // console.log(milestones.toJSON())

      return view.render('funding.offer_funding',{
        project:project.toJSON(),
        milestones:milestones.toJSON(),
        researchField:researchField.toJSON(),
        studyArea:studyArea.toJSON(),
        researcher,
        remaining:remaining
      })

    }catch(e){
      throw e
    }
    
  }

  async create ({ request, response, view, auth, session }) {
    try{

      // const testa = await Services.isChatExist(['b486625d-3a3b-4388-b29e-6a937372fe29','061f5f95-b9ae-44c6-bc0b-d7a7a8100417'])

      // console.log(testa)

      const user = await auth.getUser()

      const { agree, ref, message } = request.all()

      if(!agree){
        session.flash({ content: 'You have not read or agree the terms and conditions', title:'Sending Offer Failed', type:'danger' })
        return response.redirect('back')
      }
      const project = request.project

      let offer_id = uuid()
      let chat_id = uuid()
      let room_id = uuid()
      let content_id = uuid()
      let sender_id = uuid()
      let other_id = uuid()

      let oref = encodeURIComponent(Encryption.encrypt(offer_id))

      const trx = await Database.connection('chat').beginTransaction()

      const room = await Room.create({ id:room_id }, trx)

      await ChatMember.createMany( [ 
        { id:sender_id, room_id, user_id:user.id }, 
        { id:other_id, room_id, user_id:project.researcher_id } 
      ], trx )

      if(message) {
        const chatContent = new ChatContent()      

        chatContent.fill({
          id:content_id,
          sender_id: sender_id,
          content: message,
          sender_status: 'sent'
        })

        await room.chatContents().save(chatContent, trx)      

        await ChatStatus.create({
          chat_content_id: content_id,
          chat_member_id: other_id,
          read_status: 'unread'
        }, trx)
      }      

      trx.commit()

      const data = {
        id:offer_id,
        project_id:project.id, investor_id: user.id, 
        chat_id: chat_id, status: 'pending',
        created_at: new Date(), updated_at: new Date()
      }

      const tx = await Database.beginTransaction()

      await Chat.create({ id : chat_id , content_id:room_id }, tx)

      await tx.insert(data)
      .into('project_fundings')

      await Notification.create({ 
        user_id : project.researcher_id,
        title:`You have new funding offer for your research project from ${user.name}`,
        content:project.title,
        thumbnail:'/img/notifications/offer_incoming.svg',
        action_link:'/funding/funding-offer?oref='+oref,
        action_text:'accept funding here',
        status:'unread',
        created_by:user.id
      }, tx)

      await tx.commit()
      
      // await Database
      // .from('project_fundings')
      // .insert(data)
      

      return response.redirect('funding-pending?ref='+ref)

    }catch(e){
      
      throw e
    }    

  }

  async showPendingOffer ({ request, response, view, auth, session }) {
    try{
      const project = request.project

      const milestones = await project.projectMilestones().orderBy('order','asc').fetch();
      const researchField = await project.researchField().fetch();
      const studyArea = await project.studyArea().fetch();
      const researcher = await project.researcher().fetch();

      let remaining = new Date(project.date_start) - new Date()
      remaining = Math.round(remaining/1000/60/60/24)
      
      if(remaining < 0) remaining = '0'

      return view.render('funding.funding_offer-pending',{
        project:project.toJSON(),
        milestones:milestones.toJSON(),
        researchField:researchField.toJSON(),
        studyArea:studyArea.toJSON(),
        researcher:researcher.toJSON(),
        remaining        
      })

    }catch(e){
      throw e
    }
  }

  async getOffer ({ request, response, view, auth, session }) {
    try{
      const { pid, oid } = request.all();

      let offer_id = encodeURIComponent(Encryption.encrypt(oid))

      return response.redirect('funding-offer?oref='+offer_id)

    }catch(e){
      throw e
    }
  }

  async showOfferInfo ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser();

      const { oref } = request.all();

      // const ref = encodeURIComponent(oref)

      let offer_id = Encryption.decrypt(oref)

      const projectFunding = await ProjectFunding.find(offer_id)

      const project = await projectFunding.project().fetch()

      // console.log(project.toJSON())

      if(user.type === 'researcher'){
        return this.researcherOffer(...arguments)

      } else {
        return this.investorOffer(...arguments)

      }      

    }catch(e){
      throw e
    }
  }

  async researcherOffer ({ request, view, response, auth }) {
    try {
      const { oref } = request.all()

      let offer_id = Encryption.decrypt(oref)

      const projectFunding = await ProjectFunding.find(offer_id)      

      /* 
        chat content
      */
      
      const chat = await projectFunding.chat().fetch();

      const chatMember = (
        await ChatMember.query()
        .where('room_id',chat.content_id)
        .andWhere('user_id',projectFunding.investor_id)
        .fetch()
      ).toJSON()[0]

      let chatContent = (
        await ChatContent.query()
        .where('sender_id',chatMember.id)
        .orderBy('created_at','desc')
        .limit(1)
        .fetch()
      ).toJSON()[0]
      
      let senderData = await Services.getUserInformation(projectFunding.investor_id)
      senderData = senderData.user_information

      if(chatContent) {
        chatContent.elapsedTime = await Services.elapsedTime(chatContent.created_at)        
      }

      /* 
        project
      */

      const project = (
        await Project.query()
        .where('id',projectFunding.project_id)
        .with('researchField').with('studyArea')
        .with('projectMilestones',(item)=>{
          item.orderBy('order','asc')
        })
        .fetch()

      ).toJSON()[0]


      return view.render('funding.funding_offer',{project, oref:oref, chatContent, senderData, chat})
      
    } catch (error) {
      throw error
    }
  }

  async investorOffer ({ request, view, response, auth }) {
    try {
      const user = await auth.getUser()

      const { oref } = request.all()

      let offer_id = Encryption.decrypt(oref)

      const projectFunding = await ProjectFunding.find(offer_id)

      const project = await projectFunding.project().fetch()

      const milestones = await project.projectMilestones().fetch();
      const researchField = await project.researchField().fetch();
      const studyArea = await project.studyArea().fetch();
      const researcher = await project.researcher().fetch();

      let remaining = new Date(project.date_start) - new Date()
      remaining = Math.round(remaining/1000/60/60/24)
      
      if(remaining < 0) remaining = '0'

      /* 
        chat content
      */

      const chat = await projectFunding.chat().fetch();

      const chatMember = (
        await ChatMember.query()
        .where('room_id',chat.content_id)
        .andWhere('user_id',project.researcher_id)
        .fetch()
      ).toJSON()[0]

      let chatContent = (
        await ChatContent.query()
        .where('sender_id',chatMember.id)
        .orderBy('created_at','desc')
        .limit(1)
        .fetch()
      ).toJSON()[0]
      
      let senderData = await Services.getUserInformation(projectFunding.investor_id)
      senderData = senderData.user_information

      if(chatContent) {
        chatContent.elapsedTime = await Services.elapsedTime(chatContent.created_at)        
      }else {
        chatContent = {
          content: 'System: Please check the term payments again.',
          elapsedTime: '- system -'
        }
      }      

      return view.render('funding.funding_offer-accepted',{
        project:project.toJSON(),
        milestones:milestones.toJSON(),
        researchField:researchField.toJSON(),
        studyArea:studyArea.toJSON(),
        researcher:researcher.toJSON(),
        remaining:remaining,
        chatContent,
        senderData,
        oref:oref,
        chat
      })
      
    } catch (error) {
      throw error
    }
  }

  async acceptOffer ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser()

      const { oref, percentage, milestone_id, required_fund } = request.all()

      let offer_id = Encryption.decrypt(oref)

      const projectFunding = await ProjectFunding.find(offer_id)
      
      const project = await projectFunding.project().fetch()

      await ProjectFunding
      .query()
      .where('project_id',projectFunding.project_id)
      .andWhereNot('id',offer_id)
      .update({status: 'rejected'})
      
      projectFunding.merge({
        status: 'accepted'
      })

      await projectFunding.save()

      let totalPercent = 0

      for (const item of percentage) {
        totalPercent+=parseInt(item)
      }

      if(totalPercent != 100) {        
        session.flash({ content: "The milestone's percentage has to be 100% in total", title: 'Failed to save milestone(s)', type:'danger' })
        return response.redirect('back');
      }

      let i = milestone_id.length

      while(i--) {
        let mid = milestone_id[i]
        let pct = percentage[i]
        let reqfund = required_fund[i]

        await ProjectMilestone.query()
        .where('id',mid)
        .update({percentage: pct,required_fund:reqfund})
      }

      await Notification.create({
        user_id : projectFunding.investor_id,
        title:`${user.name} has accepted your funding offer for research:`,
        content:project.title,
        thumbnail:'/img/notifications/offer_accepted.svg',
        action_link:'/funding/funding-offer?oref='+encodeURIComponent(oref),
        action_text:'confirm here',
        status:'unread',
        created_by:user.id
      })

      session.flash({ content: "You have accept the funding's offer for your research", type:'success', title:'Accept Success' })
      return response.redirect('funding-offers')

    }catch(e){
      throw e
    }
  } 

  async confirmOffer ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser()

      const { oref } = request.all()

      let ref = encodeURIComponent(oref)

      let offer_id = Encryption.decrypt(oref)

      const projectFunding = await ProjectFunding.find(offer_id)

      const project = await Project.find(projectFunding.project_id);

      const milestones = (
        await ProjectMilestone.query()
        .where('project_id',project.id)
        .andWhere('order',1)
        .fetch()
      ).toJSON()

      let mid = encodeURIComponent(Encryption.encrypt(milestones[0].id))

      projectFunding.merge({
        status: 'completed'
      })

      project.merge({
        status: 'ongoing'
      })      
      
      const trx = await Database.beginTransaction();

      await projectFunding.save(trx);

      await user.investorIn().save(project, trx);

      await Notification.create({
        user_id : user.id,
        title:`Please transfer your payment for the following research:`,
        content:project.title,
        thumbnail:'/img/notifications/payment_unpaid.svg',
        action_link:'/services/transfer?milestone='+mid,
        action_text:'transfer here',
        status:'unread',
        created_by:user.id
      },trx)

      trx.commit();

      return response.redirect('/payments/create/'+ref)
      

    }catch(e){
      throw e
    }
  }

  async withdrawOffer ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()

      const { project } = request.all()

      if(!project) {
        session.flash({ content: 'Incomplete request data', type:'danger', title:'Bad Request' })
        return response.redirect('back')
      }

      let id = Encryption.decrypt(project)

      const offer = (
        await ProjectFunding.query()
        .where('project_id',id).andWhere('investor_id',user.id)
        .fetch()
      ).toJSON()
      // console.log(offer)

      // const offer = await ProjectFunding.find(id)
      let chat_id = offer[0].chat_id
      // console.log(chat_id)

      if(user.id !== offer[0].investor_id) {
        session.flash({ content: 'You do not have permission to modify this data', type:'danger', title:'Unauthorized Access' })
        return response.redirect('back')
      }

      // await offer.delete()
      await ProjectFunding.query()
      .where('id',offer[0].id)
      .delete()

      //delete chat
      const chat = await Chat.find(chat_id)
      let roomId = chat.content_id

      await chat.delete()

      const room = await Room.find(roomId)

      const chatContents = (
        await ChatContent.query()
        .where('room_id',roomId)
        .fetch()
      ).toJSON()

      let ids = []

      ids = chatContents.map(item=>{
        return item.id
      })

      // console.log(ids)

      await ChatStatus.query()
      .whereIn('chat_content_id', ids)
      .delete()

      // await room.chatStatuses().delete();

      await room.chatContents().delete();
      await room.chatMembers().delete();

      await room.delete()

      session.flash({ content: 'You have successfully withdraw your offer.', type:'success', title:'Withdraw Success' })
      return response.redirect('/funding/funding-offers')
    } catch (error) {
      throw error
    }
  }

  async redirectToPending ({ request, view, response, auth }) {
    try {
      const { project } = request.all()

      const params = encodeURIComponent(Encryption.encrypt(project))

      return response.redirect(`/funding/funding-pending?ref=${params}`)
    } catch (error) {
      throw error
    }
  }

  async filterStatus ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()

      const { status } = request.all()

      let data = []

      if(user.type === 'researcher') {
        const offers = (
          await User.query()
          .where('id',user.id)
          .with('researcherFundings',(item)=>{
            item.where('project_fundings.status',status)
          })
          .fetch()
        ).toJSON()

        if(offers.length) data = offers[0].researcherFundings

      } else {
        const offers = (
          await ProjectFunding.query()
          .where('investor_id',user.id)
          .andWhere('status',status)
          .fetch()
        ).toJSON()  

        data = offers
        
      }          
      
      return response.json({status:200, data})
    } catch (error) {
      throw error
    }
  }



}

module.exports = ProjectFundingController
