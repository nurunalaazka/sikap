'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserResearcherResearch = use('App/Models/UserResearcherResearch')
const UserResearcherEducation = use('App/Models/UserResearcherEducation')
const UserResearcherExperience = use('App/Models/UserResearcherExperience')
const UserResearcherExpertise = use('App/Models/UserResearcherExpertise')
const Services = new (use('App/Services/Services'))

const Database = use('Database')
const Helpers = use('Helpers')
const { v4: uuid } = require('uuid');
const Encryption = use('Encryption')
const Hash = use('Hash')

class UserResearcherExperienceController {
  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()
      const { 
        id, institution, position, industry_id,
        industry_category_id, location, work_start, work_end, description
      } = request.all()



      let dateStart = await Services.toSqlDate(work_start)
      let dateEnd = await Services.toSqlDate(work_end)

      const experience = await UserResearcherExperience.find(id)

      experience.merge({
        institution, position, industry_id,
        industry_category_id, location, date_start: dateStart, date_end: dateEnd, description,
        modified_by: user.id
      })

      await experience.save()

      session.flash({ 
        content: 'Your experience information has been successfully edited.', title:'Edit Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
  
  async delete ({ request, view, response, auth, session }) {
    try {
      const { experience } = request.all()

      let id = Encryption.decrypt(experience)

      await UserResearcherExperience
      .query()
      .where('id',id)
      .delete()

      session.flash({ 
        content: 'Your experience information has been successfully deleted.', title:'Delete Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
  
}

module.exports = UserResearcherExperienceController
