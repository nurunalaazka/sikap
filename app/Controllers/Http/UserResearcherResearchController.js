'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserResearcherResearch = use('App/Models/UserResearcherResearch')
const UserResearcherEducation = use('App/Models/UserResearcherEducation')
const UserResearcherExperience = use('App/Models/UserResearcherExperience')
const UserResearcherExpertise = use('App/Models/UserResearcherExpertise')
const Services = new (use('App/Services/Services'))


const Database = use('Database')
const Helpers = use('Helpers')
const { v4: uuid } = require('uuid');
const Encryption = use('Encryption')
const Hash = use('Hash')

class UserResearcherResearchController {
  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser()
      const { 
        id, title, position, research_field_id, study_area_id, location, research_start, research_end, description
      } = request.all()

      let dateStart = await Services.toSqlDate(research_start)
      let dateEnd = await Services.toSqlDate(research_end)

      const research = await UserResearcherResearch.find(id)

      research.merge({
        title, position, research_field_id, study_area_id,
        location, date_start: dateStart, date_end: dateEnd, description,
        modified_by: user.id
      })

      await research.save()

      session.flash({ 
        content: 'Your research information has been successfully edited.', title:'Edit Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { research } = request.all()

      let id = Encryption.decrypt(research)

      await UserResearcherResearch
      .query()
      .where('id',id)
      .delete()

      session.flash({ 
        content: 'Your research information has been successfully deleted.', title:'Delete Success', type: 'success' 
      })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
}

module.exports = UserResearcherResearchController
