'use strict'
const MdResearchField = use('App/Models/MdResearchField');
const MdMajor = use('App/Models/MdMajor');
const Project = use('App/Models/Project');
const User = use('App/Models/User');
const UserResearcher = use('App/Models/UserResearcher');
const ProjectMember = use('App/Models/ProjectMember')
const ProjectMilestone = use('App/Models/ProjectMilestone')
const ProjectImage = use('App/Models/ProjectImage')
const ProjectFunding = use('App/Models/ProjectFunding')
const Notification = use('App/Models/Notification')

const Encryption = use('Encryption')
const Services = new (use('App/Services/Services'))
const Master = new (use('App/Services/Master'))

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')


class ProjectController {
  
  async index ({ request, response, view }) {
    
  }

  async detail ({ request, response, view }) {
    try{             
      const { ref } = request.all()
      let temp = request.project
      const id = Encryption.decrypt(decodeURIComponent(ref));

      let project = await Project.query()
        .where('id', id)
        .with('researchField')
        .with('studyArea')
        .with('projectImages')
        .with('investor',(item)=>{
          item.with('userInvestorCompany').with('userInvestorIndividual')
        })
        .fetch();      

      project = project.toJSON().map(item=>{        
        let remaining = new Date(item.date_start) - new Date()

        remaining = remaining/(1000*60*60*24);

        if(remaining < 0) remaining = 0

        item.remaining = Math.round(remaining);

        return item;
      })      

      let researcher_id = project[0].researcher_id      

      const researcher = (
        await User.query()
        .where('id',researcher_id)
        .with('userResearcher')
        .fetch()
      ).toJSON();

      const countProject = await Project.query()
        .where('researcher_id',researcher_id)   
        .whereHas('investor')   
        .getCount();

      const total_funding = await Project.query()
        .where('researcher_id',researcher_id)
        .whereHas('investor')
        .sum('required_fund as total')      

      // const projectMembers = await temp.projectMembers().fetch();
      const projectMembers = await ProjectMember.query()
        .where('project_id',id)
        .with('user',(user)=>{
          user.with('userResearcher')
        })  
        .fetch();      
      
      const projectMilestones = await ProjectMilestone.query()
        .where('project_id',id)
        .with('projectMilestoneProgressess',(item)=>{
          item.with('projectMilestoneProgressFiles');
        })
        .orderBy('order','asc').fetch();           
           
      // console.log(projectMilestones.toJSON()[0].projectMilestoneProgressess[0])
      // console.log(projectMembers.toJSON())
     
      return view.render('research.project.project',{
        ref:ref,
        project:project[0],
        members:projectMembers.toJSON(),
        projectMilestones:projectMilestones.toJSON(),        
        total_funding,
        countProject,
        researcher:researcher[0]
      }); 
            
    }catch(e){
      throw e
    }    
  }

  async showProgress ({ request, view, response, auth }) {
    try {
      const { ref } = request.all();

      const project_id = Encryption.decrypt(decodeURIComponent(ref))

      const project = await Project.query().where('id',project_id)
      .with('projectMilestones',(milestone)=>{
        milestone.orderBy('order','asc')
        .with('projectMilestoneProgressess',(progress)=>{
          progress.orderBy('created_at','asc')
          .with('projectMilestoneProgressFiles')
        })
      })
      .fetch();

      // console.log(project.toJSON()[0].projectMilestones[0].projectMilestoneProgressess[0].projectMilestoneProgressFiles)      

      let temp = project.toJSON();    

      
      // console.log(temp[0].projectMilestones[0].projectMilestoneProgressess[0].projectMilestoneProgressFiles)

      return view.render('research.project.project-progress',{
        project:temp
      })

    } catch (error) {
      throw error
    }
  }

  async showGeneralForm ({ params, request, response, view, auth, session }) {
    try{
      const user = await auth.getUser();
      const { _draft } = request.all()

      let userData = await user.userResearcher().fetch();     

      if(userData && userData.status !== 'verified') {
        session.flash({ content: 'Please fill in your information before attempting to create a project. Then wait while we verify your information.', type:'warning', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile')         
      } else if(!userData) {
        session.flash({ content: 'Please fill in your information before attempting to create a project. Then wait while we verify your information.', type:'warning', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile') 
      }
      

      let id, project = null

      if(_draft) {
        id = Encryption.decrypt(_draft) 

        // project = await Project.find(id)

        project = (
          await Project.query()
          .where('id',id)
          .with('projectImages')
          .fetch()
        ).toJSON()

        if(!project.length) {
          session.flash({ content: 'Project not found', title: '404 Not Found', type:'danger' })
          return response.redirect('back')
        }

        // project = project.toJSON()
        project = project[0]
        // console.log(project)
      }

      const researchFields = await Master.getResearchFields(true)

      return view.render('research.project.form.project_form-general',{researchFields, project})

    }catch(e){
      throw e
    }    
  }

  
  async create ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser();

      const { researcher_id,
        title, sub_title, research_field_id, 
        study_area_id, required_fund, duration, 
        duration_unit, date_start, save_to_draft, cover_image_old, project_image_state, project_image_id } = request.all();      

      let { _draft } = request.all()
      let new_project = true

      let id = uuid();
      
      if(_draft) {
        id = Encryption.decrypt(_draft)
        new_project = false
      } else {
        _draft = Encryption.encrypt(id)
      }

      _draft = encodeURIComponent(_draft)

      /* 
        upload project cover
      */

      let cover_path = null;

      if(cover_image_old) cover_path = cover_image_old

      const cover_image = request.file('cover_image');
      
      if(cover_image){          

        cover_path = await Services.savePic(cover_image,'/uploads/project_cover',`${id}_project-cover.jpg` )
          
      }

      /* 
        upload project images
      */

      // console.log(project_image_state)

      let imagesStaticPath = `/uploads/project_images/${id}`
      let dataImages = []

      const projectImages = request.file('project_images', {
        // types: [''],
        size: '10mb'
      })

      // console.log(projectImages)

      const saved_images = await Services.uploadMultiple(projectImages, imagesStaticPath, false, project_image_state)

      // console.log(saved_images)

      if(saved_images) {
        dataImages = saved_images.map(item=>{
          return {
            id:uuid(), project_id:id, name: item.fileName,
            location:`${imagesStaticPath}/${item.fileName}`, type: item.subtype, size: item.size, 
            created_by: user.id, created_at: new Date(), updated_at: new Date()
          }
        })
      }

      if(project_image_state) {
        let len = project_image_state.length

        while(len--){
          if(project_image_state[len]==='deleted'){
            //del file
            const projectImage = await ProjectImage.find(project_image_id[len])

            await Services.deleteFile(projectImage.location)

            //del db
            await projectImage.delete()
          }
        }
      }

      /* 
        database
      */

      let project = new Project();

      let temp = await Services.toSqlDate(date_start);

      if(researcher_id) {       
        
        project.fill({
          researcher_id,id,title,sub_title,research_field_id, 
          study_area_id, required_fund, cover_image:cover_path,
          duration, duration_unit, date_start:temp, status:'draft'
        }) 

        await project.save();

        await this.insertProjectImages(dataImages)

        session.flash({ 
          content: 'New project has been successfully created',
          title: 'Project Saved',
          type: 'success'
        })
        return response.redirect('projects')

      } else {

        if(new_project) {
          project.fill({
            id,title,sub_title,research_field_id, 
            study_area_id, required_fund, cover_image:cover_path,
            duration, duration_unit, date_start:temp, status:'draft', created_by: user.id
          })
        } else {
          project = await Project.find(id)

          project.merge({
            title,sub_title,research_field_id, 
            study_area_id, required_fund, cover_image:cover_path,
            duration, duration_unit, date_start:temp, modified_by:user.id
          })

        }         

        await user.projects().save(project);

        await this.insertProjectImages(dataImages)


        if(save_to_draft) {
          session.flash({ 
            content: "The project's form has been successfully saved to draft",
            title: 'Success, Saved to Draft',
            type: 'success'
          })
          return response.redirect('back');

        } else {
          session.flash({ 
            content: 'The project has been successfully saved',
            title: 'Project Saved',
            type: 'success'
          })
          return response.redirect('/new-project/abstract?_draft='+_draft);
        }

        
      }           


    }catch(e){
      throw e
    }
  }

  async insertProjectImages (data) {
    try {
      await Database
      .from('project_images')
      .insert(data)
      
    } catch (error) {
      throw error
    }
  }
  
  async showAbstractForm ({ params, request, response, view, auth }) {
    try{
      const project = request.project

      return view.render('research.project.form.project_form-abstract',{project});

    }catch(e){
      throw e
    }
    
  }
  
  async insertAbstract ({ params, request, response, view, session }) {
    try{
      const { id, abstract, _draft, save_to_draft } = request.all();  
      // const _draft  = request._draft        

      const project = request.project

      project.merge({
        abstract
      });

      await project.save();

      if(save_to_draft) {
        session.flash({ 
          content: "The project's abstract form has been successfully saved to draft",
          title: 'Success, Saved to Draft',
          type: 'success'
        })

        return response.redirect('back');

      } else {
        session.flash({ 
          content: "The project's abstract form has been successfully updated",
          title: 'Update Abstract Success',
          type: 'success'
        })

        return response.redirect('/new-project/milestones?_draft='+_draft);
      }


    }catch(e){
      throw e
    }
    
  }

  async showMileStonesForm ({ params, request, response, view, auth }){
    try{      

      const project = request.project

      let milestones = await project.projectMilestones().orderBy('order','asc').fetch()

      if(milestones) milestones = milestones.toJSON()

      return view.render('research.project.form.project_form-milestones',{project:project.toJSON(), milestones})

    }catch(e){
      throw e
    }
  }

  
  async showTeamMembersForm ({ params, request, response, view, auth }) {
    try{
      const user = await auth.getUser();
      // console.log(user);
      const researcher = await user.userResearcher().fetch();

      const project = request.project

      let researchers = (
        await UserResearcher.query()
        .where('status','verified')
        .with('user')
        .fetch()
      ).toJSON()

      researchers = researchers.filter((item)=>{
        return item.user.id != user.id;
      });

      let members = (
        // await project.projectMembers().fetch()
        await ProjectMember.query()
        .where('project_id',project.id)
        .with('user',(item)=>{
          item.with('userResearcher')
        }).fetch()
      ).toJSON()

      // if(members) members = members.toJSON()      

      return view.render('research.project.form.project_form-team',{user:user,researcher:researcher.toJSON(),researchers:researchers, members, project})

    }catch(e){
      throw e
    }
    
  }  
  
  async showSuccess ({ params, request, response, view, auth }) {
    try{
      const { _draft } = request.all()

      return view.render('research.project.form.project_form-success',{_draft})

    }catch(e){
      throw e
    }
    
  }
  
  async updateProject ({ params, request, response, session }) {
    try {
      const {
        id,title, sub_title, researcher_id, research_field_id, study_area_id, 
        required_fund, duration, date_start, duration_unit, cover_image_old, abstract
      } = request.all()
      
      let temp = await Services.toSqlDate(date_start)

      let cover_path = cover_image_old || null;      
      
      const cover_image = request.file('cover_image');      
      
      if(cover_image) {
          cover_path = await Services.savePic(cover_image,'/uploads/project_cover',`${id}_project-cover.jpg` )
      }

      const project = await Project.find(id);

      project.merge({
        title, sub_title, researcher_id, research_field_id, study_area_id, 
        required_fund, duration, date_start:temp, duration_unit, cover_image:cover_path, abstract
      })

      await project.save();

      session.flash({ 
        content: 'The project has been successfully updated',
        title: 'Project Update Success',
        type: 'success'
      })

      return response.redirect('projects')      
    } catch (error) {
      throw error
    }
  }

  async deleteProject ({ request, view, response, auth, session }) {
    try {
        const user = await auth.getUser()

        const project = request.project;
        const img = project.cover_image

        if(!(project.status == 'draft' || project.status == 'published')) {
          session.flash({ message: 'You cannot delete published project' })
          return response.redirect('back')
        }

        if(user.role_id !== 'admin') {
          if(user.id !== project.researcher_id) {
            session.flash({ 
              content: 'You do not have permission to delete this project',
              title: 'Unauthorized Access',
              type: 'danger'
            })

            return response.redirect('back')
          }
        }

        await project.projectMembers().delete();
        await project.projectMilestones().delete();

        const projectImages = (
          await project.projectImages().fetch()
        ).toJSON()

        if(projectImages && projectImages.length) {
          for (const item of projectImages) {
            await Services.deleteFile(item.location)
          }

          await ProjectImage.query()
          .where('project_id',project.id)
          .delete()
        }

        if(project.status == 'published') {
          await this.deleteFundingOffers(project)
        }

        await project.delete()
        await Services.deleteFile(img)
                
        session.flash({ 
          content: 'The project has been successfully deleted',
          title: 'Project Delete Success',
          type: 'success'
        })
        return response.redirect('back')
    } catch (error) {
        throw error
    }
  }  

  async deleteFundingOffers (project) {
    try {
      const projectFundings = (
        await project.projectFundings().fetch()
      ).toJSON()
      
      let chatIds = []

      if(projectFundings && projectFundings.length) {
        for (const item of projectFundings) {
          chatIds.push(item.chat_id)

          await Notification.create({
            user_id : item.investor_id,
            title:`The Project has been deleted by the researcher. You could ask for more information to the related researcher.`,
            content:project.title,
            thumbnail:'',
            action_link:'/profile/view?ref='+encodeURIComponent(Encryption.encrypt(project.researcher_id)),
            action_text:'view researcher profile',
            status:'unread',
            created_by:project.researcher_id
          })
        }

        await ProjectFunding.query()
        .where('project_id',project.id)
        .delete()
        
        await Services.deleteChats(chatIds)
      }

      return true
    } catch (error) {
      throw error
    }
  }

  async redirectPage ({ request, view, response, auth, session }) {
    try {
      const { id, form, action } = request.all()

      if(!id) {
        session.flash({ content: 'Missing required parameter id on request', title:'Bad Request', type:'danger' })
        return response.redirect('back')
      }

      const params =  encodeURIComponent(Encryption.encrypt(id))

      if(form) {
        if(form == 'general') return response.redirect('/new-project/general-form?_draft='+params)
        else if(form == 'abstract') return response.redirect('/new-project/abstract?_draft='+params)
        else if(form == 'milestone') return response.redirect('/new-project/milestones?_draft='+params)
        else if(form == 'member') return response.redirect('/new-project/team-members?_draft='+params)
        else session.flash({ content: 'Unknown request form', title:'Redirect Failed', type:'danger' })        

      } else if(action) {
        if(action == 'delete') return response.redirect('/project/delete?ref='+params)
        else session.flash({ content: 'Unknown request action', title:'Redirect Failed', type:'danger' })
      }    
      else {
        session.flash({ content: 'Missing required parameter after id on request', title:'Redirect Failed', type:'danger' })
      }
      
      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }

  
  
}

module.exports = ProjectController
