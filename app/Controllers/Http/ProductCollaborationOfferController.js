'use strict'

const ProductCollaborationOffer = use('App/Models/ProductCollaborationOffer');
const Product = use('App/Models/Product')
const User = use('App/Models/User');
const ChatContent = use('App/Models/ChatContent')
const Chat = use('App/Models/Chat')
const Room = use('App/Models/Room')
const ChatMember = use('App/Models/ChatMember')

const ChatService = new (use('App/Services/Chat'))
const Services = new (use('App/Services/Services'))

const Mail = use('Mail');
const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Env = use('Env')
const Encryption = use('Encryption')

class ProductCollaborationOfferController {
    async create ({ request, view, response, session,auth }) {
        try {

            /* 
                SELECT room_id FROM `chat_members` 
                WHERE user_id IN ('b486625d-3a3b-4388-b29e-6a937372fe29','061f5f95-b9ae-44c6-bc0b-d7a7a8100417') 
                group by room_id having count(room_id)>1
            */
            const user = await auth.getUser();

            const { id } = request.all()

            if(!id) return response.json({ status:400, error:true, message: 'bad request, missing id' })

            const product = await Product.find(id)

            if(!product) return response.json({ status:404, error:true, message: 'product not found'})

            //check user info verified
            const userData = await Services.getUserInformation(user.id)

            if(userData.user_information.status !== 'verified') return response.json({ status:403, error:true, message: 'Unverified user information', redirect:'/profile/edit-profile' })

            //check already collaborated
            const isOffered = (
                await ProductCollaborationOffer.query()
                .where('product_id',product.id).andWhere('investor_id',user.id).fetch()
            ).toJSON()

            if(isOffered.length) {
                const chat_id = isOffered[0].chat_id

                const tmp_chat = await Chat.find(chat_id)

                let data = {roomId: tmp_chat.content_id}
                
                return response.json({ status:200 ,warning:true, data })
            }

            let roomId = uuid()
            let chatId = uuid()

            // console.log(roomId, chatId)  
            
            const tx = await Database.beginTransaction()

            //Maroojic
            await Chat.create({ 
                id : chatId, content_id: roomId, created_by: user.id
            },tx)

            const offer = new ProductCollaborationOffer()

            offer.fill({
                investor_id:user.id, chat_id: chatId, created_by:user.id
            })

            await product.productCollaborationOffers().save(offer, tx)

            tx.commit()

            //CHAT DB

            const trx = await Database.connection('chat').beginTransaction()
            
            await Room.create({  id: roomId },trx)

            await ChatMember.createMany( [ 
                { room_id:roomId, user_id:user.id }, 
                { room_id:roomId, user_id:product.researcher_id } 
              ], trx )

            trx.commit()       
            
            const data = { roomId }
            
            return response.json({
                status:200, data
            })
        } catch (error) {
            throw error
        }
    }
}

module.exports = ProductCollaborationOfferController
