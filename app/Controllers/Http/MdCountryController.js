'use strict'
const MdCountry = use('App/Models/MdCountry')

class MdCountryController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name } = request.all();

      await MdCountry.create({ name })

      session.flash({ 
        content: 'New country has been successfully added',
        title: 'Add Country Success',
        type: 'success'
      })

      return response.redirect('/admin/countries')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { country, name } = request.all()

      country.merge({name})

      await country.save()

      session.flash({ 
        content: 'The Country has been successfully updated',
        title: 'Update Country Success',
        type: 'success'
      })

      return response.redirect('/admin/countries')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { country } = request.all()

      await country.delete()

      session.flash({ 
        content: 'The Country has been successfully deleted',
        title: 'Delete Country Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdCountryController
