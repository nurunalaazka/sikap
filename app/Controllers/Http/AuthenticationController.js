'use strict'

const User = use('App/Models/User');
const { v4: uuid } = require('uuid');;
const Mail = use('Mail');
const Verification = use('App/Models/Verification');
const Services = new (use('App/Services/Services'))


class AuthenticationController {
    async login({ request, auth, response, session }) {
        try{
            let {email, password, remember} = request.all()            

            if(!remember){
                remember = false
            }else{
                remember = true
            }

            await auth
                .remember(remember)
                .attempt(email,password)
            
            
            let user = await auth.getUser();

            if(user.status !== 'verified') {
                await auth.logout()
                
                session.flash({
                    content:'Unverified account, please check your email to verify your account.', 
                    type:'danger', title:'Login Failed'})
                return response.redirect('back')
            }            

            await Services.saveUserToSession(...arguments)

            if(user.role_id === 'admin') return response.redirect('/admin/research-needs');
             

            return response.redirect('/')
        }catch(e){            
            throw e
        }
    }

    async register ({ request, view, response, auth, session }) {
        try{
            const {name, email, password, username, account_type} = request.all()

            if(account_type !== 'researcher' && account_type !== 'investor company' && account_type !== 'investor individual'){
                session.flash({message:'Invalid account type!'})
                return response.redirect('back')
            }
            
            const user = await User.create({                
                name,
                username,
                email,
                password,
                type:account_type,
                status:'unverified'
            })            

            let loggedInUser = await auth.attempt(email,password)           

            let token = await auth
            .authenticator('jwt')
            .generate(loggedInUser)

            user.token = token.token
            user.link = `http://localhost:3333/signup/register-verification/token/${token.token}`

            let verification = new Verification()

            verification.fill({
                token:token.token,
                type:'verification_token'
            })

            await loggedInUser.verifications().save(verification)

            await Mail.send('emails.verified-user', user.toJSON(), (message) => {
                message
                  .to(user.email)
                  .from('<from-email>')
                  .subject('Welcome to Maroojic')
            })

            await auth.logout()

            // return this.login(...arguments);
            if(account_type=='researcher'){                
                return response.redirect('/signup/register-verification?account=researcher')
            }else{                
                return response.redirect('/signup/register-verification?account=investor')
            }

            

        }catch(e){
            throw e
        }
    }

    async verifyScreen ({request, response, view}) {
        return view.render('auth.register-verification')
    }

    async verifyAccount ({auth, request, view, params, response, session}) {
        try{
            const { token } = params;
            // console.log(token)

            const verification = await Verification.findBy('token',token.replace(/\ /g, '+'));
            // console.log(verification.toJSON())

            const user = await verification.user().fetch();            

            // console.log(user.toJSON())

            await auth.loginViaId(user.id);            
            
            user.merge({
                verified_at:new Date(),                
                status:'verified'
            })

            await user.save()

            // let tmp_user = await auth.getUser();

            // const userData = await Services.getUserInformation(tmp_user.id)                

            // let userInfo = JSON.stringify(userData.user_information)
            
            // session.put('user',JSON.stringify(tmp_user))
            // session.put('user_info',userInfo)

            await Services.saveUserToSession(...arguments)

            // console.log(user)

            if(user.type == 'researcher'){
                return response.redirect('/signup/register-information?account=researcher')
            }
            else if(user.type == 'investor individual'){
                return response.redirect('/signup/register-information?account=investor-individual')
            }
            else if(user.type == 'investor company'){
                return response.redirect('/signup/register-information?account=investor-company')
            }

            // return response.redirect('/signup/register-information')
            // return response.redirect('back')

        }catch(e){
            throw e
        }     
        
    }

    async registerSuccess({auth,response,view}){
        try{
            const user = await auth.getUser()
            console.log(user);
            return view.render('auth.register-success',{user}) 
        }catch(e){
            return view.render('home') 
        }               
        
    }

    async logout ({ auth, response, session }) {
        await auth.logout()
        session.clear()
        return response.redirect('login')
    }
}

module.exports = AuthenticationController
