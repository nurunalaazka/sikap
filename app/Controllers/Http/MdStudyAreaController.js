'use strict'
const MdStudyArea = use('App/Models/MdStudyArea')
const MdResearchField = use('App/Models/MdResearchField')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')

class MdStudyAreaController {
  async getStudyAreas ({ request, auth, response }) {

    const { id } = request.all()

    const researchField = await MdResearchField.find(id)    
    const studyAreas = await researchField.studyAreas().fetch()
    
    return response.json({status:200, data:studyAreas.toJSON()})
  }

  async create ({ request, view, response, auth, session }) {
    try {
      const { name, research_field_id } = request.all();

      await MdStudyArea.create({ name, research_field_id })

      session.flash({ 
        content: 'New study area has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/study-areas')
      
    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { studyArea, name, research_field_id } = request.all()

      studyArea.merge({name, research_field_id})

      await studyArea.save()

      session.flash({ 
        content: 'The study area has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/study-areas')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { studyArea } = request.all()

      await studyArea.delete()

      session.flash({ 
        content: 'The study area has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdStudyAreaController
