'use strict'
const Encryption = use('Encryption')
const Hash = use('Hash')
const ForgotPassword = use('App/Models/ForgotPassword')
const User = use('App/Models/User')
const Mail = use('Mail');

class ForgotPasswordController {
  async show ({ params, request, response, view }) {
    return view.render('auth.forgot-password')
  }
  
  async sendMail ({ params, request, response, view, session }) {
    
    const { email } = request.all()
    const token = encodeURIComponent(await Hash.make(Encryption.encrypt(`${email} ${new Date()}`)))

    const user = await User.findBy('email',email);    

    const forgotPassword = new ForgotPassword();

    var d = new Date();
    var h = d.setHours(d.getHours()+3);
    h = new Date(h)

    forgotPassword.fill({
      token:token,
      expired_at:h
    })

    let link = `http://localhost:3333/auth/forgot-password/token/${token}`

    user.link = link

    await user.forgotPasswords().save(forgotPassword)

    await Mail.send('emails.forgot-password-mail', user.toJSON(), (message) => {
      message
        .to(user.email)
        .from('<from-email>')
        .subject('Welcome to Maroojic')
    })

    // return response.json({enc:token})   //ke mail 
    session.flash({type:'success',title:'Link Sent to Your Email',content:'A link to reset password has been sent to your email. Klik link in your email to reset your password, don\'t forget to check your spam folder too..'})
    return response.redirect('back')
  }

  async confirmRemakePassword ({ params, request, response, view, session, auth }) {
    const { token } = params

    const forgotPassword = await ForgotPassword.findBy('token',token)

    if(new Date() >= forgotPassword.expired_at){
      session.flash({message:'token expired!'})
      return response.redirect('/auth/forgot-password')
    }

    const user = await forgotPassword.user().fetch();

    await auth.loginViaId(user.id);      

    return response.redirect('/auth/forgot-password/new-password');
  }

  async remakePassword ({ params, request, response, view, session, auth }) {
    const user = await auth.getUser()
    const { password } = request.all()

    const hashedPassword = await Hash.make(password)

    user.merge({
      password:hashedPassword
    })

    await user.save()

    session.flash({message:"success"})
    session.flash({type:'success',title:'Reset Password Successful',content:'Reset your password has been successful. Now you are logged in with the new password'})
    return response.redirect('back')

  }

  
  async update ({ params, request, response }) {
  } 
  
}

module.exports = ForgotPasswordController
