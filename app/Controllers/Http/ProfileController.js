'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserResearcherResearch = use('App/Models/UserResearcherResearch')
const UserResearcherEducation = use('App/Models/UserResearcherEducation')
const UserResearcherExperience = use('App/Models/UserResearcherExperience')
const UserResearcherExpertise = use('App/Models/UserResearcherExpertise')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdResearchField = use('App/Models/MdResearchField')
const MdStudyArea = use('App/Models/MdStudyArea')
const ResearchNeed = use('App/Models/ResearchNeed')
const MdBank = use('App/Models/MdBank')
const User = use('App/Models/User')
const Project = use('App/Models/Project')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')
const Notification = use('App/Models/Notification')
const MdIndustry = use('App/Models/MdIndustry')
const MdIndustryCategory = use('App/Models/MdIndustryCategory')
const MdDegree = use('App/Models/MdDegree')
const Master = new (use('App/Services/Master'))
const Services = new (use('App/Services/Services'))



const Database = use('Database')
const Helpers = use('Helpers')
const { v4: uuid } = require('uuid');
const Encryption = use('Encryption')
const Hash = use('Hash')


class ProfileController {
    async index ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();

            const type = user.type

            let data = null
            let stat = null
            let projects = null
            let funded = null
            let researchNeeds = null
            let products = null
            let researchField = null
            let studyArea = null
            let experiences = null
            let researchs = null
            let expertises = null
            let educations = null
            let country,province,city,district = null;
            let experienceCount, latestDegree, latestResearch = null;


            if(type === 'researcher') {
                data = await this.researcherProfile(...arguments);                

                stat = await this.researcherStat(...arguments);
                researchField = data.researchField;
                studyArea = data.studyArea;
                projects = stat.countProjects;
                products = stat.countProducts;
                funded = stat.countFundedProjects;
                experiences = stat.experiences;
                expertises = stat.expertises;
                educations = stat.educations;
                researchs = stat.researchs
                experienceCount = stat.experienceCount
                latestDegree = stat.latestDegree
                latestResearch = stat.latestResearch

            }
            else if(type === 'investor company') {
                data = await this.investorCompanyProfile(...arguments);
                stat = await this.investorStat(...arguments)

                projects = stat.countProjects;
                funded = stat.countFundedProjects;
                researchNeeds = stat.countResearchNeeds;
                
            }
            else if(type === 'investor individual') {
                data = await this.investorIndividualProfile(...arguments);                
                stat = await this.investorStat(...arguments)

                projects = stat.countProjects;
                funded = stat.countFundedProjects;
                researchNeeds = stat.countResearchNeeds;

            }
            else {
                return response.json({msg: 'invalid user type'})
            }

            let profile = data.profile

            if(profile) profile = profile.toJSON()
            
            let industries = await Master.getIndustries(true)
            let industryCategories = await Master.getIndustryCategories(true)
            let studyAreas = await Master.getStudyAreas(true)
            let researchFields = await Master.getResearchFields(true)
            let degrees = await Master.getDegrees(true)  
            
            country = data.country
            province = data.province
            city = data.city
            district = data.district

            return view.render('profile.profile',{
                user, profile, researchField, studyArea,
                projects, products, funded, industryCategories, industries, studyAreas, researchFields,
                degrees, researchs, expertises, experiences, educations, country, province, city, district,
                researchNeeds, experienceCount, latestDegree, latestResearch
            })
            
        } catch (error) {
            throw error
        }
    }

    async showProfileForm ({ request, view, response, auth }) {
        try {
            const user = await auth.getUser();

            const type = user.type

            let data = null            

            if(type === 'researcher') {
                data = await this.researcherProfile(...arguments);
            }
            else if(type === 'investor company') {
                data = await this.investorCompanyProfile(...arguments);
            }
            else if(type === 'investor individual') {
                data = await this.investorIndividualProfile(...arguments);
            }
            else {
                return response.json({msg: 'invalid user type'})
            }   
            
            const bankAccount = data.bankAccount
            let profile = data.profile
            const provinces = await Master.getProvinces(true)
            let researchFields = await Master.getResearchFields(true)
            let studyAreas = await Master.getStudyAreas(true)
            const banks = await Master.getBanks(true)
            const countries = await Master.getCountries(true)
            const cities = await Master.getCities(true)
            const districts = await Master.getDistricts(true)            
            
            if(profile) profile = profile.toJSON()            
            
            return view.render('profile.edit-profile', {
                user, profile, bankAccount,
                banks, provinces, researchFields, countries, 
                cities, districts, studyAreas
            })

        } catch (error) {
            throw error
        }
    }

    async investorCompanyProfile({ request, view, response, auth }, visitor) {
        try {
            let { user } = request.all();

            if(!visitor) user = await auth.getUser();

            const profile = await user.userInvestorCompany().fetch();

            if(!profile) return {profile, bankAccount:null}

            let country, province, city, district = null;

            if(profile.country_id)  country = await MdCountry.find(profile.country_id)
            if(profile.province_id) province = await MdProvince.find(profile.province_id)
            if(profile.city_id) city = await MdCity.find(profile.city_id)
            if(profile.district_id) district = await MdDistrict.find(profile.district_id)
            
            if(country) country = country.toJSON()
            if(province) province = province.toJSON()
            if(city) city = city.toJSON()
            if(district) district = district.toJSON()

            let bankAccount = await user.userBankAccount().fetch()
            if(bankAccount) bankAccount = bankAccount.toJSON()


            return {profile, bankAccount, country, province, city, district}
            
        } catch (error) {
            throw error
        }
    }

    async investorIndividualProfile({ request, view, response, auth }, visitor) {
        try {
            let { user } = request.all();

            if(!visitor) user = await auth.getUser();

            const profile = await user.userInvestorIndividual().fetch();

            if(!profile) return {profile, bankAccount:null}

            let country, province, city, district = null;

            if(profile.country_id)  country = await MdCountry.find(profile.country_id)
            if(profile.province_id) province = await MdProvince.find(profile.province_id)
            if(profile.city_id) city = await MdCity.find(profile.city_id)
            if(profile.district_id) district = await MdDistrict.find(profile.district_id)
            
            if(country) country = country.toJSON()
            if(province) province = province.toJSON()
            if(city) city = city.toJSON()
            if(district) district = district.toJSON()

            let bankAccount = await user.userBankAccount().fetch()
            if(bankAccount) bankAccount = bankAccount.toJSON()

            // console.log(country)

            return {profile, bankAccount, country, province, city, district}
            
        } catch (error) {
            throw error
        }
    }

    async researcherProfile({ request, view, response, auth }, visitor) {
        try {
            let { user } = request.all();

            if(!visitor) user = await auth.getUser();

            const profile = await user.userResearcher().fetch();
            
            if(!profile) return {profile, studyArea: null, researchField:null}

            let researchField = await MdResearchField.find(profile.research_field_id)
            let studyArea = await MdStudyArea.find(profile.study_area_id)
            let bankAccount = await user.userBankAccount().fetch()

            if(researchField){
                researchField = researchField.toJSON();
            }
            if(studyArea){
                studyArea = studyArea.toJSON();
            }
            if(bankAccount) {
                bankAccount = bankAccount.toJSON()
            }
            
            // console.log(studyArea)

            return {profile, studyArea, researchField, bankAccount}
            
        } catch (error) {
            throw error
        }
    }    

    async researcherStat ({ auth, request }, visitor) {
        try {
            let { user } = request.all();

            if(!visitor) user = await auth.getUser();
            let experienceCount, latestDegree, latestResearch = null;

            const countProjects = await user.projects().getCount();
            const countFundedProjects = await user.projects().whereHas('investor').getCount()
            const countProducts = await user.products().getCount();
            const expertises = (
                await user.userResearcherExpertise().fetch()
            ).toJSON();
            const experiences = (
                await UserResearcherExperience.query()
                .where('user_id', user.id)
                .with('industry')
                .with('industryCategory')
                .orderBy('date_start','desc')
                .fetch()
            ).toJSON();
            const educations = (
                await UserResearcherEducation.query()
                .where('user_id', user.id)
                .with('degree')
                .orderBy('date_start','desc').fetch()
            ).toJSON();
            const researchs = (
                await UserResearcherResearch.query()
                .where('user_id', user.id)
                .with('researchField')
                .with('studyArea')
                .orderBy('date_start','desc').fetch()
            ).toJSON();

            let exp_len = experiences.length
            
            if( experiences.length && experiences[0].date_end) {
                experienceCount = experiences[0].date_end - experiences[exp_len-1].date_start
                experienceCount = await Services.getInterval(experienceCount)
                
            } else if(experiences.length && !experiences[0].date_end) {
                experienceCount = new Date() - experiences[exp_len-1].date_start
                experienceCount = await Services.getInterval(experienceCount)
            }
            
            if(educations.length && educations[0].degree) latestDegree = educations[0].degree.name            
            if(researchs.length && researchs[0].title) latestResearch = researchs[0].title

            return {
                countProjects, countProducts, countFundedProjects, 
                expertises, experiences, educations, researchs, 
                experienceCount, latestResearch, latestDegree
            }
        } catch (error) {
            throw error
        }
    }

    async investorStat ({ request, view, response, auth }, visitor) {
        try {
            let { user } = request.all();

            if(!visitor) user = await auth.getUser();

            //project            
            const countProjects = await Project.query()
            .where('investor_id',user.id).andWhere('status','ongoing').getCount()

            //funded
            const countFundedProjects = await Project.query()
            .where('investor_id',user.id).andWhere('status','completed').getCount()

            const countResearchNeeds = await ResearchNeed.query()
            .where('investor_id',user.id).andWhere('status','active')
            .getCount()

            // console.log(countFundedProjects, countProjects)
            return {countProjects, countFundedProjects, countResearchNeeds}

        } catch (error) {
            throw error
        }
    }

    async editProfile ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();
            const type = user.type

            const { chairman, pic, phone, province, city, district, address, profile_pic_old,
                gender, institution, position, research_field_id, study_area_id,
                scopus_index, google_scholar_link, id_card_old
            } = request.all();            

            let pp_path = profile_pic_old
            let id_path = id_card_old

            const pp_name = `${user.id}_prof-pic.jpg`
            const id_name = `${user.id}_id-card.jpg`

            const profile_pic_file = request.file('profile_pic')
            const id_card_file = request.file('id_card')            

            if(profile_pic_file){
                pp_path = '/uploads/profile_picture'

                await profile_pic_file.move(Helpers.publicPath(pp_path), {
                    name: pp_name,
                    overwrite: true
                })

                pp_path = `${pp_path}/${pp_name}`
                
            } 

            // console.log(Helpers.publicPath(pp_path),"oo")
            
            if(id_card_file){
                id_path = '/uploads/id_card'

                await id_card_file.move(Helpers.publicPath(id_path), {
                    name: id_name,
                    overwrite: true
                })

                id_path = `${id_path}/${id_name}`

            }


            if(type === 'researcher') {
                let temp = await user.userResearcher().fetch();

                if(temp) {
                    temp.merge({                    
                        profile_picture: pp_path, gender, institution, position, research_field_id, study_area_id,
                        scopus_index, google_scholar_link, id_card: id_path, modified_by: user.id
                    })
                }
                else {
                    temp = new UserResearcher()

                    temp.fill({
                        profile_picture: pp_path, gender, institution, position, research_field_id, study_area_id,
                        scopus_index, google_scholar_link, id_card: id_path, created_by: user.id, status:'unverified'
                    })

                }                

                await user.userResearcher().save(temp);

            }
            else if(type === 'investor company') {
                let temp = await user.userInvestorCompany().fetch();

                if(temp) {
                    temp.merge({
                        chairman, pic, phone, province_id:province, city_id:city, district_id:district, address,
                        profile_picture: pp_path, modified_by: user.id
                    })
                }
                else {
                    temp = new UserInvestorCompany()

                    temp.fill({
                        chairman, pic, phone, province_id:province, city_id:city, district_id:district, address,
                        profile_picture: pp_path, created_by: user.id, status:'unverified'
                    })
                }                

                await user.userInvestorCompany().save(temp);

            }
            else if(type === 'investor individual') {
                let temp = await user.userInvestorIndividual().fetch();

                if(temp) {
                    temp.merge({                    
                        profile_picture: pp_path, gender, phone, province_id:province, city_id:city, district_id:district, address,
                        modified_by: user.id
                    })
                }
                else {
                    temp = new UserInvestorIndividual()

                    temp.fill({
                        profile_picture: pp_path, gender, phone, province_id:province, city_id:city, district_id:district, address,
                        created_by: user.id, status:'unverified'
                    })
                }

                await user.userInvestorIndividual().save(temp);

            }

            session.forget('user')
            session.forget('user_info')

            await Services.saveUserToSession(...arguments)

            session.flash({ content: 'Your profile has been successfully updated', title: 'Update Profile Success', type: 'success' })
            return response.redirect('back')
        } catch (error) {
            throw error
        }
    }
    
    async showPasswordForm ({ request, view, response, auth }) {
        try {
            return view.render('profile.password_change')
        } catch (error) {
            throw error
        }
    }

    async updatePassword ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();
            const { old_password, new_password, repeat_password } = request.all();

            if(new_password !== repeat_password) {
                session.flash({ message: 'Mismatch password' })
                return response.redirect('back')
            }
            
            const hashed_password = await Hash.make(new_password)
            
            const isSame = await Hash.verify(old_password, user.password)            

            if(!isSame) {
                session.flash({ message: 'Old password is incorrect' })
                return response.redirect('back')
            }

            user.merge({
                password: hashed_password
            })

            await user.save(); 
            
            session.flash({ 
                content: 'Your password has been successfully updated',
                title:'Update Success',
                type: 'success'
            })
            
            return response.redirect('/profile')
            
        } catch (error) {
            throw error
        }
    }

    async showBankAccount ({ request, view, response, auth }) {
        try {
            const bank_account = await this.getUserBankAccount(...arguments);

            console.log(bank_account)

            return view.render('profile.bank_account',{bank_account})
        } catch (error) {
            throw error
        }
    }

    async showBankAccountForm ({ request, view, response, auth }) {
        try {
            const bank_account = await this.getUserBankAccount(...arguments);

            const banks = await Master.getBanks(true);

            return view.render('profile.edit-bank_account',{bank_account, banks})
        } catch (error) {
            throw error
        }
    }

    async getUserBankAccount ({ request, view, response, auth }) {
        try {
            const user = await auth.getUser();

            const bank_account = (
                await UserBankAccount.query()
                .where('user_id',user.id)
                .with('bank')
                .fetch()
            ).toJSON()[0];

            return bank_account

        } catch (error) {
            throw error
        }
    }

    async updateBankAccount ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();

            const { number, holder, bank_address, bank_id } = request.all();

            let bank_account = await user.userBankAccount().fetch();

            if(bank_account) {
                bank_account.merge({
                    holder, number, bank_address, bank_id, modified_by: user.id, status: 'unverified'
                })
            } else {
                bank_account = new UserBankAccount()

                bank_account.fill({
                    holder, number, bank_address, bank_id, created_by: user.id, status: 'unverified'
                })
            }            

            await user.userBankAccount().save(bank_account);

            session.flash({ 
                content: 'Your bank account has been successfully updated',
                title:'Update Success',
                type: 'success'
            })

            return response.redirect('bank-account')
        } catch (error) {
            throw error
        }
    }

    async updateExperience ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();

            const { 
                institution, position, industry_id, industry_category_id, 
                location, work_start, work_end, description 
            } = request.all();

            let date_start = await Services.toSqlDate(work_start)
            let date_end = await Services.toSqlDate(work_end)   

            const userResearcherExperience = new UserResearcherExperience;

            userResearcherExperience.fill({
                institution, position, industry_id, industry_category_id, 
                location, date_start, date_end, description
            })

            await user.userResearcherExperience().save(userResearcherExperience);

            session.flash({ 
                content: 'Your experience information has been successfully updated',
                title:'Update Success',
                type: 'success'
            })

            return response.redirect('back')
        } catch (error) {
            throw error
        }
    }

    async updateEducation ({ request, view, response, auth, session}) {
        try {
            const user = await auth.getUser();
            
            const { 
                university, faculty, major, degree_id, gpa, max_gpa, location, study_start, study_end, description
            } = request.all();            

            let date_start = await Services.toSqlDate(study_start)
            let date_end = await Services.toSqlDate(study_end)

            const userResearcherEducation = new UserResearcherEducation;

            userResearcherEducation.fill({
                university, faculty, major, degree_id, gpa, max_gpa,
                location, date_start, date_end, description
            })

            await user.userResearcherEducation().save(userResearcherEducation);

            session.flash({ 
                content: 'Your education history has been successfully updated',
                title:'Update Success',
                type: 'success'
            })

            return response.redirect('back')            
        } catch (error) {
            throw error
        }
    }

    async updateResearch ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();

            const { 
                title, position, research_field_id, study_area_id, location, description, research_start, research_end
            } = request.all();            

            let date_start = await Services.toSqlDate(research_start);
            let date_end = await Services.toSqlDate(research_end);

            const userResearcherResearch = new UserResearcherResearch;

            userResearcherResearch.fill({
                title, position, research_field_id, study_area_id, 
                location, description, date_start, date_end
            })

            await user.userResearcherResearch().save(userResearcherResearch);

            session.flash({ 
                content: 'Your research history has been successfully updated',
                title:'Update Success',
                type: 'success'
            })

            return response.redirect('back')            
        } catch (error) {
            throw error
        }
    }

    async updateExpertise ({ request, view, response, auth, session }) {
        try {
            const user = await auth.getUser();

            const { id, expertise, value } = request.all();

            console.log(request.all())

            let userResearcherExpertise = new UserResearcherExpertise;

            if(id) {
                userResearcherExpertise = await UserResearcherExpertise.find(id)

                userResearcherExpertise.merge({
                    expertise, value
                })

            }  else {

                userResearcherExpertise.fill({
                    expertise, value
                })
            }           

            await user.userResearcherExpertise().save(userResearcherExpertise);

            // return response.redirect('back')
            
        } catch (error) {
            throw error
        }
    }

    async view ({ request, view, response, auth }) {
        try {
            const {user} = request.all()

            const type = user.type

            let data = null
            let stat = null
            let projects = null
            let funded = null
            let researchNeeds = null
            let products = null
            let researchField = null
            let studyArea = null
            let experiences = null
            let researchs = null
            let expertises = null
            let educations = null
            let country,province,city,district = null;
            let experienceCount, latestDegree, latestResearch = null;

            if(type === 'researcher') {
                data = await this.researcherProfile(...arguments, true);                

                stat = await this.researcherStat(...arguments, true);
                researchField = data.researchField;
                studyArea = data.studyArea;
                projects = stat.countProjects;
                products = stat.countProducts;
                funded = stat.countFundedProjects;
                experiences = stat.experiences;
                expertises = stat.expertises;
                educations = stat.educations;
                researchs = stat.researchs
                experienceCount = stat.experienceCount
                latestDegree = stat.latestDegree
                latestResearch = stat.latestResearch

            }
            else if(type === 'investor company') {
                data = await this.investorCompanyProfile(...arguments, true);
                stat = await this.investorStat(...arguments, true)

                projects = stat.countProjects;
                funded = stat.countFundedProjects;
                researchNeeds = stat.countResearchNeeds;
                
            }
            else if(type === 'investor individual') {
                data = await this.investorIndividualProfile(...arguments, true);                
                stat = await this.investorStat(...arguments, true)

                projects = stat.countProjects;
                funded = stat.countFundedProjects;
                researchNeeds = stat.countResearchNeeds;

            }
            else {
                return response.json({msg: 'invalid user type'})
            }

            let profile = data.profile

            if(profile) profile = profile.toJSON()           
            
            
            country = data.country
            province = data.province
            city = data.city
            district = data.district

            return view.render('profile.view-profile',{
                user, profile, researchField, studyArea,
                projects, products, funded, 
                researchs, expertises, experiences, educations, country, province, city, district,
                researchNeeds, experienceCount, latestDegree, latestResearch
            })

        } catch (error) {
            throw error
        }
    }

    async formatDate (date) {
        try {
            if(!date) return null

            let temp = date.split('/');

            let formattedDate = new Date(`${temp[2]}-${temp[1]}-${temp[0]}`);

            return formattedDate
        } catch (error) {
            throw error
        }
    }

}

module.exports = ProfileController
