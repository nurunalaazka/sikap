'use strict'

const MdBank = use('App/Models/MdBank')

class MdBankController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name, short_name } = request.all();

      await MdBank.create({ name, short_name })

      session.flash({ 
        content: 'New bank has been successfully added',
        title: 'Add Bank Success',
        type: 'success'
      })
      return response.redirect('/admin/banks')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { bank, name, short_name } = request.all()

      bank.merge({name, short_name})

      await bank.save()

      session.flash({ 
        content: 'The Bank has been successfully updated',
        title: 'Bank Update Success',
        type: 'success'
      })

      return response.redirect('/admin/banks')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { bank } = request.all()

      await bank.delete()

      session.flash({ 
        content: 'The Bank has been successfully deleted',
        title: 'Bank Delete Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdBankController
