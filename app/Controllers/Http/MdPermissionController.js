'use strict'

const MdPermission = use('App/Models/MdPermission')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')

class MdPermissionController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name } = request.all();

      await MdPermission.create({ name })

      session.flash({ 
        content: 'New permission has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/permissions')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { permission, name } = request.all()

      permission.merge({name})

      await permission.save()

      session.flash({ 
        content: 'The permission has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/permissions')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { permission } = request.all()

      await permission.delete()

      session.flash({ 
        content: 'permission has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdPermissionController
