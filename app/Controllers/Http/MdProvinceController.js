'use strict'
const MdProvince = use('App/Models/MdProvince')

class MdProvinceController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name, country_id } = request.all();

      await MdProvince.create({ name, country_id })

      session.flash({ 
        content: 'New Province has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/provinces')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { province, name, country_id } = request.all()

      province.merge({name, country_id})

      await province.save()

      session.flash({ 
        content: 'The province has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/provinces')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { province } = request.all()

      await province.delete()

      session.flash({ 
        content: 'The Delete has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }

  async getProvinces ({ request, view, response, auth }) {
    try {
      const { id } = request.all();

      const provinces = (
        await MdProvince.query().where('country_id',id).fetch()
      ).toJSON()

      return response.json({status:200, data:provinces})

    } catch (error) {
      throw error
    }
  }
}

module.exports = MdProvinceController
