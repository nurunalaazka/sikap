'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const MdStudyArea = use('App/Models/MdStudyArea');
const ResearchNeed = use('App/Models/ResearchNeed')
const Project = use('App/Models/Project');
const Product = use('App/Models/Product')
const User = use('App/Models/User');
const Notification = use('App/Models/Notification')
const Encryption = use('Encryption')
const ChatContent = use('App/Models/ChatContent')
const ChatService = new (use('App/Services/Chat'))
const Services = new (use('App/Services/Services'))
const Mail = use('Mail');

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Env = use('Env')

class NotificationController {
  async setToRead ({ request, view, response, auth }) {
    try {
      const { user_id } = request.all()

      if(!user_id) return response.json({ error:'missing user_id', status:400 })

      await Notification.query()
      .where('user_id',user_id)
      .update({'status':'read'})

      return response.json({ status:200, data:'success'})
    } catch (error) {
      throw error
    }
  }
}

module.exports = NotificationController
