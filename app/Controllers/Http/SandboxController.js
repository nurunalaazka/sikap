'use strict'
const types = []
const provinces = [
   {"value":"","label":"a"},
   {"value":"","label":"b"}
]
const cities = [
   {"value":"","label":"c"},
   {"value":"","label":"d"}
]
const districts = [
   {"value":"","label":"e"},
   {"value":"","label":"g"}
]
const subdistricts = [
   {"value":"","label":"h"},
   {"value":"","label":"i"}
]

const standardizations = [
   {"value":"a","label":"h"},
   {"value":"x","label":"i"}
]

const dummyData = [
    {
        "colnum":2,
        "columns":[
           {
            "label":"",
            "type":"file",
            "placeholder":"",
            "name":"",
            "value":"",
            "mandatory":true,
            "src":"",
            "additional":""
           },
           {
            "label":"",
            "type":"image",
            "placeholder":"",
            "name":"",
            "value":"",
            "mandatory":true,
            "src":"",
            "additional":"",
            "colwidth":3
           },
           {
            "label":"",
            "type":"",
            "placeholder":"",
            "name":"",
            "value":"",
            "mandatory":true,
            "additional":"",
            "min":"",
            "max":"",
            "rows":"",
            "maxLength":""
           }
        ]
     },
     {
        "colnum":1,
        "columns":[
           {
              "label":"",
              "type":types[Math.floor(Math.random() * 13)-1],
              "placeholder":"",
              "name":"",
              "value":"",
              "mandatory":false,
              "multiple":true,
              "options":[
                 {"label":"","value":""}
              ],
              "additional":""
           }
        ]
     }
];
const data = [
    {
       "colnum":1,
       "columns":[
          {
             "label":"Nama Kegiatan",
             "type":"text",
             "placeholder":"Masukkan Nama Kegiatan di sini",
             "value":"ad",
             "name":"ad",
             "id":"ad",
             "mandatory":true
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Tag",
             "type":"text",
             "placeholder":"Pisahkan dengan koma ( contoh: Pendidikan, Kesehatan, Ekonomi )",
             "value":"",
             "mandatory":true
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"User yang Dapat Daftar",
             "type":"checkbox",
             "name":"role",
             "value":"",
             "mandatory":true,
             "colnum":1,
             "options":[
                {
                   "label":"Penyedia",
                   "name":"role"
                },
                {
                   "label":"Pokja",
                   "name":"role"
                },
                {
                   "label":"Staf LPSE",
                   "name":"role"
                },
                {
                   "label":"PP",
                   "name":"role"
                },
                {
                   "label":"Lain-lain",
                   "name":"role"
                },
                {
                   "label":"UKPBJ",
                   "name":"role"
                }
             ]
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Mulai Kegiatan",
             "type":"datepicker",
             "placeholder":"Tanggal Mulai Kegiatan",
             "value":"",
             "mandatory":true,
             "colwidth":3
          },
          {
             "label":"Selesai Kegiatan",
             "type":"datepicker",
             "placeholder":"Tanggal Selesai Kegiatan",
             "value":"",
             "mandatory":true
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Mulai Pendaftaran",
             "type":"datepicker",
             "placeholder":"Tanggal Mulai Pendaftaran",
             "value":"",
             "mandatory":true
          },
          {
             "label":"Selesai Pendaftaran",
             "type":"datepicker",
             "placeholder":"Tanggal Selesai Pendaftaran",
             "value":"",
             "mandatory":true
          }
       ]
    },
    {
       "colnum":"",
       "columns":[
          {
             "label":"Kuota Peserta",
             "type":"radio",
             "name":"quota",
             "value":"1",
             "mandatory":true,
             "options":[
                {
                   "label":"Ya",
                   "value":"1",
                },
                {
                   "label":"Tidak",
                   "value":"0"
                }
             ],
             "colwidth":2
          },
          {
             "label":"Masukkan Kuota",
             "type":"number",
             "placeholder":"Masukkan Kuota Peserta di sini",
             "value":"",
             "mandatory":true,
             "colwidth":3
          },
          {
             "label":"Masukkan Biaya",
             "type":"money",
             "placeholder":"Masukkan Kuota Peserta di sini",
             "value":"",
             "mandatory":true,
             "colwidth":3
          },
          {
             "label":"",
             "type":"percentage",
             "placeholder":"Masukkan Kuota Peserta di sini",
             "value":"",
             "mandatory":true,
             "colwidth":3
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Nama Tempat Kegiatan",
             "type":"text",
             "placeholder":"Masukkan Tempat Kegiatan di sini",
             "value":"",
             "mandatory":true
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Provinsi",
             "type":"select",
             "placeholder":"Pilih Provinsi",
             "value":"",
             "mandatory":true,
             "options":provinces
          },
          {
             "label":"Kabupaten/Kota",
             "type":"select",
             "placeholder":"Pilih Kabupaten/Kota",
             "value":"",
             "mandatory":true,
             "options":cities
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Kecamatan",
             "type":"select",
             "placeholder":"Pilih Kecamatan",
             "value":"",
             "mandatory":true,
             "options":districts
          },
          {
             "label":"Kelurahan",
             "type":"select",
             "placeholder":"Pilih Kelurahan",
             "value":"",
             "mandatory":true,
             "options":subdistricts
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Kode Pos",
             "type":"password",
             "placeholder":"Masukkan Kode Pos di sini",
             "value":"",
             "mandatory":true,
             "colwidth":3
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Alamat Lengkap",
             "type":"hidden",
             "placeholder":"Masukkan Alamat Lengkap Perusahaan di sini",
             "value":"",
             "mandatory":true,
             "colnum":1
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Pilih dari Google Maps",
             "type":"email",
             "placeholder":"email",
             "value":"",
             "mandatory":true,
             "colnum":1
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Deskripsi Kegiatan",
             "type":"wysiwyg",
             "placeholder":"Masukkan Deskripsi Kegiatan di sini",
             "value":"",
             "mandatory":true,
             "colnum":1
          }
       ]
    },
    {
       "colnum":1,
       "columns":[
          {
             "label":"Standardisasi",
             "type":"select",
             "placeholder":"Pilih Standardisasi",
             "value":"",
             "mandatory":false,
             "colnum":1,
             "multiple":true,
             "options":standardizations
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Penyelenggara",
             "type":"radio",
             "name":"organizer",
             "value":"Instansi",
             "mandatory":true,
             "colnum":2,
             "options":[
                {
                   "label":"Instansi",
                   "name":"organizer"
                },
                {
                   "label":"LPSE",
                   "name":"organizer"
                }
             ]
          },
          {
             "label":"Instansi",
             "type":"select",
             "placeholder":"Pilih Instansi",
             "value":"",
             "mandatory":true,
             "colnum":2,
             "options":[
                "Kementerian Keuangan",
                "Kementerian Pemuda dan Olahraga",
                "Kementerian Perindustrian"
             ]
          }
       ]
    },
    {
       "colnum":2,
       "columns":[
          {
             "label":"Upload file PDF",
             "type":"file",
             "placeholder":"Unggah Cover Photo",
             "value":"",
             "mandatory":true,
             "src":"click upload to select your file..",
             "additional":"ketentuan: *.pdf (Max 2MB)"
          },
          {
            "label":"Upload Cover Photo",
            "type":"image",
            "placeholder":"Unggah Cover Photo",
            "value":"",
            "mandatory":true,
            "src":"../../img//agenda-detail.jpg",
            "additional":"ketentuan: *.jpg, *.png, *.gif (Max 2MB) Rec: 1366px x 484px)"
         }
       ]
    }
 ]

class SandboxController {
    async home ({ auth, params, response, request, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.home',{user,data,dummyData}) 
        }catch(e){
            return view.render('sandbox.home',{data,dummyData}) 
        }               
        
    }

    async productFormGeneral ({ auth, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.product_form-general',{user}) 
        }catch(e){
            return view.render('sandbox.product_form-general') 
        }               
    }

    async productFormSummary ({ auth, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.product_form-summary',{user}) 
        }catch(e){
            return view.render('sandbox.product_form-summary') 
        }               
    }

    async productFormTeam ({ auth, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.product_form-team',{user}) 
        }catch(e){
            return view.render('sandbox.product_form-team') 
        }               
    }

    async productFormFinish ({ auth, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.product_form-finish',{user}) 
        }catch(e){
            return view.render('sandbox.product_form-finish') 
        }               
    }

     async product({ auth, view}) {
        try{
            const user = await auth.getUser()
            return view.render('sandbox.product',{user}) 
        }catch(e){
            return view.render('sandbox.product') 
        }               
    }
}

module.exports = SandboxController
