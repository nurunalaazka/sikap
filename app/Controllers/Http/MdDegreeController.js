'use strict'
const MdDegree = use('App/Models/MdDegree')

class MdDegreeController {
  async create ({ request, view, response, auth, session }) {
    try {
      const { name } = request.all();

      await MdDegree.create({ name })

      session.flash({ 
        content: 'New Degree has been successfully created',
        title:'Create Success',
        type: 'success'
      })

      return response.redirect('/admin/degrees')

    } catch (error) {
      throw error
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const { degree, name } = request.all()

      degree.merge({name})

      await degree.save()

      session.flash({ 
        content: 'The Degree has been successfully updated',
        title:'Update Success',
        type: 'success'
      })

      return response.redirect('/admin/degrees')
    } catch (error) {
      throw error
    }
  }

  async delete ({ request, view, response, auth, session }) {
    try {
      const { degree } = request.all()

      await degree.delete()

      session.flash({ 
        content: 'The Degree has been successfully deleted',
        title:'Delete Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = MdDegreeController
