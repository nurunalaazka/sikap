'use strict'

const Project = use('App/Models/Project');
const ProjectMilestone = use('App/Models/ProjectMilestone');
const ResearchNeed = use('App/Models/ResearchNeed');
const User = use('App/Models/User');
const Chat = use('App/Models/Chat');
const ProjectFunding = use('App/Models/ProjectFunding');
const Payment = use('App/Models/Payment');
const PaymentHistory = use('App/Models/PaymentHistory');

const { v4: uuid } = require('uuid');;
const Database = use('Database');
const Encryption = use('Encryption');
const Helpers = use('Helpers')

class DashboardController {
    async index ({ auth, params, response, request, view}) {
        const user = await auth.getUser();        

        if(user.type === 'researcher'){
            return this.researcherDashboard(...arguments)
        }else{
            return this.investorDashboard(...arguments)
        }
    }

    async researcherDashboard ({ auth, params, response, request, view}) {
        try {
            const user = await auth.getUser();
            //get count product & project & funded project

            const countAllProject = await user.projects().getCount()
            const countFundedProject = await user.projects().whereHas('investor').getCount()
            const countProduct = await user.products().getCount()

            // get all research

            const researchs = await User
            .query().where('id',user.id)
            .with('projects', (project) => {
                project
                .with('projectMilestones', (item) =>{
                    item.with('payment')
                })
                .with('researchField')
                .with('studyArea')
                .with('investor')
            })
            .with('products', (i) => {
                i
                .with('researchField')
                .with('studyArea')
            })
            .fetch();            

            // console.log(researchs.toJSON()[0].projects[0].projectMilestones)
            // console.log(researchs.toJSON()[0].products)

            return view.render('dashboard.dashboard-researcher',{
                countAllProject:countAllProject, countFundedProject:countFundedProject,
                countProduct:countProduct, researchs:researchs.toJSON(),
                user
            })
        } catch (error) {
            throw error   
        }
    }

    async investorDashboard ({ auth, params, response, request, view}) {
        try {
            //state login logout belum dibedakan
            const user = await auth.getUser();
            
            const countAllProject = await Project.query()
            .where('investor_id',user.id).andWhere('status','ongoing')
            .getCount()

            const countFundedProject = await Project.query()
            .where('investor_id',user.id).andWhere('status','completed')
            .getCount()

            const countResearchNeed = await ResearchNeed.query()
            .where('investor_id',user.id).andWhere('status','active')
            .getCount()

            let researchs = (
                await User
                .query().where('id',user.id)
                .with('investorIn', (project) => {
                    project
                    .with('projectMilestones', (item)=>{
                        item.with('payment')
                    })
                    .with('researchField')
                    .with('studyArea')
                    .with('researcher',(researcher)=>{
                        researcher.with('userResearcher')
                    })
                })                
                .fetch() 
            ).toJSON();


            // const user = {username:'asdas'}
            return view.render('dashboard.dashboard-investor',{
                user, countAllProject, countFundedProject, researchs, countResearchNeed
            })
        } catch (error) {
            throw error   
        }
    }

    async redirectProjectProgress ({ request, view, response, auth }){
        try {
            const { ref } = request.all();

            const param = encodeURIComponent(Encryption.encrypt(ref))            

            return response.redirect('/project/progress?ref='+param);
            
        } catch (error) {
            throw error
        }
    }
    
}

module.exports = DashboardController
