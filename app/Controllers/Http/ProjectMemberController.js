'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const Project = use('App/Models/Project');
const User = use('App/Models/User');
const ProjectMember = use('App/Models/ProjectMember')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')


class ProjectMemberController {  

  async addTeamMembers ({ params, request, response, view, auth, session }) {
    try{     
      const user = await auth.getUser()
      let { id, member, _draft, state, save_to_draft } = request.all();
      
      // if(!member) return response.redirect('finish?_draft='+_draft);
      if(!member) member = []

      const project = request.project

      let index = member.length

      let newMembers = []      

      while(index--) {
        if(state[index] === 'deleted') {
          await project.projectMembers().where('researcher_id',member[index]).delete();

        } else if(state[index] === 'new') {
          newMembers.push({
            id:uuid(),project_id:id,
            researcher_id:member[index], 
            created_at:new Date(), updated_at:new Date(), 
            created_by: user.id
          })
        }
      }

      await Database
      .from('project_members')
      .insert(newMembers)     

      

      if(save_to_draft) {
        session.flash({ content: "The project's member list has been successfully saved to draft", title: 'Success, Saved to Draft', type:'success' })
        return response.redirect('back')

      } else {
        project.merge({
          status: 'published'
        })
  
        await project.save()
        
        session.flash({ content: "The project's member list has been successfully saved", title: 'Update Members Success', type:'success' })
        return response.redirect('finish?_draft='+_draft);
      }

    }catch(e){
      throw e
    }
  }

  async update ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser();
      const { member } = request.all();
      const project = request.project

      await project.projectMembers().delete()

      let data = []

      for (const item of member) {
        data.push({
          id:uuid(), 
          project_id:project.id,researcher_id:item,
          created_at:new Date(), updated_at:new Date(), created_by: user.id, modified_by: user.id
        })
      }

      await Database
      .from('project_members')
      .insert(data)

      session.flash({ message: "Project's member has been updated" })
      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }

  
}

module.exports = ProjectMemberController
