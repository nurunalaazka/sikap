'use strict'
const Helpers = use('Helpers')
const Encryption = use('Encryption')

class DownloadController {
    async image ({ request, view, response, auth }) {
        try {
            let { path } = request.all();
            
            const pub = Helpers.publicPath()
            
            path = Encryption.decrypt(path)
            
            return response.attachment(pub+path) //force download
            // return response.download(pub+path) //might be opened in new window
        } catch (error) {
            throw error
        }
    }

    async file ({ request, view, response, auth }) {
        try {
            let { path } = request.all();
            
            const pub = Helpers.publicPath()
            
            path = Encryption.decrypt(path)
            
            return response.attachment(pub+path) //force download
            // return response.download(pub+path) //might be opened in new window
        } catch (error) {
            throw error
        }
    }
}

module.exports = DownloadController
