'use strict'
const MdResearchField = use('App/Models/MdResearchField');
const Product = use('App/Models/Product');
const Project = use('App/Models/Project');
const ProductMember = use('App/Models/ProductMember')
const ProductImage= use('App/Models/ProductImage')
const Notification = use('App/Models/Notification')
const ProductCollaborationOffer = use('App/Models/ProductCollaborationOffer');

const Services = new(use('App/Services/Services'))
const Master = new(use('App/Services/Master'))

const User = use('App/Models/User');
const UserResearcher = use('App/Models/UserResearcher');

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Encryption = use('Encryption')


class ProductController {
  async detail ({ request, view, response, auth }) {
    try {
      const { ref } = request.all();

      const id = Encryption.decrypt(ref);
      
      const product = (
        await Product.query()
        .where('id', id)
        .with('researchField')
        .with('studyArea')
        .with('productImages')
        .fetch()
      ).toJSON();     

      let researcher_id = product[0].researcher_id

      const researcher = (
        await User.query()
        .where('id',researcher_id)
        .with('userResearcher')
        .fetch()
      ).toJSON()[0];

      const countProject = await Project.query()
      .where('researcher_id',researcher_id)   
      .whereHas('investor')   
      .getCount();

      const total_funding = await Project.query()
      .where('researcher_id',researcher_id)
      .whereHas('investor')
      .sum('required_fund as total') 

      const productMembers = (
        await ProductMember.query()
        .where('product_id', id)
        .with('user', (user)=>{
          user.with('userResearcher')
        })
        .fetch()
      ).toJSON()     

      return view.render('research.product.product',{ product, researcher, countProject, total_funding, members:productMembers });

    } catch (error) {
      throw error
    }
  }

  async showGeneralForm ({ params, request, response, view, session, auth }) {
    try{
      const user = await auth.getUser()
      const { _draft } = request.all()

      let userData = await user.userResearcher().fetch();     

      if(userData && userData.status !== 'verified') {
        session.flash({ content: 'Please fill in your information before attempting to create/submit a product. Then wait while we verify your information.', type:'success', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile')         
      } else if(!userData) {
        session.flash({ content: 'Please fill in your information before attempting to create/submit a product. Then wait while we verify your information.', type:'success', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile') 
      }

      let id, product;

      const researchFields = await Master.getResearchFields(true)

      if(_draft) {
        id = Encryption.decrypt(_draft)

        // product = await Product.find(id)
        product = (
          await Product.query()
          .where('id',id)
          .with('productImages')
          .fetch()
        ).toJSON()

        if(!product.length) {
          session.flash({ content: 'Product not found', title:'404 Not Found', type:'danger' })
          return response.redirect('back')
        }

        product = product[0]
      }

      return view.render('research.product.form.product_form-general',{researchFields, product})

    }catch(e){
      throw e
    }    
  }

  
  async create ({ request, response, view, auth, session }) {
    try{
      const user = await auth.getUser();

      const { researcher_id,
        title, sub_title, research_field_id, 
        study_area_id, duration, 
        duration_unit, date_start, save_to_draft, required_fund, cover_image_old, product_image_state, product_image_id 
      } = request.all();
      
      let { _draft } = request.all()
      let id = uuid();
      let new_product = true

      if(_draft) {
        id = Encryption.decrypt(_draft)
        new_product = false

      } else {
        _draft = Encryption.encrypt(id)        
      }    

      _draft = encodeURIComponent(_draft)

      let cover_path = null;

      if(cover_image_old) cover_path = cover_image_old

      const cover_image = request.file('cover_image');
      
      if(cover_image){
        cover_path = await Services.savePic(cover_image,'/uploads/product_cover', `${id}_product-cover.jpg`);
      }

      /* 
        upload multiple
      */

      let imagesStaticPath = `/uploads/product_images/${id}`
      let dataImages = []

      const productImages = request.file('product_images', {
        // types: [''],
        size: '10mb'
      })

      // console.log(productImages)

      const saved_images = await Services.uploadMultiple(productImages, imagesStaticPath, false, product_image_state)

      // console.log(saved_images)

      if(saved_images) {
        dataImages = saved_images.map(item=>{
          return {
            id:uuid(), product_id:id, name: item.fileName,
            location:`${imagesStaticPath}/${item.fileName}`, type: item.subtype, size: item.size, 
            created_by: user.id, created_at: new Date(), updated_at: new Date()
          }
        })
      }

      if(product_image_state) {
        let len = product_image_state.length

        while(len--){
          if(product_image_state[len]==='deleted'){
            //del file
            const productImage = await ProductImage.find(product_image_id[len])

            await Services.deleteFile(productImage.location)

            //del db
            await productImage.delete()
          }
        }
      }

      /* 
        database
      */

      let product = new Product();

      let temp = await Services.toSqlDate(date_start);

      if(researcher_id) {
        
        product.fill({
          researcher_id,id,title,sub_title,research_field_id, 
          study_area_id, cover_image:cover_path, required_fund,
          duration, duration_unit, date_start:temp, status: 'draft'
        }) 

        await product.save()

        await this.insertProductImages(dataImages)

        session.flash({ 
          content: 'New product has been successfully created',
          title: 'Product Saved',
          type: 'success'
        })
        return response.redirect('products')
      }
      else {
        if(new_product) {
          product.fill({
            id,title,sub_title,research_field_id, 
            study_area_id, cover_image:cover_path, required_fund,
            duration, duration_unit, date_start:temp, status: 'draft', created_by: user.id
          })  
        }
        else {
          product = await Product.find(id);

          product.merge({
            title,sub_title,research_field_id, 
            study_area_id, cover_image:cover_path, required_fund,
            duration, duration_unit, date_start:temp, modified_by: user.id
          })
        }
            
  
        await user.products().save(product);

        await this.insertProductImages(dataImages)


        if(save_to_draft) {
          session.flash({ 
            content: "The product's form has been successfully saved to draft",
            title: 'Success, Saved to Draft',
            type: 'success'
          })
          return response.redirect('back');

        } else {
          session.flash({ 
            content: 'The product has been successfully saved',
            title: 'Product Saved',
            type: 'success'
          })
          return response.redirect('/new-product/description?_draft='+_draft);

        }

      }      

    }catch(e){
      throw e
    }
  }

  async insertProductImages (data) {
    try {
      await Database
      .from('product_images')
      .insert(data)
      
    } catch (error) {
      throw error
    }
  }
  
  async showDescriptionForm ({ params, request, response, view, auth }) {
    try{
      const product = request.product

      return view.render('research.product.form.product_form-description',{product});

    }catch(e){
      throw e
    }

    
  }
  
  async insertDescription ({ params, request, response, view, session }) {
    try{
      const { _draft, description, save_to_draft } = request.all();

      let params = encodeURIComponent(_draft)

      const product = request.product
      
      product.merge({
        description
      });

      await product.save();

      if(save_to_draft) {
        session.flash({ 
          content: "The product's description has been successfully saved to draft",
          title: 'Success, Saved to Draft',
          type: 'success'
        })
        return response.redirect('back');

      }else{
        session.flash({ 
          content: "Product's description has been successfully updated",
          title: 'Update Description Success',
          type: 'success'
        })
        return response.redirect('/new-product/team-members?_draft='+params);
      }

      

    }catch(e){
      throw e
    }
    
  }
  
  async showTeamMembersForm ({ params, request, response, view, auth }) {
    try{
      const user = await auth.getUser();            

      const product = request.product

      const researcher = await user.userResearcher().fetch();

      let researchers = (
        await UserResearcher.query()
        .where('status','verified')
        .with('user')
        .fetch()
      ).toJSON()

      researchers = researchers.filter((item)=>{
        return item.user.id != user.id;
      });

      let members = (
        await ProductMember.query()
        .where('product_id',product.id)
        .with('user',(item)=>{
          item.with('userResearcher')
        }).fetch()
      ).toJSON()
      
      // await product.productMembers().fetch()

      // if(members) members = members.toJSON()

      return view.render('research.product.form.product_form-team',{user:user,researcher:researcher.toJSON(),researchers:researchers, members, product})

    }catch(e){
      throw e
    }
    
  }  
  
  async showSuccess ({ params, request, response, view, auth }) {
    try{      

      let { _draft } = request.all();

      _draft = encodeURIComponent(_draft)

      return view.render('research.product.form.product_form-success', {_draft})

    }catch(e){
      throw e
    }
    
  }

  async updateProduct ({ params, request, response, session, auth }) {
    try {
      const user = await auth.getUser()
      const {
        id,title, sub_title, researcher_id, research_field_id, study_area_id, 
        duration, date_start, duration_unit, cover_image_old, required_fund
      } = request.all()
      
      let temp = await Services.toSqlDate(date_start)
      let cover_path = cover_image_old || null;      
      
      const cover_image = request.file('cover_image');
      
      if(cover_image){
        cover_path = await Services.savePic(cover_image, '/uploads/product_cover', `${id}_product-cover.jpg`);
      }

      const product = await Product.find(id);

      product.merge({
        title, sub_title, researcher_id, research_field_id, study_area_id, 
        duration, date_start:temp, duration_unit, cover_image:cover_path, required_fund, modified_by: user.id
      })

      await product.save();

      session.flash({ 
        content: 'The product has been successfully updated',
        title: 'Update Product Success',
        type: 'success'
      })

      return response.redirect('back')      
    } catch (error) {
      throw error
    }
  }
  
  async deleteProduct ({ request, view, response, auth, session }) {
    try {
        const user = await auth.getUser()

        const product = request.product
        const img = product.cover_image

        if(!(product.status == 'draft' || product.status == 'published')) {
          session.flash({ message: 'You cannot delete published product' })
          return response.redirect('back')
        }

        if(user.role_id !== 'admin') {
          if(user.id !== product.researcher_id) {
            session.flash({ 
              content: 'You do not have permission to delete this product',
              title: 'Unauthorized Access',
              type: 'danger'
            })

            return response.redirect('back')
          }
        }

        const productImages = (
          await product.productImages().fetch()
        ).toJSON()

        if(productImages && productImages.length) {
          for (const item of productImages) {
            await Services.deleteFile(item.location)
          }

          await ProductImage.query()
          .where('product_id',product.id)
          .delete()
        }

        if(product.status == 'published') {
          await this.deleteCollaborationOffers(product)
        }

        await product.productMembers().delete()        

        await product.delete()
        await Services.deleteFile(img)

        session.flash({ 
          content: 'The Product has been successfully deleted',
          title: 'Product Delete Success',
          type: 'success'
        })
        return response.redirect('back')
    } catch (error) {
        throw error
    }
  }

  async deleteCollaborationOffers (product) {
    try {
      const productCollabs = (
        await product.productCollaborationOffers().fetch()
      ).toJSON()
      
      let chatIds = []

      if(productCollabs && productCollabs.length) {
        for (const item of productCollabs) {
          chatIds.push(item.chat_id)

          await Notification.create({
            user_id : item.investor_id,
            title:`The Product has been deleted by the researcher. You could ask for more information to the related researcher.`,
            content:product.title,
            thumbnail:'',
            action_link:'/profile/view?ref='+encodeURIComponent(Encryption.encrypt(product.researcher_id)),
            action_text:'view researcher profile',
            status:'unread',
            created_by:product.researcher_id
          })
        }

        await ProductCollaborationOffer.query()
        .where('product_id',product.id)
        .delete()
        
        await Services.deleteChats(chatIds)
      }

      return true
    } catch (error) {
      throw error
    }
  }

  async redirectPage ({ request, view, response, auth, session }) {
    try {
      const { id, form, action } = request.all()

      if(!id) {
        session.flash({ content: 'Missing required parameter id on request', title:'Bad Request', type:'danger' })
        return response.redirect('back')
      }

      const params =  encodeURIComponent(Encryption.encrypt(id))

      if(form) {
        if(form == 'general') return response.redirect('/new-product/general-form?_draft='+params)
        else if(form == 'description') return response.redirect('/new-product/description?_draft='+params)        
        else if(form == 'member') return response.redirect('/new-product/team-members?_draft='+params)
        else {
          session.flash({ content: 'Unknown request form', title:'Redirect Failed', type:'danger' })
          return response.redirect('back')
        }

      } else if(action) {
        if(action == 'delete') return response.redirect('/product/delete?ref='+params)
        else session.flash({ content: 'Unknown request action', title:'Redirect Failed', type:'danger' })
      }
      else {
        session.flash({ content: 'Missing required parameter form on request', title:'Redirect Failed', type:'danger' })
        return response.redirect('back')
      }
      
    } catch (error) {
      throw error
    }
  }
  
}

module.exports = ProductController
