'use strict'
const UserBankAccount = use('App/Models/UserBankAccount')

class UserBankAccountController {
  async verifyAccounts({ request, view, response, auth, session }) {
    try {
      const { userBankAccount } = request.all()

      userBankAccount.merge({
        status:'verified'
      })

      await userBankAccount.save()

      session.flash({ 
        content: 'The Bank account has been successfully verified',
        title: 'Verify Success',
        type: 'success'
      })

      return response.redirect('/admin/verify-bank-accounts')
    } catch (error) {
      throw error
    }
  }
  
}

module.exports = UserBankAccountController
