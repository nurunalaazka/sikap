'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const ResearchNeed = use('App/Models/ResearchNeed')
const Project = use('App/Models/Project');
const User = use('App/Models/User');
const ProjectMember = use('App/Models/ProjectMember')
const Encryption = use('Encryption')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')


class ResearchNeedController {
 
  async index ({ request, response, view, auth, session }) {
    try {
      const user = await auth.getUser();

      let userData = null
      // const researchNeeds = (await user.researchNeeds().fetch()).toJSON(); 
      if(user.type === 'investor individual') {
        userData = await user.userInvestorIndividual().fetch();
      } else if(user.type === 'investor company') {
        userData = await user.userInvestorCompany().fetch();
      }     

      if(userData && userData.status !== 'verified') {
        session.flash({ content: 'Please fill in your information before attempting to create a project. Then wait while we verify your information.', type:'warning', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile')         
      } else if(!userData) {
        session.flash({ content: 'Please fill in your information before attempting to create a project. Then wait while we verify your information.', type:'warning', title:'User Information Needed' })
        return response.redirect('/profile/edit-profile') 
      }
      
      const researchNeeds = (await ResearchNeed.query()
      .where('investor_id',user.id)
      .with('researchField')
      .with('studyArea')
      .fetch()).toJSON()

      const researchFields = (await MdResearchField.all()).toJSON();

      return view.render('research.research-needs', {researchFields,researchNeeds})
    } catch (error) {
      throw error
    }
  }

  
  async create ({ request, response, view, auth, session }) {
    try {
      const user = await auth.getUser();
      const { title, research_field_id, study_area_id, description } = request.all();
      const [status, created_by] = ['active', user.id]

      // console.log(request.all())

      const researchNeed = new ResearchNeed();

      researchNeed.fill({
        title, research_field_id, study_area_id, description, status, created_by
      })

      await user.researchNeeds().save(researchNeed);

      session.flash({ 
        content: 'New research need has been successfully added',
        title: 'Add Success',
        type: 'success'
      })

      return response.redirect('back')
      
    } catch (error) {
      throw error
    }
  }

  async updateStatus ({ request, view, response, auth }) {
    try {
      const { id } = request.all();
      let status = 'active'

      const researchNeed = await ResearchNeed.find(id);

      if(researchNeed.status === 'active'){
        status = 'inactive'
      } else {
        status = 'active'
      }

      researchNeed.merge({
        status
      })

      await researchNeed.save();

      return response.json({success:true})
    } catch (error) {
      throw error
    }
  }

 
  async store ({ request, response }) {
  }

  
  async show ({ params, request, response, view }) {
  }

  
  async edit ({ params, request, response, view }) {
  }
  
  async update ({ params, request, response, session }) {
    try {
      const { id, title, research_field_id, study_area_id, description } = request.all();

      const researchNeed = await ResearchNeed.find(id);

      researchNeed.merge({
        id ,title, research_field_id, study_area_id, description
      })

      await researchNeed.save();  
      
      session.flash({ 
        content: 'The research need has been successfully updated',
        title: 'Update Success',
        type: 'success'
      })

      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
  
  async delete ({ params, request, response, session, auth }) {
    try {
      const user = await auth.getUser();

      const { id } = request.all();

      const researchNeed = await ResearchNeed.find(id);

      if(user.id !== researchNeed.investor_id){
        session.flash({message:'Unauthorized access to resource!'})
        return response.redirect('back');
      }

      await researchNeed.delete();

      session.flash({ 
        content: 'The research need has been successfully deleted',
        title: 'Delete Success',
        type: 'success'
      })

      return response.redirect('back')

    } catch (error) {
      throw error
    }
  }
}

module.exports = ResearchNeedController
