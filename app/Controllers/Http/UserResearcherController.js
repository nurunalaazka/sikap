'use strict'

const UserResearcher = use('App/Models/UserResearcher')
const UserBankAccount = use('App/Models/UserBankAccount')
const MdResearchField = use('App/Models/MdResearchField')
const MdBank = use('App/Models/MdBank')
const User = use('App/Models/User')
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual')
const UserInvestorCompany = use('App/Models/UserInvestorCompany')
const MdCountry = use('App/Models/MdCountry')
const MdProvince = use('App/Models/MdProvince')
const MdCity = use('App/Models/MdCity')
const MdDistrict = use('App/Models/MdDistrict')
const Services = use('App/Services/Services')
const savePic = new Services().savePic

const Database = use('Database')
const Helpers = use('Helpers')

class UserResearcherController {
 
  async update ({ params, request, response, session, auth }) {
    try {
      const admin = await auth.getUser()
      const { 
        name, username, email, role_id, user_status, user_information_status, profile_picture_old,
        id_card_old, gender, phone, institution, position, research_field_id, study_area_id, scopus_index,
        google_scholar_link, id, user
      } = request.all();      

      let profile_picture = profile_picture_old || null
      let id_card = id_card_old

      const profile_pic = request.file('profile_picture')
      const id_pic = request.file('id_card')

      if(profile_pic) profile_picture = await savePic(profile_pic, '/uploads/profile_picture', `${id}_prof-pic.jpg`)
      if(id_pic) id_card = await savePic(id_pic, '/uploads/id_card', `${id}_id-card.jpg`)

      let userResearcher = await user.userResearcher().fetch();

      if(userResearcher) {
        
        userResearcher.merge({
          gender, phone, institution, position, research_field_id, study_area_id, scopus_index,
          google_scholar_link, profile_picture, id_card, status: user_information_status, modified_by:admin.id
        })

      } else {        

        userResearcher = new UserResearcher()

        userResearcher.fill({
          gender, phone, institution, position, research_field_id, study_area_id, scopus_index,
          google_scholar_link, profile_picture, id_card, status: user_information_status, created_by:admin.id
        })

      }      
      
      user.merge({
        name, username, email, role_id, status: user_status, modified_by:admin.id
      })      

      const trx = await Database.beginTransaction();

      await user.save(trx);
      await user.userResearcher().save(userResearcher,trx)

      trx.commit();

      session.flash({content:'User data has been updated', type:'success', title:'Update Success'})
      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }
  
  async destroy ({ params, request, response }) {
  }
}

module.exports = UserResearcherController
