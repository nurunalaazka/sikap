'use strict'

const Product = use('App/Models/Product');
const User = use('App/Models/User');
const ProductMember = use('App/Models/ProductMember')

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Encryption = use('Encryption')


class ProductMemberController {
  async addTeamMembers ({ params, request, response, view, auth, session }) {
    try{     
      const user = await auth.getUser()

      let { member, state, _draft, save_to_draft } = request.all();
      
      let params = encodeURIComponent(_draft)

      // if(!member) return response.redirect('finish?_draft='+params);
      if(!member) member = []

      const product = request.product;
      let id = product.id
      
      let index = member.length

      let newMembers = []      

      while(index--) {
        if(state[index] === 'deleted') {
          await product.productMembers().where('researcher_id',member[index]).delete();

        } else if(state[index] === 'new') {
          newMembers.push({
            id:uuid(),product_id:id,
            researcher_id:member[index],
            created_at:new Date(), updated_at:new Date(),
            created_by: user.id
          })
        }
      }

      await Database
      .from('product_members')
      .insert(newMembers)


      if(save_to_draft) {
        session.flash({ 
          content: "The product's description has been successfully saved to draft",
          title: 'Success, Saved to Draft',
          type: 'success'
        })
        return response.redirect('back');

      }else{ 
        product.merge({
          status:'published'
        })

        await product.save()

        session.flash({ 
          content: "The product's member list has been successfully saved",
          title: 'Update Members Success',
          type: 'success'
        })

        return response.redirect('finish?_draft='+params); 
      }

           

    }catch(e){
      throw e
    }
  }

}

module.exports = ProductMemberController
