'use strict'
const MdResearchField = use('App/Models/MdResearchField');
const Project = use('App/Models/Project');
const ProjectMilestone = use('App/Models/ProjectMilestone');
const User = use('App/Models/User');
const ProjectMember = use('App/Models/ProjectMember')
const Notification = use('App/Models/Notification')


const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')
const Encryption = use('Encryption');


class ProjectMilestoneController {
 
  async index ({ request, response, view }) {
  }

  
  async createMilestones ({ request, response, view, auth, session }) {
    try{
      let user = await auth.getUser()
      const {id, title, percentage, duration, duration_unit, required_fund, description, _draft, state, milestone_id, save_to_draft } = request.all();

      console.log(request.all())
      let totalPercent = 0

      let len = percentage.length

      while(len--) {
        if(state[len] !== 'deleted') totalPercent+=parseInt(percentage[len])        
      }

      console.log(totalPercent)

      // for (const item of percentage) {
      //   totalPercent+=parseInt(item)
      // }

      if(totalPercent != 100) {        
        session.flash({ content: "The milestone's percentage has to be 100% in total", title: 'Failed to save milestone(s)', type:'danger' })
        return response.redirect('back');
      }
      
      /**
       * state: deleted, new , old
       */
     
      let orderModifier = 1
      let index = 0
      
      while(index < title.length) {

        if(state[index] === 'deleted') {
          orderModifier--

          await ProjectMilestone.query()
          .where('id',milestone_id[index])
          .delete()
        }
        else {
          let data = {
            project_id:id,
            title:title[index], percentage:percentage[index], 
            duration:duration[index], duration_unit:duration_unit[index],
            required_fund:required_fund[index], description:description[index],
            order:index+orderModifier, status: 'new'
          }          

          if(state[index] === 'new'){
            data.created_by = user.id
            data.created_at = new Date()
            data.updated_at = new Date()
            data.id = uuid()

            await Database
            .table('project_milestones')
            .insert(data)
          }
          else if(state[index] === 'old'){
            data.modified_by = user.id
            data.updated_at = new Date()

            await Database
            .table('project_milestones')
            .where('id',milestone_id[index])
            .update(data)

          }
        }       

        index++
      }

      if(save_to_draft) {
        session.flash({ content: "The project's milestone(s) has been successfully saved to draft", title: 'Success, Saved to Draft', type:'success' })
        return response.redirect('back');
      } else {
        session.flash({ content: "The project's milestone(s) has been successfully saved", title: 'Save Milestone Success', type:'success' })
        return response.redirect('team-members?_draft='+_draft);
      }

    }catch(e){
      throw e
    } 
  }

  async update ({ params, request, response, session, auth }) {
    try {
      let user = await auth.getUser()
      const { title, percentage, duration, duration_unit, 
        required_fund, description, projectMilestone 
      } = request.all();

      projectMilestone.merge({
        title, percentage, duration, duration_unit, 
        required_fund, description, modified_by:user.id
      })

      await projectMilestone.save()

      return response.redirect('back')      
      
    } catch (error) {
      throw error
    }
  }

  async updateBulk ({ params, request, response, session, auth }) {
    try {
      let user = await auth.getUser()
      const { id, title, percentage, duration, duration_unit, 
        required_fund, description, project_id 
      } = request.all();

      const project = await Project.find(project_id[0])

      let i = title.length

      while(i--) {
        let [ mid, mtitle, mpercentage, mduration, mduration_unit, mrequired_fund, mdescription ] = [
          id[i], title[i], percentage[i], duration[i], duration_unit[i], required_fund[i], description[i]
        ]

        if(mid && mid !== '') {
          await Database.table('project_milestones')
          .where('id',mid)
          .update({
            title:mtitle, percentage:mpercentage, order:i,
            duration:mduration, duration_unit:mduration_unit,
            required_fund:mrequired_fund, description:mdescription,
            updated_at:new Date(), modified_by: user.id
          })
        }
        else{
          await Database
          .table('project_milestones')
          .insert({id:uuid(), project_id:project.id, status: 'new', order: i,
            title:mtitle, percentage:mpercentage,
            duration:mduration, duration_unit:mduration_unit,
            required_fund:mrequired_fund, description:mdescription,
            created_at:new Date(), updated_at:new Date(), created_by: user.id
          })
        }

      }
      
      session.flash({message:'Milestone has been updated'})
      return response.redirect('back')      
      
    } catch (error) {
      throw error
    }
  }
 
  async delete ({ request, view, response, auth }) {
    try {
      
    } catch (error) {
      throw error
    }
  }

  async updateStatus ({ request, view, response, auth, session }) {
    try {
      const user = await auth.getUser();

      const { projectMilestone } = request.all()

      const nextOrder = projectMilestone.order + 1 
      
      const project = await projectMilestone.project().fetch()

      const ref = encodeURIComponent(Encryption.encrypt(project.id))

      projectMilestone.merge({
        status: 'completed'
      })

      await ProjectMilestone.query()
      .where('project_id',projectMilestone.project_id)
      .andWhere('order',nextOrder)
      .update({status:'on going'})

      await Notification.create({
        user_id : project.investor_id,
        title:`${user.name} has marked milestone ${projectMilestone.title} as completed on the following research:`,
        content:project.title,
        thumbnail:'/img/notifications/progress_completed.svg',
        action_link:`/project/detail?ref=${ref}&tab=progress`,
        action_text:'view detail',
        status:'unread',
        created_by:user.id
      })

      await projectMilestone.save()

      session.flash({ content: "Milestone's status has been changed to 'completed'", title:'Success, Milestone Marked as Completed', type:'success' })
      return response.redirect('back')
    } catch (error) {
      throw error
    }
  }

  async getMilestone ({ request, view, response, auth }) {
    try {
      const { id } = request.all()

      const data = (
        await ProjectMilestone.find(id)
      ).toJSON()

      return response.json({status:200, data})
    } catch (error) {
      throw error
    }
  }
}

module.exports = ProjectMilestoneController
