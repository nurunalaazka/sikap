'use strict'

const MdResearchField = use('App/Models/MdResearchField');
const MdRole = use('App/Models/MdRole');
const MdPermission = use('App/Models/MdPermission');
const MdRolePermission = use('App/Models/MdRolePermission');
const MdStudyArea = use('App/Models/MdStudyArea');
const ResearchNeed = use('App/Models/ResearchNeed')
const Project = use('App/Models/Project');
const Product = use('App/Models/Product');
const User = use('App/Models/User');
const UserBankAccount = use('App/Models/UserBankAccount');
const UserResearcher = use('App/Models/UserResearcher');
const UserInvestorCompany = use('App/Models/UserInvestorCompany');
const UserInvestorIndividual = use('App/Models/UserInvestorIndividual');
const ProjectMember = use('App/Models/ProjectMember')
const Encryption = use('Encryption')
const Master = new (use('App/Services/Master'))
const Props = new (use('App/Services/Props'))
const getRoles = Master.getRoles

const { v4: uuid } = require('uuid');
const Database = use('Database')
const Helpers = use('Helpers')

class AdminController {
    async showResearchNeeds ({ request, view, response, auth }) {
        try {

            const researchNeeds = (await ResearchNeed.query()
            .where('status','active')
            .with('investor')
            .with('researchField')
            .with('studyArea')
            .fetch()).toJSON();

            // console.log(researchNeeds)

            return view.render('admin.admin-research-needs',{researchNeeds})
            
        } catch (error) {
            throw error
        }
    }
    
    async showVerifyUsers ({ request, view, response, auth }) {
        try {
           const userResearchers = (await UserResearcher.query()
           .where('status','unverified')
           .with('user')
           .with('researchField')
           .with('studyArea')
           .fetch()).toJSON();

           const userInvestorCompany = (await UserInvestorCompany.query()
           .where('status','unverified')
           .with('user')
           .with('industry')
           .with('industryCategory')
           .with('country')
           .with('province')
           .with('district')
           .with('city')
           .fetch()).toJSON();

           const userInvestorIndividual = (await UserInvestorIndividual.query()
           .where('status','unverified')
           .with('user')
           .with('industry')
           .with('industryCategory')
           .with('country')
           .with('province')
           .with('district')
           .with('city')
           .fetch()).toJSON();

        //    console.log(userInvestorIndividual)

            return view.render('admin.admin-user-information', {userResearchers, userInvestorCompany, userInvestorIndividual})
            
        } catch (error) {
            throw error
        }
    }

    async verifyUsers ({ request, view, response, auth, session }) {
        try {
            const { id, type } = request.all();

            console.log(id, type);

            if(type == 'researcher') {
                const userResearcher = await UserResearcher.find(id);

                userResearcher.merge({
                    status: 'verified'
                })

                await userResearcher.save();
            }
            else if(type == 'investor-company') {
                const userInvestorCompany = await UserInvestorCompany.find(id);

                userInvestorCompany.merge({
                    status:'verified'
                })

                await userInvestorCompany.save();


            }
            else if(type == 'investor-individual') {
                const userInvestorIndividual = await UserInvestorIndividual.find(id);

                userInvestorIndividual.merge({
                    status:'verified'
                })

                await userInvestorIndividual.save();
            }
            else {
                session.flash({ message: 'Invalid account type!' })
            }

            session.flash({ 
                content: 'The user information has been successfully verified',
                title: 'Verify Success',
                type: 'success'
            })

            return response.redirect('back')

        } catch (error) {
            throw error
        }
    }

    async showVerifyBankAccounts ({ request, view, response, auth }) {
        try {
            const userBankAccounts = (
                await UserBankAccount.query()
                .where('status','unverified')
                .with('bank')
                .fetch()
            ).toJSON()

            return view.render('admin.admin-verify-bank-account', {userBankAccounts})            
        } catch (error) {
            throw error
        }
    }

    async indexProject ({ request, view, response, auth }) {
        try {
            const projects = (
                await Project.query()
                .with('researchField')
                .with('studyArea')
                .fetch()
            ).toJSON()
            
            // console.log(projects)

            return view.render('admin.admin-projects',{projects})
        } catch (error) {
            throw error
        }
    }

    async indexProduct ({ request, view, response, auth }) {
        try {
            const products = (
                await Product.query()
                .with('researchField')
                .with('studyArea')
                .fetch()
            ).toJSON()
            
            // console.log(products)

            return view.render('admin.admin-products',{products})
        } catch (error) {
            throw error
        }
    }

    async indexUser ({ request, view, response, auth }) {
        try {
            const users = (
                await User.query()                
                .fetch()
            ).toJSON()
            
            // console.log(users)

            return view.render('admin.admin-users',{users})
        } catch (error) {
            throw error
        }
    }

    async createProject ({ request, view, response, auth }) {
        try {
            const props = await this.getProps(...arguments)

            const researchers = props.researchers
            const researchFields = props.researchFields

            const duration_unit = await Props.duration_unit();

            return view.render('admin.forms.admin-create-project',{researchers, researchFields, duration_unit})
        } catch (error) {
            throw error
        }
    }

    async createProduct ({ request, view, response, auth }) {
        try {
            const props = await this.getProps(...arguments)

            const researchers = props.researchers
            const researchFields = props.researchFields

            const duration_unit = await Props.duration_unit();


            return view.render('admin.forms.admin-create-product',{researchers, researchFields, duration_unit})
        } catch (error) {
            throw error
        }
    }

    async getProps ({auth}) {
        try {
            let researchers = (
                await User.query().where('type','researcher').fetch()
            ).toJSON();

            let researchFields = (
                await MdResearchField.all()
            ).toJSON();

            let studyAreas = (
                await MdStudyArea.all()
            ).toJSON()

            researchers = researchers.map(item=>{
                return {value:item.id, label:item.name}
            })

            researchFields = researchFields.map(item=>{
                return {value:item.id, label:item.name}
            })

            studyAreas = studyAreas.map(item=>{
                return {value:item.id, label:item.name}
            })

            return {researchers, researchFields, studyAreas}
            
        } catch (error) {
            throw error
        }
    }    

    async createUser ({ request, view, response, auth }) {
        try {           
            const roles = await getRoles(true)
            const accountTypes = await Props.accountTypes();

            return view.render('admin.forms.admin-create-user',{roles, accountTypes})
        } catch (error) {
            throw error
        }
    }

    async register ({ request, view, response, auth, session }) {
        try{
            const {name, email, password, username, type, role_id} = request.all()  
            
            console.log(request.all())
            
            const user = await User.create({                
                name,
                username,
                email,
                password,
                type,
                role_id,
                status:'verified'
            })            
        /* 
            let loggedInUser = await auth.attempt(email,password)           

            let token = await auth
            .authenticator('jwt')
            .generate(loggedInUser)

            user.token = token.token
            user.link = `http://localhost:3333/signup/register-verification/token/${token.token}`

            let verification = new Verification()

            verification.fill({
                token:token.token,
                type:'verification_token'
            })

            await loggedInUser.verifications().save(verification)

            await Mail.send('emails.verified-user', user.toJSON(), (message) => {
                message
                  .to(user.email)
                  .from('<from-email>')
                  .subject('Welcome to Maroojic')
            })

            await auth.logout()

        */       

        session.flash({ 
            content: 'New user has been successfully created',
            title: 'Create User Success',
            type: 'success'
        })
       
        return response.redirect('back')

        }catch(e){
            throw e
        }
    }

    async updateProject ({ request, view, response, auth }) {
        try {
            const { id } = request.all()
            const props = await this.getProps(...arguments)
            const researchFields = props.researchFields
            const studyAreas = props.studyAreas
            const researchers = props.researchers
            const project = await Project.find(id)    
            const duration_unit = await Props.duration_unit();
            

            return view.render('admin.forms.admin-update-project',{project, researchFields, studyAreas, researchers, duration_unit})
        } catch (error) {
            throw error
        }
    }

    async updateProduct ({ request, view, response, auth }) {
        try {
            const { id } = request.all()
            const props = await this.getProps(...arguments)
            const researchFields = props.researchFields
            const studyAreas = props.studyAreas
            const researchers = props.researchers
            const product = await Product.find(id)
            const duration_unit = await Props.duration_unit()


            return view.render('admin.forms.admin-update-product',{product, researchFields, studyAreas, researchers, duration_unit})
        } catch (error) {
            throw error
        }
    }

    async updateUser ({ request, view, response, auth }) {
        try{
            const { id, user } = request.all();

            const type = user.type
            let userData, countries, districts, cities, provinces, industries, industryCategories;
            const roles = await getRoles(true)
            const userStatus = await Props.userStatus();
            const userInformationStatus = await Props.userInformationStatus();

            if(type === 'researcher') {
                userData = (
                    await User.query()
                    .where('id',id)                    
                    .with('userResearcher', (user)=>{
                        user.with('researchField')
                        .with('studyArea')
                    })
                    .fetch()
                ).toJSON()

            }
            else if(type === 'investor company') {
                userData = (
                    await User.query()
                    .where('id',id)
                    .with('userInvestorCompany', (user)=>{
                        user.with('industry')
                        .with('industryCategory')
                        .with('country')
                        .with('province')
                        .with('city')
                        .with('district')
                    })                    
                    .fetch()
                ).toJSON()
            }
            else if(type === 'investor individual') {
                userData = (
                    await User.query()
                    .where('id',id)
                    .with('userInvestorIndividual', (user)=>{
                        user.with('industry')
                        .with('industryCategory')
                        .with('country')
                        .with('province')
                        .with('city')
                        .with('district')
                    })                    
                    .fetch()
                ).toJSON()
            }

            if(type !== 'researcher') {
                countries = await Master.getCountries(true);
                districts = await Master.getDistricts(true);
                provinces = await Master.getProvinces(true);
                cities = await Master.getCities(true);
                industries = await Master.getIndustries(true);
                industryCategories = await Master.getIndustryCategories(true);
            }

            const props = await this.getProps(...arguments);
            const researchFields = props.researchFields
            const studyAreas = props.studyAreas            
            
            return view.render('admin.forms.admin-update-user',{
                roles, userStatus, userInformationStatus, researchFields, studyAreas, user:userData[0], type,
                countries, districts, cities, provinces, industries, industryCategories
            })

        }catch(e) {
            throw e
        }
    }

    async indexRole ({ request, view, response, auth }) {
        try {
            const roles = await getRoles();

            return view.render('admin.admin-role',{
                roles
            })
        } catch (error) {
            throw error
        }
    }

    async createRole ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-role')
        } catch (error) {
            throw error
        }
    }

    async updateRole ({ request, view, response, auth }) {
        try {
            const { role } = request.all();

            return view.render('admin.forms.admin-update-role',{
                role: role.toJSON()
            })
            
        } catch (error) {
            throw error
        }
    }

    async indexResearchField ({ request, view, response, auth }) {
        try {
            const researchFields = await Master.getResearchFields();

            return view.render('admin.admin-research-fields',{researchFields})
            
        } catch (error) {
            throw error
        }
    }
    
    async createResearchField ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-research-field')
        } catch (error) {
            throw error
        }
    }

    async updateResearchField ({ request, view, response, auth }) {
        try {
            const { researchField } = request.all()

            return view.render('admin.forms.admin-update-research-field',{researchField: researchField.toJSON()})
        } catch (error) {
            throw error
        }
    }

    async indexStudyArea ({ request, view, response, auth }) {
        try {
            const studyAreas = await Master.getStudyAreas();

            return view.render('admin.admin-study-areas',{studyAreas})
            
        } catch (error) {
            throw error
        }
    }
    
    async createStudyArea ({ request, view, response, auth }) {
        try {
            const researchFields = await Master.getResearchFields(true)

            return view.render('admin.forms.admin-create-study-area',{researchFields})
        } catch (error) {
            throw error
        }
    }

    async updateStudyArea ({ request, view, response, auth }) {
        try {
            const { studyArea } = request.all()
            const researchFields = await Master.getResearchFields(true)

            return view.render('admin.forms.admin-update-study-area',{studyArea: studyArea.toJSON(), researchFields})
        } catch (error) {
            throw error
        }
    }

    async indexPermission ({ request, view, response, auth }) {
        try {
            const permissions = await Master.getPermissions();

            return view.render('admin.admin-permissions',{permissions})
            
        } catch (error) {
            throw error
        }
    }
    
    async createPermission ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-permission')
        } catch (error) {
            throw error
        }
    }

    async updatePermission ({ request, view, response, auth }) {
        try {
            const { permission } = request.all()

            return view.render('admin.forms.admin-update-permission',{permission: permission.toJSON()})
        } catch (error) {
            throw error
        }
    }

    async indexBank ({ request, view, response, auth }) {
        try {
            const banks = await Master.getBanks();

            return view.render('admin.admin-banks',{banks})
            
        } catch (error) {
            throw error
        }
    }
    
    async createBank ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-bank')
        } catch (error) {
            throw error
        }
    }

    async updateBank ({ request, view, response, auth }) {
        try {
            const { bank } = request.all()

            return view.render('admin.forms.admin-update-bank',{bank: bank.toJSON()})
        } catch (error) {
            throw error
        }
    }

    async indexCountry ({ request, view, response, auth }) {
        try {
            const countries = await Master.getCountries();

            return view.render('admin.admin-countries',{countries})
            
        } catch (error) {
            throw error
        }
    }
    
    async createCountry ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-country')
        } catch (error) {
            throw error
        }
    }

    async updateCountry ({ request, view, response, auth }) {
        try {
            const { country } = request.all()

            return view.render('admin.forms.admin-update-country',{country: country.toJSON()})
        } catch (error) {
            throw error
        }
    }

    async indexProvince ({ request, view, response, auth }) {
        try {
            const provinces = await Master.getProvinces();

            return view.render('admin.admin-provinces',{provinces})
            
        } catch (error) {
            throw error
        }
    }
    
    async createProvince ({ request, view, response, auth }) {
        try {
            const countries = await Master.getCountries(true);
            return view.render('admin.forms.admin-create-province',{countries})
        } catch (error) {
            throw error
        }
    }

    async updateProvince ({ request, view, response, auth }) {
        try {
            const { province } = request.all()
            const countries = await Master.getCountries(true);

            return view.render('admin.forms.admin-update-province',{province: province.toJSON(), countries})
        } catch (error) {
            throw error
        }
    }

    async indexCity ({ request, view, response, auth }) {
        try {
            const cities = await Master.getCities();

            return view.render('admin.admin-cities',{cities})
            
        } catch (error) {
            throw error
        }
    }
    
    async createCity ({ request, view, response, auth }) {
        try {            
            const countries = await Master.getCountries(true);
            const provinces = await Master.getProvinces(true);

            return view.render('admin.forms.admin-create-city',{ countries, provinces})
        } catch (error) {
            throw error
        }
    }

    async updateCity ({ request, view, response, auth }) {
        try {
            const { city } = request.all()
            const countries = await Master.getCountries(true);
            const provinces = await Master.getProvinces(true);

            return view.render('admin.forms.admin-update-city',{city: city.toJSON(), countries, provinces})
        } catch (error) {
            throw error
        }
    }

    async indexDistrict ({ request, view, response, auth }) {
        try {
            const districts = await Master.getDistricts();

            return view.render('admin.admin-districts',{districts})
            
        } catch (error) {
            throw error
        }
    }
    
    async createDistrict ({ request, view, response, auth }) {
        try {            
            const countries = await Master.getCountries(true);
            const provinces = await Master.getProvinces(true);
            const cities = await Master.getCities(true);

            return view.render('admin.forms.admin-create-district',{ countries, provinces, cities})
        } catch (error) {
            throw error
        }
    }

    async updateDistrict ({ request, view, response, auth }) {
        try {
            const { district } = request.all()
            const countries = await Master.getCountries(true);
            const provinces = await Master.getProvinces(true);
            const cities = await Master.getCities(true);

            return view.render('admin.forms.admin-update-district',{district: district.toJSON(), countries, provinces, cities})
        } catch (error) {
            throw error
        }
    }

    async indexIndustry ({ request, view, response, auth }) {
        try {
            const industries = await Master.getIndustries();

            return view.render('admin.admin-industries',{industries})
            
        } catch (error) {
            throw error
        }
    }
    
    async createIndustry ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-industry')
        } catch (error) {
            throw error
        }
    }

    async updateIndustry ({ request, view, response, auth }) {
        try {
            const { industry } = request.all()            

            return view.render('admin.forms.admin-update-industry',{industry: industry.toJSON()})
        } catch (error) {
            throw error
        }
    }

    async indexIndustryCategory ({ request, view, response, auth }) {
        try {
            const industryCategories = await Master.getIndustryCategories();

            return view.render('admin.admin-industry-categories',{industryCategories})
            
        } catch (error) {
            throw error
        }
    }
    
    async createIndustryCategory ({ request, view, response, auth }) {
        try {            
            const industries = await Master.getIndustries(true)

            return view.render('admin.forms.admin-create-industry-category',{industries})
        } catch (error) {
            throw error
        }
    }

    async updateIndustryCategory ({ request, view, response, auth }) {
        try {
            const { industryCategory } = request.all()
            const industries = await Master.getIndustries(true)

            return view.render('admin.forms.admin-update-industry-category',{industryCategory: industryCategory.toJSON(), industries})
        } catch (error) {
            throw error
        }
    }

    async indexDegree ({ request, view, response, auth }) {
        try {
            const degrees = await Master.getDegrees();

            return view.render('admin.admin-degrees',{degrees})
            
        } catch (error) {
            throw error
        }
    }
    
    async createDegree ({ request, view, response, auth }) {
        try {
            return view.render('admin.forms.admin-create-degree')
        } catch (error) {
            throw error
        }
    }

    async updateDegree ({ request, view, response, auth }) {
        try {
            const { degree } = request.all()            

            return view.render('admin.forms.admin-update-degree',{degree: degree.toJSON()})
        } catch (error) {
            throw error
        }
    }

}

module.exports = AdminController
