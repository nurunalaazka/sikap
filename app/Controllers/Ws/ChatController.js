'use strict'
const ChatContent = use('App/Models/ChatContent')
const Chat = use('App/Models/Chat')
const ChatMember = use('App/Models/ChatMember')
const ChatStatus = use('App/Models/ChatStatus')
const Room = use('App/Models/Room')
const Services = new (use('App/Services/Services'))
const { v4: uuid } = require('uuid');


class ChatController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
  }

  async onMessage (message) {
    try{
      // console.log('user joined with %s socket id', this.socket.id)
      // console.log(message)

      let customMessage = {
        message:message.body,
        created_at:'just now',
        sender:message.user_id,
        room:message.room_id
      }

      this.socket.broadcastToAll('message', customMessage);      

      await this.saveMessage(message)

    }catch(e) {
      throw e
    }    
    
  }

  async saveMessage (message) {
    try {
      const chatMember = (
        await ChatMember.query()
        .where('room_id',message.room_id)
        // .where('user_id',message.user_id).limit(1)
        .fetch() 
      ).toJSON()
      
      let sender;
      let receiver;
      let i = chatMember.length

      while(i--) {
        if (chatMember[i].user_id == message.user_id) {
          sender = chatMember[i].id
        } else {
          receiver = chatMember[i].id
        }
      }
      

      if(chatMember.length) {
        let content_id = uuid()

        await ChatContent.create({ 
          id:content_id,
          room_id:message.room_id,
          sender_id:sender,     
          content: message.body,
          sender_status: 'sent',
          // receiver_status: 'unread'
        })  

        await ChatStatus.create({
          chat_content_id: content_id,
          chat_member_id: receiver,
          read_status: 'unread'
        })

      }
      
    } catch (error) {
      throw error
    }
  }
}

module.exports = ChatController
