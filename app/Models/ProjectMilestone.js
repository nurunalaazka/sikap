'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class ProjectMilestone extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    project () {
        return this.belongsTo('App/Models/Project')
    }

    projectMilestoneProgressess () {
        return this.hasMany('App/Models/ProjectMilestoneProgress')
    }

    payment () {
        return this.hasOne('App/Models/Payment')
    }
}

module.exports = ProjectMilestone
