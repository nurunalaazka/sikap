'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ProductCollaborationOffer extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    investor () {
        return this.belongsTo('App/Models/User','investor_id','id')
    }

    product () {
        return this.belongsTo('App/Models/Product')
    }

    chat () {
        return this.belongsTo('App/Models/Chat')
    }
}

module.exports = ProductCollaborationOffer
