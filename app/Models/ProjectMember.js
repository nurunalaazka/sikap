'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ProjectMember extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    user () {
        return this.belongsTo('App/Models/User','researcher_id','id')
    }
    
    project () {
        return this.belongsTo('App/Models/Project','project_id','id')
    }
}

module.exports = ProjectMember
