'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ChatStatus extends Model {
    static get connection () {
        return 'chat'
    }
    
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    chatContent () {
        return this.belongsTo('App/Models/ChatContent')
    }    

    chatMember () {
        return this.belongsTo('App/Models/ChatMember')
    }
}

module.exports = ChatStatus
