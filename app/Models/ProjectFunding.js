'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ProjectFunding extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    project () {
        return this.belongsTo('App/Models/Project')
    }

    investor () {
        return this.belongsTo('App/Models/User','investor_id','id')
    }

    chat () {
        return this.belongsTo('App/Models/Chat')
    }
    
}

module.exports = ProjectFunding
