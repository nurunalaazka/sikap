'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class UserBankAccount extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (bankAccount) => {                        
            bankAccount.id = uuid()
        })
    }

    static get incrementing () {
        return false
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    bank () {
        return this.belongsTo('App/Models/MdBank','bank_id','id')
    }
}

module.exports = UserBankAccount
