'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdResearchField extends Model {
    static boot () {
        super.boot()
    
        /**
         * A hook to hash the user password before saving
         * it to the database.
         */
        this.addHook('beforeCreate', async (rFieldInstance) => {          
            
            rFieldInstance.id = uuid()          
          
        })
                
    }

    static get incrementing () {
        return false
    }

    studyAreas () {        
        return this.hasMany('App/Models/MdStudyArea','id','research_field_id')
    }

    projects () {        
        return this.hasMany('App/Models/Project','id','research_field_id')
    }

    products () {        
        return this.hasMany('App/Models/Product','id','research_field_id')
    }
}

module.exports = MdResearchField
