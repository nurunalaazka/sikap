'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdCity extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {                        
            instance.id = uuid()
        })
    }

    districts () {
        return this.hasMany('App/Models/MdDistrict','id','city_id')
    }

    country () {
        return this.belongsTo('App/Models/MdCountry','country_id','id')
    }

    province () {
        return this.belongsTo('App/Models/MdProvince','province_id','id')
    }

    static get incrementing () {
        return false
    }
}

module.exports = MdCity
