'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class MdRole extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = instance.name.toLowerCase();
            }
        })
    }    

    static get incrementing () {
        return false
    }
}

module.exports = MdRole
