'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
const { v4: uuid } = require('uuid');

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeCreate', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)       
      }

      if(!userInstance.id){
        userInstance.id = uuid()
      }
      
    })
  }

  static get incrementing () {
    return false
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  verifications () {
    return this.hasMany('App/Models/Verification')
  }

  researchNeeds () {
    return this.hasMany('App/Models/ResearchNeed')
  }

  userResearcher () {
    return this.hasOne('App/Models/UserResearcher')
  }

  userResearcherExperience () {
    return this.hasMany('App/Models/UserResearcherExperience')
  }

  userResearcherEducation () {
    return this.hasMany('App/Models/UserResearcherEducation')
  }

  userResearcherExpertise () {
    return this.hasMany('App/Models/UserResearcherExpertise')
  }

  userResearcherResearch () {
    return this.hasMany('App/Models/UserResearcherResearch')
  }

  userInvestorIndividual () {
    return this.hasOne('App/Models/UserInvestorIndividual')
  }

  userInvestorCompany () {
    return this.hasOne('App/Models/UserInvestorCompany')
  }

  userBankAccount () {
    return this.hasOne('App/Models/UserBankAccount')
  }

  role () {
    return this.hasOne('App/Models/MdRole')
  }

  projects () {
    return this.hasMany('App/Models/Project','id','researcher_id')
  }

  products () {
    return this.hasMany('App/Models/Product','id','researcher_id')
  }

  investorIn () {
    return this.hasMany('App/Models/Project','id','investor_id')
  }

  forgotPasswords () {
    return this.hasMany('App/Models/ForgotPassword')
  }

  projectMembers () {
    return this.hasMany('App/Models/ProjectMember','id','researcher_id')
  }

  projectFundings () {        
    return this.hasMany('App/Models/ProjectFunding','id','investor_id')
  }

  // chats () {
  //   return null
  // }

  researcherPayments () {
    return this.manyThrough('App/Models/Project','payments','id','researcher_id')
  }

  investorPayments () {
    return this.manyThrough('App/Models/Project','payments','id','investor_id')
  }

  researcherFundings () {
    return this.manyThrough('App/Models/Project','projectFundings','id','researcher_id')
  }

  researchNeeds () {        
    return this.hasMany('App/Models/ResearchNeed','id','investor_id')
  }

  notifications () {
    return this.hasMany('App/Models/Notification')
  }

  productCollaborationOffers () {
    return this.hasMany('App/Models/ProductCollaborationOffer','id','investor_id')
  }

}

module.exports = User
