'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class ChatMap extends Model {
    static get connection () {
        return 'chat'
    }
    
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    room () {
        return this.belongsTo('App/Models/Room')
    }    

    chatContents () {
        return this.hasMany('App/Models/ChatContent')
    }


}

module.exports = ChatMap
