'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdDegree extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {                        
            instance.id = uuid()
        })
    }    

    static get incrementing () {
        return false
    }
}

module.exports = MdDegree
