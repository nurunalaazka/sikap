'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class ForgotPassword extends Model {
    static boot () {
        super.boot()
    
        /**
         * A hook to hash the user password before saving
         * it to the database.
         */
        this.addHook('beforeCreate', async (instance) => {          
            
            instance.id = uuid();                     
          
        })
    }

    user () {
        return this.belongsTo('App/Models/User')
    }
}

module.exports = ForgotPassword
