'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdStudyArea extends Model {
    static boot () {
        super.boot()
    
        /**
         * A hook to hash the user password before saving
         * it to the database.
         */
        this.addHook('beforeCreate', async (studyAreaInstance) => {          
            
            studyAreaInstance.id = uuid();                     
          
        })
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    projects () {        
        return this.hasMany('App/Models/Project','id','study_area_id')
    }

    products () {        
        return this.hasMany('App/Models/Product','id','study_area_id')
    }
}

module.exports = MdStudyArea
