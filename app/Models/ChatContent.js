'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class ChatContent extends Model {
    static get connection () {
        return 'chat'
    }
    
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }    

    room () {
        return this.belongsTo('App/Models/Room')
    }

    chatMember () {
        return this.belongsTo('App/Models/ChatMember','sender_id','id')
    }

    chatStatuses () {
        return this.hasMany('App/Models/ChatStatus')
    }

}

module.exports = ChatContent
