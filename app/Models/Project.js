'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class Project extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    researcher () {
        return this.belongsTo('App/Models/User','researcher_id','id')
    }
    
    investor () {
        return this.belongsTo('App/Models/User','investor_id','id')
    }

    projectMembers () {
        return this.hasMany('App/Models/ProjectMember','id','project_id')
    }

    projectMilestones () {
        return this.hasMany('App/Models/ProjectMilestone')
    }

    projectFundings () {
        return this.hasMany('App/Models/ProjectFunding')
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    studyArea () {
        return this.belongsTo('App/Models/MdStudyArea','study_area_id','id')
    }

    payments () {
        return this.hasMany('App/Models/Payment')
    }

    projectImages () {
        return this.hasMany('App/Models/ProjectImage')
    }

}

module.exports = Project
