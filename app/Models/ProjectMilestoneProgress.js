'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ProjectMilestoneProgress extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    projectMilestone () {
        return this.belongsTo('App/Models/ProjectMilestone')
    }

    projectMilestoneProgressFiles () {
        return this.hasMany('App/Models/ProjectMilestoneProgressFile')
    }
}

module.exports = ProjectMilestoneProgress
