'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class Room extends Model {
    static get connection () {
        return 'chat'
    }
    
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }  

    chatContents () {
        return this.hasMany('App/Models/ChatContent')
    }

    chatMembers () {
        return this.hasMany('App/Models/ChatMember')
    }

    chatStatuses () {
        return this.manyThrough('App/Models/ChatContent','chatStatuses','id','room_id')
    }
}

module.exports = Room
