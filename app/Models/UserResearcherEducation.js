'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class UserResearcherEducation extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userResearcher) => {                        
            userResearcher.id = uuid()
        })
    }    

    static get incrementing () {
        return false
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    degree () {
        return this.belongsTo('App/Models/MdDegree','degree_id','id')
    }
}

module.exports = UserResearcherEducation
