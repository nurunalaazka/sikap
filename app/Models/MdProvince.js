'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdProvince extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {                        
            instance.id = uuid()
        })
    }

    cities () {
        return this.hasMany('App/Models/MdCity','id','province_id')
    }
    districts () {
        return this.hasMany('App/Models/MdDistrict','id','province_id')
    }

    country () {
        return this.belongsTo('App/Models/MdCountry','country_id','id')
    }    

    static get incrementing () {
        return false
    }
}

module.exports = MdProvince
