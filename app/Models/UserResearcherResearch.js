'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class UserResearcherResearch extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userResearcher) => {                        
            userResearcher.id = uuid()
        })
    }    

    static get incrementing () {
        return false
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    studyArea () {
        return this.belongsTo('App/Models/MdStudyArea','study_area_id','id')
    }
    
}

module.exports = UserResearcherResearch
