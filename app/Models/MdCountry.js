'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class MdCountry extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {                        
            instance.id = uuid()
        })
    }

    cities () {
        return this.hasMany('App/Models/MdCity','id','country_id')
    }
    provinces () {
        return this.hasMany('App/Models/MdProvince','id','country_id')
    }
    districts () {
        return this.hasMany('App/Models/MdDistrict','id','country_id')
    }

    static get incrementing () {
        return false
    }
}

module.exports = MdCountry
