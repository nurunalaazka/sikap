'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class Product extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }

    researcher () {
        return this.belongsTo('App/Models/User','researcher_id','id')
    }
    
    investor () {
        return this.belongsTo('App/Models/User','investor_id','id')
    }

    productMembers () {
        return this.hasMany('App/Models/ProductMember','id','product_id')
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    studyArea () {
        return this.belongsTo('App/Models/MdStudyArea','study_area_id','id')
    }

    productImages () {
        return this.hasMany('App/Models/ProductImage')
    }

    productCollaborationOffers () {
        return this.hasMany('App/Models/ProductCollaborationOffer')
    }
}

module.exports = Product
