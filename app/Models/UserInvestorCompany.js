'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class UserInvestorCompany extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {                        
            instance.id = uuid()
        })
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    static get incrementing () {
        return false
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    studyArea () {
        return this.belongsTo('App/Models/MdStudyArea','study_area_id','id')
    }

    industry () {
        return this.belongsTo('App/Models/MdIndustry','industry_id','id')
    }

    industryCategory () {
        return this.belongsTo('App/Models/MdIndustryCategory','industry_category_id','id')
    }

    country () {
        return this.belongsTo('App/Models/MdCountry','country_id','id')
    }

    province () {
        return this.belongsTo('App/Models/MdProvince','province_id','id')
    }

    district () {
        return this.belongsTo('App/Models/MdDistrict','district_id','id')
    }

    city () {
        return this.belongsTo('App/Models/MdCity','city_id','id')
    }
}


module.exports = UserInvestorCompany
