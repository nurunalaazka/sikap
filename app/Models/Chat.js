'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class Chat extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }    

    projectFunding () {
        return this.hasOne('App/Models/ProjectFunding')
    }    

    productCollaborationOffer () {
        return this.hasOne('App/Models/ProductCollaborationOffer')
    }

}

module.exports = Chat
