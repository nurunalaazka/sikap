'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class UserResearcher extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userResearcher) => {                        
            userResearcher.id = uuid()
        })
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    static get incrementing () {
        return false
    }

    researchField () {
        return this.belongsTo('App/Models/MdResearchField','research_field_id','id')
    }

    studyArea () {
        return this.belongsTo('App/Models/MdStudyArea','study_area_id','id')
    }
}

module.exports = UserResearcher
