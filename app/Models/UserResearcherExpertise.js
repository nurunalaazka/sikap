'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class UserResearcherExpertise extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userResearcher) => {                        
            userResearcher.id = uuid()
        })
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    static get incrementing () {
        return false
    }
}

module.exports = UserResearcherExpertise
