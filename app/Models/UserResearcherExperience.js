'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class UserResearcherExperience extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (userResearcher) => {                        
            userResearcher.id = uuid()
        })
    }    

    static get incrementing () {
        return false
    }

    user () {
        return this.belongsTo('App/Models/User')
    }

    industry () {
        return this.belongsTo('App/Models/MdIndustry','industry_id','id')
    }

    industryCategory () {
        return this.belongsTo('App/Models/MdIndustryCategory','industry_category_id','id')
    }
}

module.exports = UserResearcherExperience
