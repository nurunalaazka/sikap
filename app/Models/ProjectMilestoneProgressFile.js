'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');


class ProjectMilestoneProgressFile extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })

        this.addHook('afterFetch', async (instance) => {
            
            instance = await calculate(instance)           
            
        })

        this.addHook('afterFind', async (instance) => {
            
            instance = await calculate(instance)
            
        })

        this.addHook('afterPaginate', async (instance) => {
            
            instance = await calculate(instance)
            
        })

        async function calculate(instance) {

            instance = instance.map(temp=>{
                let id = temp.project_milestone_progress_id

                if(temp.file) {
                    temp.filename = temp.file.split(`/${id}/`)[1]

                    temp.sizeInMB = (temp.size/1000000).toFixed(2)

                }
    
                let elapsed = (new Date() - new Date(temp.updated_at))
    
                let unit = 'seconds'                
    
                if(elapsed <= (60*1000)-1) {
                    //sec
                    elapsed = Math.floor(elapsed / 1000)
                    if(elapsed == 1) unit = 'second'
                }
                
                if((elapsed >= (60*1000)) && (elapsed <= (60*60*1000)-1)) {
                    //min
                    unit = 'minutes'
                    elapsed = Math.floor(elapsed / (60*1000))
                    if(elapsed == 1) unit = 'minute'
                }
    
                if((elapsed >= (60*60*1000)) && (elapsed <= (24*60*60*1000)-1)) {
                    //h
                    unit = 'hours'
                    elapsed = Math.floor(elapsed / (60*60*1000))
                    if(elapsed == 1) unit = 'hour'
                }
    
                if((elapsed >= (24*60*60*1000)) && (elapsed <= (7*24*60*60*1000)-1)) {
                    // d
                    unit = 'days'
                    elapsed = Math.floor(elapsed / (24*60*60*1000))
                    if(elapsed == 1) unit = 'day'
                }
    
                if((elapsed >= (7*24*60*60*1000)) && (elapsed <= (4*7*24*60*60*1000)-1)) {
                    //w
                    unit = 'weeks'
                    elapsed = Math.floor(elapsed / (7*24*60*60*1000))
                    if(elapsed == 1) unit = 'week'
                }
    
                if((elapsed >= (4*7*24*60*60*1000)) && (elapsed <= (12*4*7*24*60*60*1000)-1)) {
                    //m
                    unit = 'months'
                    elapsed = Math.floor(elapsed / (4*7*24*60*60*1000))
                    if(elapsed == 1) unit = 'month'
                }
    
                if(elapsed >= (12*4*7*24*60*60*1000)) {
                    // y
                    unit = 'years'
                    elapsed = Math.floor(elapsed / (12*4*7*24*60*60*1000))
                    if(elapsed == 1) unit = 'year'
                }
    
    
                temp.uploaded_time = elapsed
                temp.uploaded_time_unit = unit
                return temp
            })   
            
            return instance
        }
    }    

    static get incrementing () {
        return false
    }

    projectMilestoneProgress () {
        return this.belongsTo('App/Models/ProjectMilestoneProgress')
    }

    
}

module.exports = ProjectMilestoneProgressFile
