'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuid } = require('uuid');

class Payment extends Model {
    static boot () {
        super.boot()

        this.addHook('beforeCreate', async (instance) => {            
            if(!instance.id){
                instance.id = uuid()
            }
        })
    }    

    static get incrementing () {
        return false
    }
    
    project () {
        return this.belongsTo('App/Models/Project')
    }

    paymentHistories () {
        return this.hasMany('App/Models/PaymentHistory')
    }

    projectMilestone () {
        return this.belongsTo('App/Models/ProjectMilestone')
    }
}

module.exports = Payment
