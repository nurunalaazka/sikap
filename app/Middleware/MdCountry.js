'use strict'
const Country = use('App/Models/MdCountry')
const Encryption = use('Encryption')

class MdCountry {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const country = await Country.find(id)

    if(!country || !ref) return response.json({status:404, error: 'Country not found'})

    request.all().id = id
    request.all().country = country

    await next()
  }
}

module.exports = MdCountry
