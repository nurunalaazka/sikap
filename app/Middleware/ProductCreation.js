'use strict'
const Encryption = use('Encryption')
const ProductModel = use('App/Models/Product')

class ProductCreation {
  
  async handle ({ request, response, auth }, next) {
    // call next to advance the request
    const user = await auth.getUser()

    let { _draft } = request.all();

    const id = Encryption.decrypt(_draft)
    
    const product = await ProductModel.find(id)

    if(!product || !_draft) return response.json({status:404, error: 'Product not found'})

    if(product.researcher_id !== user.id) return response.json({status:403, error: 'Unauthorized Access'})
        
    request.product = product;
    await next()
  }
}

module.exports = ProductCreation
