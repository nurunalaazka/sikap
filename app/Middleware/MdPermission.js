'use strict'
const Permission = use('App/Models/MdPermission')
const Encryption = use('Encryption')

class MdPermission {  
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const permission = await Permission.find(id)

    if(!permission || !ref) return response.json({status:404, error: 'Permission not found'})

    request.all().id = id
    request.all().permission = permission

    await next()
  }
}

module.exports = MdPermission
