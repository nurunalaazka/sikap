'use strict'
const Bank = use('App/Models/MdBank')
const Encryption = use('Encryption')

class MdBank {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const bank = await Bank.find(id)

    if(!bank || !ref) return response.json({status:404, error: 'Bank not found'})

    request.all().id = id
    request.all().bank = bank

    await next()
  }
}

module.exports = MdBank
