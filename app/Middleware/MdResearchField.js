'use strict'
const ResearchField = use('App/Models/MdResearchField')
const Encryption = use('Encryption')

class MdResearchField {
  
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const researchField = await ResearchField.find(id)

    if(!researchField || !ref) return response.json({status:404, error: 'Role not found'})

    request.all().id = id
    request.all().researchField = researchField
    
    await next()
  }
}

module.exports = MdResearchField
