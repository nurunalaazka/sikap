'use strict'
const Role = use('App/Models/MdRole')
const Encryption = use('Encryption')

class MdRole { 
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const role = await Role.find(id)

    if(!role || !ref) return response.json({status:404, error: 'Role not found'})

    request.all().id = id
    request.all().role = role

    await next()
  }
}

module.exports = MdRole
