'use strict'
const RolePermission = use('App/Models/MdRolePermission')
const Encryption = use('Encryption')

class MdRoleRolePermission {  
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const rolePermission = await RolePermission.find(id)

    if(!rolePermission || !ref) return response.json({status:404, error: 'RolePermission not found'})

    request.all().id = id
    request.all().rolePermission = rolePermission

    await next()
  }
}

module.exports = MdRoleRolePermission
