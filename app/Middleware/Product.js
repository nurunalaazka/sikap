'use strict'
const Encryption = use('Encryption')
const ProductModel = use('App/Models/Product')

class Product {
  
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref)
    
    const product = await ProductModel.find(id)

    if(!product || !ref) return response.json({status:404, error: 'Product not found'})

    request.all().id = id;    
    request.product = product;

    await next()
  }
}

module.exports = Product
