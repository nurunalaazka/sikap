'use strict'
const ProjectMilestone = use('App/Models/ProjectMilestone')
const Encryption = use('Encryption')

class Milestone {
  
  async handle ({ request, response }, next) {
    // call next to advance the request
    const { milestone } = request.all()

    if(!milestone) return response.json({status:400, error: 'Bad request'})

    const id = Encryption.decrypt(milestone)

    const projectMilestone = await ProjectMilestone.find(id);

    if(!projectMilestone) return response.json({status:404, error: 'Project Milestone not found'})

    request.all().projectMilestone = projectMilestone
    request.all().id = id

    await next()
  }
}

module.exports = Milestone
