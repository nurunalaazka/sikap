'use strict'
const District = use('App/Models/MdDistrict')
const Encryption = use('Encryption')

class MdDistrict {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const district = await District.find(id)

    if(!district || !ref) return response.json({status:404, error: 'District not found'})

    request.all().id = id
    request.all().district = district

    await next()
  }
}

module.exports = MdDistrict
