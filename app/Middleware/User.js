'use strict'
const Encryption = use('Encryption')
const UserModel = use('App/Models/User')

class User {
  
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const user = await UserModel.find(id)

    if(!user || !ref) return response.json({status:404, error: 'User not found'})

    request.all().id = id
    request.all().user = user

    await next()
  }
}

module.exports = User
