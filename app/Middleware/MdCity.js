'use strict'
const City = use('App/Models/MdCity')
const Encryption = use('Encryption')

class MdCity {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const city = await City.find(id)

    if(!city || !ref) return response.json({status:404, error: 'City not found'})

    request.all().id = id
    request.all().city = city

    await next()
  }
}

module.exports = MdCity
