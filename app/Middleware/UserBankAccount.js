'use strict'
const ModelUserBankAccount = use('App/Models/UserBankAccount')
const Encryption = use('Encryption')

class UserBankAccount {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const userBankAccount = await ModelUserBankAccount.find(id)

    if(!userBankAccount || !ref) return response.json({status:404, error: 'UserBankAccount not found'})

    request.all().id = id
    request.all().userBankAccount = userBankAccount

    await next()
  }
}

module.exports = UserBankAccount
