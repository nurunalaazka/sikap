'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Services = new (use('App/Services/Services'))

class View {
  
  async handle ({ request, view, session }, next) {
    // call next to advance the request
    try{      
      let userLogin = JSON.parse(session.get('user'))
      let userData = JSON.parse(session.get('user_info'))

      let isLoggedIn = await Services.isLoggedIn(...arguments)      
      
      if(isLoggedIn && !userLogin) {

        const resaveUser =  await Services.saveUserToSession(...arguments)
        
        userLogin = resaveUser.user
        userData = resaveUser.userData        

    }            

      view.share({
        userLogin,
        userData
      })

    }catch(e) {
      throw e
    }   

    await next()
  }
}

module.exports = View
