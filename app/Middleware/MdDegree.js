'use strict'
const Degree = use('App/Models/MdDegree')
const Encryption = use('Encryption')

class MdDegree { 
  async handle ({ request, response }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const degree = await Degree.find(id)

    if(!degree || !ref) return response.json({status:404, error: 'Degree not found'})

    request.all().id = id
    request.all().degree = degree

    await next()
  }
}

module.exports = MdDegree
