'use strict'
const Industry = use('App/Models/MdIndustry')
const Encryption = use('Encryption')

class MdIndustry {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const industry = await Industry.find(id)

    if(!industry || !ref) return response.json({status:404, error: 'Industry not found'})

    request.all().id = id
    request.all().industry = industry

    await next()
  }
}

module.exports = MdIndustry
