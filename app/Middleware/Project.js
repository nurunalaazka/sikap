'use strict'
const Encryption = use('Encryption')
const ProjectModel = use('App/Models/Project')

class Project {
  
  async handle ({ request, response, params }, next) {
    let { ref } = request.all()      

    const id = Encryption.decrypt(ref)
    
    const project = await ProjectModel.find(id)

    if(!project || !ref) return response.json({status:404, error: 'Project not found'})

    request.all().id = id;
    request.all().ref = encodeURIComponent(ref);  
    request.project = project;
    await next()
  }
}

module.exports = Project
