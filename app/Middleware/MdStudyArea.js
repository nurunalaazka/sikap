'use strict'
const StudyArea = use('App/Models/MdStudyArea')
const Encryption = use('Encryption')

class MdStudyArea {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const studyArea = await StudyArea.find(id)

    if(!studyArea || !ref) return response.json({status:404, error: 'Role not found'})

    request.all().id = id
    request.all().studyArea = studyArea

    await next()
  }
}

module.exports = MdStudyArea
