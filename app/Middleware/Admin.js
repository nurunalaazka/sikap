'use strict'

const Encryption = use('Encryption')
const UserModel = use('App/Models/User')

class Admin {
  async handle ({ request, auth, session, response }, next) {
    // call next to advance the request    
    const user = await auth.getUser()

    if(user.role_id !== 'admin') {
      session.flash({ content: 'You do not have authorization to access admin page.', title:'403 Unauthorized', type:'danger' })
      return response.redirect('back')
    }

    await next()
  }
}

module.exports = Admin
