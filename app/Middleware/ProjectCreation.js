'use strict'
const Encryption = use('Encryption')
const Project = use('App/Models/Project')

class ProjectCreation {
  
  async handle ({ request, response, auth }, next) {
    const user = await auth.getUser();

    let { _draft } = request.all();    
    
    // if(_draft) _draft = _draft.replace(/\ /g, '+')

    const id = Encryption.decrypt(_draft);

    const project = await Project.find(id);

    if(!project || !_draft) return response.json({status:404, error: 'Project not found'})


    if(project.researcher_id !== user.id) return response.json({status:403, error: 'Unauthorized Access'})

    request.all().id = id
    request.all()._draft = encodeURIComponent(_draft)
    request.project = project

    await next()
  }
}

module.exports = ProjectCreation
