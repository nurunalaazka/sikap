'use strict'
const Province = use('App/Models/MdProvince')
const Encryption = use('Encryption')

class MdProvince {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const province = await Province.find(id)

    if(!province || !ref) return response.json({status:404, error: 'Province not found'})

    request.all().id = id
    request.all().province = province

    await next()
  }
}

module.exports = MdProvince
