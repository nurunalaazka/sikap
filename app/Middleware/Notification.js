'use strict'
const NotificationModel = use('App/Models/Notification');
const Services = new (use('App/Services/Services'))

class Notification {
  
  async handle ({ request, auth, view }, next) {
    try {
      const isLoggedIn = await Services.isLoggedIn(...arguments)

      let user;
      let unread = 0

      if(isLoggedIn) {
        user = await auth.getUser()          

        const notifications = (
          await NotificationModel.query()
          .where('user_id',user.id)
          .orderBy('created_at','desc')
          .fetch()
        ).toJSON()

        for (const item of notifications) {
          if(item.status === 'unread') unread++
        }
        
        // console.log(notifications)     

        view.share({
          notifications,
          unread
        })      
      }

    } catch (error) {
      throw error
    }
    
    await next()
    
  }
}

module.exports = Notification
