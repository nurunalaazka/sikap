'use strict'
const IndustryCategory = use('App/Models/MdIndustryCategory')
const Encryption = use('Encryption')

class MdIndustryCategory {
  
  async handle ({ request }, next) {
    // call next to advance the request
    let { ref } = request.all();

    const id = Encryption.decrypt(ref);

    const industryCategory = await IndustryCategory.find(id)

    if(!industryCategory || !ref) return response.json({status:404, error: 'IndustryCategory not found'})

    request.all().id = id
    request.all().industryCategory = industryCategory

    await next()
  }
}

module.exports = MdIndustryCategory
