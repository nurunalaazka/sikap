'use strict'

class Registration {
  get rules () {
    return {      
      'name':'required',
      'username':'required|unique:users',
      'email':'required|email|unique:users',
      'password':'required',      
    }
  }

  get messages(){
    return{
      'required':'{{field}} is required!',
      'unique':'{{field}} is already exists!',
      'email':'{{field}} is not a valid email address!'
    }
  }

  async fails(e){
    this.ctx.session.withErrors(e)
      .flashAll()

    return this.ctx.response.redirect('back')
  }
}

module.exports = Registration
