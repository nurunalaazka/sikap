'use strict'

class UserInformation {
  get rules () {    
    return {      
      'profile_pic':'file_ext:png,jpg|file_size:2mb|file_types:image',
      'id_card':'file_ext:png,jpg|file_size:2mb|file_types:image',

    }
  }

  get messages(){
    return{      
      // 'file':'{{field}} you are uploaded might be corrupt!',
      'file_ext':'{{field}} file must be jpg or png format!',
      'file_size':'{{field}} file size exceed 2MB!',
      'file_types':'{{field}} file must be image file!',      
    }
  }

  async fails(e){
    this.ctx.session.withErrors(e)
      .flashAll()

    return this.ctx.response.redirect('back')
  }
}

module.exports = UserInformation
