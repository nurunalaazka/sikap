'use strict'

class Login {
  get rules () {
    return {
      // validation rules      
      'email':'required|email',
      'password':'required',      
    }
  }

  get messages(){
    return{
      'required':'{{field}} is required!',
      'email.email':'You must provide a valid email address.'
    }
  }

  async fails(e){
    this.ctx.session.withErrors(e)
      .flashAll()

    return this.ctx.response.redirect('back')
  }
}

module.exports = Login
