<table class="table table-hover responsive">
    <thead>
        <tr>
            <th>no</th>
            @each(item in table.head)
            <th>{{item}}</th>
            @endeach
        </tr>                
    </thead>
    <tbody>
        @each(item in table.body)
        <tr>
            <td>{{($loop.index + 1)}}</td>
            @each(column in item.columns)
            <td>{{ column || '-' }}</td>
            @endeach
            @if(item.actions)
            <td class="dropdown">
                @each(action in item.actions)
                    @if(action.type=='modal' || action.type=='dropdown')
                    <a class="btn btn-sm btn-icon {{action.className}} float-right" data-toggle="{{action.type}}" data-target="{{action.link}}" id="{{action.id}}">
                    @else
                    <a class="btn btn-sm btn-icon {{action.className}} float-right" href="{{action.link}}" id="{{action.id}}">
                    @endif
                        <i class="material-icons-outlined">{{action.icon}}</i>
                    </a>
                    
                    @if(action.type=='dropdown')
                    <div class="dropdown-menu dropdown-menu-right dropdown-account" aria-labelledby="{{action.id}}">
                        <!--  <div class="dropdown-divider"></div>  -->
                        @each(item in action.dropdown)
                        @if(item.type=='modal')
                        <a class="dropdown-item {{item.className}}" data-toggle="modal" data-target="{{item.link}}" data-href="{{item.dataHref}}" data-title="{{item.dataTitle}}" data-type="{{item.dataType}}">
                        @else
                        <a class="dropdown-item {{item.className}}" href="{{item.link}}">
                        @endif
                            <i class="material-icons-outlined">{{item.icon}}</i>
                            {{item.label}}
                        </a>
                        @endeach
                        
                        
                    </div>
                    @endif
                @endeach
                
            </td>
            @endif
        </tr>
        @if(item.body)
        <tr>
            <td colspan="{{item.columns.length+2}}">
                <div class="row text-left">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <label>Basic Salary</label>
                            </div>
                            <div class="col-md-6 text-right">
                                Rp 6.000.000
                            </div>
                        </div>
                    </div>
                    
                </div>
            </td>
        </tr>
        @endif
        @endeach
        
    </tbody>
    @if(table.footer && table.footer.length)
    <tfoot>
        @each(item in table.footer)
            
            <td>
                @if(item)
                <h4 class="{{item.className}}">{{item.content}}</h4>
                @endif
            </td>
            
        @endeach
    </tfoot>
    @endif
</table>