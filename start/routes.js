'use strict'
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

const Route = use('Route')

Route.group(()=>{
    Route.on('home').render('sandbox.home')
    Route.on('explore').render('sandbox.explore')
    Route.on('project').render('sandbox.project')
    Route.on('project-milestones').render('sandbox.project-milestones')
    Route.on('project-progress').render('sandbox.project-progress')
    Route.on('project-team').render('sandbox.project-team')
    Route.get('product','SandboxController.product').middleware('auth')
    Route.on('product-team').render('sandbox.product-team')
    Route.on('dashboard').render('sandbox.dashboard')
    Route.on('user_profile').render('sandbox.user_profile')
    Route.on('success').render('sandbox.success')
    Route.on('login').render('sandbox.login')
    Route.on('register').render('sandbox.register')
    Route.on('project_form-general').render('sandbox.project_form-general')
    Route.on('project_form-summary').render('sandbox.project_form-summary')
    Route.on('project_form-milestones').render('sandbox.project_form-milestones')
    Route.on('project_form-team').render('sandbox.project_form-team')
    Route.on('project_form-finish').render('sandbox.project_form-finish')

    Route.get('product_form-general','SandboxController.productFormGeneral').middleware('auth')
    Route.get('product_form-summary','SandboxController.productFormSummary').middleware('auth')
    Route.get('product_form-team','SandboxController.productFormTeam').middleware('auth')
    Route.get('product_form-finish','SandboxController.productFormFinish').middleware('auth')


    Route.on('bank_account').render('sandbox.bank_account')
    Route.on('bank_form').render('sandbox.bank_form')
    Route.on('password_change').render('sandbox.password_change')
    Route.on('user_profile-researcher').render('sandbox.user_profile-researcher')
    Route.on('user').render('sandbox.user')
    Route.on('profile').render('sandbox.profile')
    Route.on('profile_form').render('sandbox.profile_form')
    Route.on('profile-education').render('sandbox.profile-education')
    Route.on('profile-expertise').render('sandbox.profile-expertise')
    Route.on('modal').render('sandbox.modal')
    Route.on('funding_offers').render('sandbox.funding_offers')
    Route.on('funding_offers-investor').render('sandbox.funding_offers-investor')
    Route.on('payments').render('sandbox.payments')
    Route.on('payment-pending').render('sandbox.payment-pending')
    Route.on('payment-unpaid').render('sandbox.payment-unpaid')
    Route.on('payment-paid').render('sandbox.payment-paid')
    Route.on('funding_offer').render('sandbox.funding_offer')
    Route.on('chat').render('sandbox.chat')
    Route.on('chat_detail').render('sandbox.chat_detail')
    Route.on('notification').render('sandbox.notification')
    Route.on('offer_funding').render('sandbox.offer_funding')
    Route.on('research_needs').render('sandbox.research_needs')

}).prefix('')

// Public
/*
Route.on('public-').render('view_public.')
*/
Route.on('/').render('view_public.index')
Route.on('/public-html').render('view_public.index')
Route.on('/public-cari-paket-html').render('view_public.cari-paket.index')
Route.on('/public-cari-paket-tender-html').render('view_public.cari-paket.tender')
Route.on('/public-cari-paket-non-tender-html').render('view_public.cari-paket.non-tender')
Route.on('/public-konten-khusus-html').render('view_public.konten-khusus.index')
Route.on('/public-konten-khusus-detail-html').render('view_public.konten-khusus.detail')
Route.on('/public-berita-html').render('view_public.berita.index')
Route.on('/public-berita-detail-html').render('view_public.berita.detail')
Route.on('/public-kontak-kami-html').render('view_public.kontak-kami')
Route.on('/public-tentang-kami-html').render('view_public.tentang-kami')
Route.on('/public-lupa-password-html').render('view_public.lupa-password')
Route.on('/public-login-penyedia-html').render('view_public.auth.login-penyedia')
Route.on('/public-login-penyedia-password-html').render('view_public.auth.login-penyedia-password')
Route.on('/public-login-non-penyedia-html').render('view_public.auth.login-non-penyedia')
Route.on('/public-login-non-penyedia-password-html').render('view_public.auth.login-non-penyedia-password')

Route.on('/public-login-ppe-html').render('view_public.auth.login-ppe')
Route.on('/general-enable-totp-html').render('view_general.enable-totp')
Route.on('/general-enable-totp-enabled-html').render('view_general.enable-totp-enabled')
Route.on('/public-reset-totp-html').render('view_public.auth.reset-totp')
// Public
// Admin Agency
/*
Route.on('agency-').render('view_admin_agency.')
*/
// Admin PPE
/*
Route.on('ppe-').render('view_admin_ppe.')
Route.on('ppe--html').render('view_admin_ppe..index')

*/
Route.on('ppe-html').render('view_admin_ppe.index')
Route.on('ppe-berita-html').render('view_admin_ppe.berita.index')
Route.on('ppe-header-website-html').render('view_admin_ppe.header-website.index')
Route.on('ppe-konfigurasi-jaim-html').render('view_admin_ppe.konfigurasi-jaim.index')
Route.on('ppe-konfigurasi-spse-html').render('view_admin_ppe.konfigurasi-spse.index')
Route.on('ppe-konten-khusus-html').render('view_admin_ppe.konten-khusus.index')
Route.on('ppe-konten-multimedia-html').render('view_admin_ppe.konten-multimedia.index')
Route.on('ppe-konten-multimedia-banner-html').render('view_admin_ppe.konten-multimedia.banner')
Route.on('ppe-nama-lpse-html').render('view_admin_ppe.nama-lpse.index')
Route.on('ppe-pengumuman-html').render('view_admin_ppe.pengumuman.index')
Route.on('ppe-perubahan-jadwal-html').render('view_admin_ppe.perubahan-jadwal.index')
Route.on('ppe-pesan-berjalan-html').render('view_admin_ppe.pesan-berjalan.index')
Route.on('ppe-pesan-sistem-html').render('view_admin_ppe.pesan-sistem.index')
Route.on('ppe-reaktivasi-non-tender-html').render('view_admin_ppe.reaktivasi-non-tender.index')
Route.on('ppe-reaktivasi-tender-html').render('view_admin_ppe.reaktivasi-tender.index')
Route.on('ppe-sesi-pelatihan-html').render('view_admin_ppe.sesi-pelatihan.index')
Route.on('ppe-summary-report-non-tender-html').render('view_admin_ppe.summary-report-non-tender.index')
Route.on('ppe-summary-report-tender-html').render('view_admin_ppe.summary-report-tender.index')
Route.on('ppe-user-aktif-html').render('view_admin_ppe.user-aktif.index')
Route.on('ppe-log-akses-html').render('view_admin_ppe.log-akses.index')

Route.on('ppe-form-html').render('view_admin_ppe.form')
Route.on('ppe-berita-form-html').render('view_admin_ppe.berita.form')
Route.on('ppe-header-website-form-html').render('view_admin_ppe.header-website.form')
Route.on('ppe-konfigurasi-jaim-form-html').render('view_admin_ppe.konfigurasi-jaim.form')
Route.on('ppe-konfigurasi-spse-form-html').render('view_admin_ppe.konfigurasi-spse.form')
Route.on('ppe-konten-khusus-form-html').render('view_admin_ppe.konten-khusus.form')
Route.on('ppe-konten-multimedia-form-html').render('view_admin_ppe.konten-multimedia.form')
Route.on('ppe-konten-multimedia-banner-form-html').render('view_admin_ppe.konten-multimedia.banner-form')

Route.on('ppe-nama-lpse-form-html').render('view_admin_ppe.nama-lpse.form')
Route.on('ppe-pengumuman-form-html').render('view_admin_ppe.pengumuman.form')
Route.on('ppe-perubahan-jadwal-form-html').render('view_admin_ppe.perubahan-jadwal.form')
Route.on('ppe-pesan-berjalan-form-html').render('view_admin_ppe.pesan-berjalan.form')
Route.on('ppe-pesan-sistem-form-html').render('view_admin_ppe.pesan-sistem.form')
Route.on('ppe-reaktivasi-non-tender-form-html').render('view_admin_ppe.reaktivasi-non-tender.form')
Route.on('ppe-reaktivasi-tender-form-html').render('view_admin_ppe.reaktivasi-tender.form')
Route.on('ppe-sesi-pelatihan-form-html').render('view_admin_ppe.sesi-pelatihan.form')
Route.on('ppe-summary-report-non-tender-form-html').render('view_admin_ppe.summary-report-non-tender.form')
Route.on('ppe-summary-report-tender-form-html').render('view_admin_ppe.summary-report-tender.form')
Route.on('ppe-user-aktif-form-html').render('view_admin_ppe.user-aktif.form')
Route.on('ppe-log-akses-form-html').render('view_admin_ppe.log-akses.form')

Route.on('ppe-detail-html').render('view_admin_ppe.detail')
Route.on('ppe-berita-detail-html').render('view_admin_ppe.berita.detail')
Route.on('ppe-header-website-detail-html').render('view_admin_ppe.header-website.detail')
Route.on('ppe-konfigurasi-jaim-detail-html').render('view_admin_ppe.konfigurasi-jaim.detail')
Route.on('ppe-konfigurasi-spse-detail-html').render('view_admin_ppe.konfigurasi-spse.detail')
Route.on('ppe-konten-khusus-detail-html').render('view_admin_ppe.konten-khusus.detail')
Route.on('ppe-konten-multimedia-detail-html').render('view_admin_ppe.konten-multimedia.detail')
Route.on('ppe-nama-lpse-detail-html').render('view_admin_ppe.nama-lpse.detail')
Route.on('ppe-pengumuman-detail-html').render('view_admin_ppe.pengumuman.detail')
Route.on('ppe-perubahan-jadwal-detail-html').render('view_admin_ppe.perubahan-jadwal.detail')
Route.on('ppe-pesan-berjalan-detail-html').render('view_admin_ppe.pesan-berjalan.detail')
Route.on('ppe-pesan-sistem-detail-html').render('view_admin_ppe.pesan-sistem.detail')
Route.on('ppe-reaktivasi-non-tender-detail-html').render('view_admin_ppe.reaktivasi-non-tender.detail')
Route.on('ppe-reaktivasi-tender-detail-html').render('view_admin_ppe.reaktivasi-tender.detail')
Route.on('ppe-sesi-pelatihan-detail-html').render('view_admin_ppe.sesi-pelatihan.detail')
Route.on('ppe-summary-report-non-tender-detail-html').render('view_admin_ppe.summary-report-non-tender.detail')
Route.on('ppe-summary-report-tender-detail-html').render('view_admin_ppe.summary-report-tender.detail')
Route.on('ppe-user-aktif-detail-html').render('view_admin_ppe.user-aktif.detail')
Route.on('ppe-log-akses-detail-html').render('view_admin_ppe.log-akses.detail')

Route.on('ppe-agency-html').render('view_admin_ppe.agency.index')
Route.on('ppe-agency-form-html').render('view_admin_ppe.agency.form')
Route.on('ppe-agency-detail-html').render('view_admin_ppe.agency.detail')

Route.on('ppe-pegawai-html').render('view_admin_ppe.pegawai.index')
Route.on('ppe-pegawai-form-html').render('view_admin_ppe.pegawai.form')
Route.on('ppe-pegawai-detail-html').render('view_admin_ppe.pegawai.detail')

Route.on('ppe-auditor-html').render('view_admin_ppe.auditor.index')
Route.on('ppe-auditor-form-html').render('view_admin_ppe.auditor.form')
Route.on('ppe-auditor-detail-html').render('view_admin_ppe.auditor.detail')

Route.on('ppe-utility-html').render('view_admin_ppe.utility.index')
Route.on('ppe-utility-form-html').render('view_admin_ppe.utility.form')
Route.on('ppe-utility-detail-html').render('view_admin_ppe.utility.detail')

// Helpdesk SIKaP
/*
Route.on('helpdesk-').render('view_helpdesk.')
*/
// Penyedia
/*
Route.on('penyedia-').render('view_penyedia.')
*/
Route.on('penyedia-html').render('view_penyedia.index')

Route.on('penyedia-inbox-html').render('view_penyedia.inbox.index')
Route.on('penyedia-inbox-detail-html').render('view_penyedia.inbox.detail')

Route.on('penyedia-data-html').render('view_penyedia.data.identitas')
Route.on('penyedia-data-identitas-html').render('view_penyedia.data.identitas')
Route.on('penyedia-data-izin-usaha-html').render('view_penyedia.data.izin-usaha')
Route.on('penyedia-data-akta-html').render('view_penyedia.data.akta')
Route.on('penyedia-data-pemilik-html').render('view_penyedia.data.pemilik')
Route.on('penyedia-data-pengurus-html').render('view_penyedia.data.pengurus')
Route.on('penyedia-data-tenaga-ahli-html').render('view_penyedia.data.tenaga-ahli')
Route.on('penyedia-data-peralatan-html').render('view_penyedia.data.peralatan')
Route.on('penyedia-data-pengalaman-html').render('view_penyedia.data.pengalaman')
Route.on('penyedia-data-pajak-html').render('view_penyedia.data.pajak')
Route.on('penyedia-data-integrasi-html').render('view_penyedia.data.integrasi')

Route.on('penyedia-data-identitas-form-html').render('view_penyedia.data.identitas-form')
Route.on('penyedia-data-izin-usaha-form-html').render('view_penyedia.data.izin-usaha-form')
Route.on('penyedia-data-akta-form-html').render('view_penyedia.data.akta-form')
Route.on('penyedia-data-pemilik-form-html').render('view_penyedia.data.pemilik-form')
Route.on('penyedia-data-pengurus-form-html').render('view_penyedia.data.pengurus-form')
Route.on('penyedia-data-tenaga-ahli-form-html').render('view_penyedia.data.tenaga-ahli-form')
Route.on('penyedia-data-peralatan-form-html').render('view_penyedia.data.peralatan-form')
Route.on('penyedia-data-pengalaman-form-html').render('view_penyedia.data.pengalaman-form')
Route.on('penyedia-data-pajak-form-html').render('view_penyedia.data.pajak-form')
Route.on('penyedia-data-integrasi-form-html').render('view_penyedia.data.integrasi-form')

Route.on('penyedia-daftar-paket-html').render('view_penyedia.daftar-paket')
Route.on('penyedia-berita-html').render('view_penyedia.berita.index')
Route.on('penyedia-berita-form-html').render('view_penyedia.berita.form')

Route.on('penyedia-tender-detail-html').render('view_penyedia.tender.detail')
Route.on('penyedia-tender-detail-filled-html').render('view_penyedia.tender.detail-filled')
Route.on('penyedia-tender-detail-uploaded-html').render('view_penyedia.tender.detail-uploaded')

Route.on('penyedia-tender-detail-pertanyaan-html').render('view_penyedia.tender.detail-pertanyaan')
Route.on('penyedia-tender-detail-pertanyaan-edit-html').render('view_penyedia.tender.detail-pertanyaan-edit')
Route.on('penyedia-tender-detail-pertanyaan-pembukaan-html').render('view_penyedia.tender.detail-pertanyaan-pembukaan')
Route.on('penyedia-tender-detail-pertanyaan-jawab-html').render('view_penyedia.tender.detail-pertanyaan-jawab')
Route.on('penyedia-tender-detail-pertanyaan-jawaban-html').render('view_penyedia.tender.detail-pertanyaan-jawaban')



Route.on('penyedia-tender-detail-penawaran-html').render('view_penyedia.tender.detail-penawaran.index')
Route.on('penyedia-tender-detail-penawaran-kualifikasi-html').render('view_penyedia.tender.detail-penawaran.kualifikasi')
Route.on('penyedia-tender-detail-penawaran-administrasi-html').render('view_penyedia.tender.detail-penawaran.administrasi')
Route.on('penyedia-tender-detail-penawaran-harga-html').render('view_penyedia.tender.detail-penawaran.harga')



Route.on('penyedia-tender-detail-reverse-auction-html').render('view_penyedia.tender.detail-reverse-auction.index')
Route.on('penyedia-tender-detail-reverse-auction-scheduled-html').render('view_penyedia.tender.detail-reverse-auction.scheduled')
Route.on('penyedia-tender-detail-reverse-auction-done-html').render('view_penyedia.tender.detail-reverse-auction.done')
Route.on('penyedia-tender-detail-reverse-auction-ubah-jadwal-html').render('view_penyedia.tender.detail-reverse-auction.ubah-jadwal')
Route.on('penyedia-tender-detail-reverse-auction-rincian-penawaran-html').render('view_penyedia.tender.detail-reverse-auction.rincian-penawaran')



Route.on('penyedia-tender-detail-evaluasi-html').render('view_penyedia.tender.detail-evaluasi.index')
// Route.on('penyedia-tender-detail-evaluasi--html').render('view_penyedia.tender.detail-evaluasi.')
Route.on('penyedia-tender-detail-evaluasi-administrasi-html').render('view_penyedia.tender.detail-evaluasi.administrasi')
Route.on('penyedia-tender-detail-evaluasi-kualifikasi-html').render('view_penyedia.tender.detail-evaluasi.kualifikasi')
Route.on('penyedia-tender-detail-evaluasi-pesan-html').render('view_penyedia.tender.detail-evaluasi.pesan')
Route.on('penyedia-tender-detail-evaluasi-pesan-pembuktian-html').render('view_penyedia.tender.detail-evaluasi.pesan-pembuktian')
Route.on('penyedia-tender-detail-evaluasi-teknis-html').render('view_penyedia.tender.detail-evaluasi.teknis')
Route.on('penyedia-tender-detail-evaluasi-harga-html').render('view_penyedia.tender.detail-evaluasi.harga')
Route.on('penyedia-tender-detail-evaluasi-pembuktian-html').render('view_penyedia.tender.detail-evaluasi.pembuktian')
Route.on('penyedia-tender-detail-evaluasi-konfirmasi-html').render('view_penyedia.tender.detail-evaluasi.konfirmasi')
Route.on('penyedia-tender-detail-evaluasi-konfirmasi-failed-html').render('view_penyedia.tender.detail-evaluasi.konfirmasi-failed')
Route.on('penyedia-tender-detail-evaluasi-checked-html').render('view_penyedia.tender.detail-evaluasi.checked')
Route.on('penyedia-tender-detail-evaluasi-jadwal-html').render('view_penyedia.tender.detail-evaluasi.jadwal')
Route.on('penyedia-tender-detail-evaluasi-jadwal-filled-html').render('view_penyedia.tender.detail-evaluasi.jadwal-filled')
Route.on('penyedia-tender-detail-evaluasi-nego-html').render('view_penyedia.tender.detail-evaluasi.nego')
Route.on('penyedia-tender-detail-evaluasi-nego-filled-html').render('view_penyedia.tender.detail-evaluasi.nego-filled')
Route.on('penyedia-tender-detail-evaluasi-input-nego-html').render('view_penyedia.tender.detail-evaluasi.input-nego')
Route.on('penyedia-tender-detail-evaluasi-penetapan-pemenang-html').render('view_penyedia.tender.detail-evaluasi.penetapan-pemenang')
Route.on('penyedia-tender-detail-evaluasi-penetapan-pemenang-cadangan-html').render('view_penyedia.tender.detail-evaluasi.penetapan-pemenang-cadangan')


Route.on('penyedia-tender-detail-sanggahan-html').render('view_penyedia.tender.detail-sanggahan.index')
Route.on('penyedia-tender-detail-sanggahan-ubah-jadwal-html').render('view_penyedia.tender.detail-sanggahan.ubah-jadwal')
Route.on('penyedia-tender-detail-sanggahan-jawab-html').render('view_penyedia.tender.detail-sanggahan.jawab')
Route.on('penyedia-tender-detail-sanggahan-jawaban-html').render('view_penyedia.tender.detail-sanggahan.jawaban')

Route.on('penyedia-tender-pilih-ppk-html').render('view_penyedia.tender.pilih-ppk')
Route.on('penyedia-tender-pilih-ppk-alasan-html').render('view_penyedia.tender.pilih-ppk-alasan')

Route.on('penyedia-tender-detail-kualifikasi-dukungan-bank-html').render('view_penyedia.tender.detail-kualifikasi.dukungan-bank')
Route.on('penyedia-tender-detail-kualifikasi-izin-usaha-html').render('view_penyedia.tender.detail-kualifikasi.izin-usaha')
Route.on('penyedia-tender-detail-kualifikasi-akta-html').render('view_penyedia.tender.detail-kualifikasi.akta')
Route.on('penyedia-tender-detail-kualifikasi-pekerjaan-berjalan-html').render('view_penyedia.tender.detail-kualifikasi.pekerjaan-berjalan')
Route.on('penyedia-tender-detail-kualifikasi-persyaratan-lain-html').render('view_penyedia.tender.detail-kualifikasi.persyaratan-lain')
Route.on('penyedia-tender-detail-kualifikasi-tenaga-ahli-html').render('view_penyedia.tender.detail-kualifikasi.tenaga-ahli')
Route.on('penyedia-tender-detail-kualifikasi-peralatan-html').render('view_penyedia.tender.detail-kualifikasi.peralatan')
Route.on('penyedia-tender-detail-kualifikasi-pengalaman-html').render('view_penyedia.tender.detail-kualifikasi.pengalaman')
Route.on('penyedia-tender-detail-kualifikasi-pajak-html').render('view_penyedia.tender.detail-kualifikasi.pajak')
Route.on('penyedia-tender-detail-kualifikasi-integrasi-html').render('view_penyedia.tender.detail-kualifikasi.integrasi')
/*
Route.on('penyedia-tender-edit-').render('view_penyedia.tender.edit.')
*/
Route.on('penyedia-tender-edit-dokumen-pemilihan-html').render('view_penyedia.tender.edit.dokumen-pemilihan')
Route.on('penyedia-tender-edit-dokumen-penawaran-html').render('view_penyedia.tender.edit.dokumen-penawaran')
Route.on('penyedia-tender-edit-form-filled-html').render('view_penyedia.tender.edit.form-filled')
Route.on('penyedia-tender-edit-form-uploaded-html').render('view_penyedia.tender.edit.form-uploaded')
Route.on('penyedia-tender-edit-form-persetujuan-html').render('view_penyedia.tender.edit.form-persetujuan')
Route.on('penyedia-tender-edit-form-html').render('view_penyedia.tender.edit.form')
Route.on('penyedia-tender-edit-jadwal-html').render('view_penyedia.tender.edit.jadwal')
Route.on('penyedia-tender-edit-jadwal-filled-html').render('view_penyedia.tender.edit.jadwal-filled')
Route.on('penyedia-tender-edit-jadwal-disabled-html').render('view_penyedia.tender.edit.jadwal-disabled')
Route.on('penyedia-tender-edit-konfirmasi-pembatalan-dokumen-html').render('view_penyedia.tender.edit.konfirmasi-pembatalan-dokumen')
Route.on('penyedia-tender-edit-konfirmasi-pembatalan-persetujuan-html').render('view_penyedia.tender.edit.konfirmasi-pembatalan-persetujuan')
Route.on('penyedia-tender-edit-masa-berlaku-penawaran-html').render('view_penyedia.tender.edit.masa-berlaku-penawaran')
Route.on('penyedia-tender-edit-metode-pengadaan-html').render('view_penyedia.tender.edit.metode-pengadaan')
Route.on('penyedia-tender-edit-persyaratan-kualifikasi-html').render('view_penyedia.tender.edit.persyaratan-kualifikasi')
Route.on('penyedia-tender-edit-rincian-hps-html').render('view_penyedia.tender.edit.rincian-hps')
Route.on('penyedia-tender-edit-upload-dokumen-pemilihan-html').render('view_penyedia.tender.edit.upload-dokumen-pemilihan')
Route.on('penyedia-tender-edit-upload-informasi-lain-html').render('view_penyedia.tender.edit.upload-informasi-lain')
Route.on('penyedia-tender-edit-upload-kak-html').render('view_penyedia.tender.edit.upload-kak')
Route.on('penyedia-tender-edit-upload-rancangan-kontrak-html').render('view_penyedia.tender.edit.upload-rancangan-kontrak')
Route.on('penyedia-tender-edit-form-setuju-html').render('view_penyedia.tender.edit.form-setuju')
/*
Route.on('penyedia-tender-adendum-').render('view_penyedia.tender.adendum.')
*/
Route.on('penyedia-tender-adendum-dokumen-pemilihan-html').render('view_penyedia.tender.adendum.dokumen-pemilihan')
Route.on('penyedia-tender-adendum-dokumen-penawaran-html').render('view_penyedia.tender.adendum.dokumen-penawaran')
Route.on('penyedia-tender-adendum-form-filled-html').render('view_penyedia.tender.adendum.form-filled')
Route.on('penyedia-tender-adendum-form-uploaded-html').render('view_penyedia.tender.adendum.form-uploaded')
Route.on('penyedia-tender-adendum-form-persetujuan-html').render('view_penyedia.tender.adendum.form-persetujuan')
Route.on('penyedia-tender-adendum-form-html').render('view_penyedia.tender.adendum.form')
Route.on('penyedia-tender-adendum-jadwal-html').render('view_penyedia.tender.adendum.jadwal')
Route.on('penyedia-tender-adendum-jadwal-filled-html').render('view_penyedia.tender.adendum.jadwal-filled')
Route.on('penyedia-tender-adendum-jadwal-disabled-html').render('view_penyedia.tender.adendum.jadwal-disabled')
Route.on('penyedia-tender-adendum-konfirmasi-pembatalan-dokumen-html').render('view_penyedia.tender.adendum.konfirmasi-pembatalan-dokumen')
Route.on('penyedia-tender-adendum-konfirmasi-pembatalan-persetujuan-html').render('view_penyedia.tender.adendum.konfirmasi-pembatalan-persetujuan')
Route.on('penyedia-tender-adendum-masa-berlaku-penawaran-html').render('view_penyedia.tender.adendum.masa-berlaku-penawaran')
Route.on('penyedia-tender-adendum-metode-pengadaan-html').render('view_penyedia.tender.adendum.metode-pengadaan')
Route.on('penyedia-tender-adendum-persyaratan-kualifikasi-html').render('view_penyedia.tender.adendum.persyaratan-kualifikasi')
Route.on('penyedia-tender-adendum-rincian-hps-html').render('view_penyedia.tender.adendum.rincian-hps')
Route.on('penyedia-tender-adendum-upload-dokumen-pemilihan-html').render('view_penyedia.tender.adendum.upload-dokumen-pemilihan')
Route.on('penyedia-tender-adendum-upload-informasi-lain-html').render('view_penyedia.tender.adendum.upload-informasi-lain')
Route.on('penyedia-tender-adendum-upload-kak-html').render('view_penyedia.tender.adendum.upload-kak')
Route.on('penyedia-tender-adendum-upload-rancangan-kontrak-html').render('view_penyedia.tender.adendum.upload-rancangan-kontrak')
Route.on('penyedia-tender-adendum-form-setuju-html').render('view_penyedia.tender.adendum.form-setuju')

Route.on('penyedia-tender-detail-upload-berita-acara-html').render('view_penyedia.tender.upload-berita-acara')
Route.on('penyedia-tender-detail-cetak-berita-acara-html').render('view_penyedia.tender.cetak-berita-acara')
Route.on('penyedia-tender-detail-upload-berita-acara-lain-html').render('view_penyedia.tender.upload-berita-acara-lain')



Route.on('penyedia-tender-detail-pengumuman-kirim-html').render('view_penyedia.tender.detail-pengumuman.kirim')
Route.on('penyedia-tender-detail-pengumuman-sent-html').render('view_penyedia.tender.detail-pengumuman.sent')

Route.on('penyedia-tender-detail-pengumuman-html').render('view_penyedia.tender.detail-pengumuman.index')
Route.on('penyedia-tender-detail-pengumuman-tender-gagal-html').render('view_penyedia.tender.detail-pengumuman.tender-gagal')
Route.on('penyedia-tender-detail-pengumuman-konfirmasi-tender-gagal-html').render('view_penyedia.tender.detail-pengumuman.konfirmasi-tender-gagal')
Route.on('penyedia-tender-detail-pengumuman-konfirmasi-tender-ulang-html').render('view_penyedia.tender.detail-pengumuman.konfirmasi-tender-ulang')
Route.on('penyedia-tender-detail-pengumuman-konfirmasi-evaluasi-ulang-html').render('view_penyedia.tender.detail-pengumuman.konfirmasi-evaluasi-ulang')
Route.on('penyedia-tender-detail-pengumuman-konfirmasi-tender-batal-html').render('view_penyedia.tender.detail-pengumuman.konfirmasi-tender-batal')

Route.on('penyedia-tender-detail-pengumuman-tender-ulang-html').render('view_penyedia.tender.detail-pengumuman.tender-ulang')
Route.on('penyedia-tender-detail-pengumuman-tender-batal-html').render('view_penyedia.tender.detail-pengumuman.tender-batal')

Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.index')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-tender-gagal-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.tender-gagal')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-gagal-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-gagal')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-ulang-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-ulang')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-konfirmasi-evaluasi-ulang-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.konfirmasi-evaluasi-ulang')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-batal-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-batal')

Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-tender-ulang-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.tender-ulang')
Route.on('penyedia-tender-detail-pengumuman-pascakualifikasi-tender-batal-html').render('view_penyedia.tender.detail-pengumuman-pascakualifikasi.tender-batal')

// Pokja
/*
Route.on('pokja-').render('view_pokja.')
*/
Route.on('pokja-html').render('view_pokja.index')
Route.on('pokja-daftar-paket-html').render('view_pokja.daftar-paket')
Route.on('pokja-berita-html').render('view_pokja.berita.index')
Route.on('pokja-berita-form-html').render('view_pokja.berita.form')

Route.on('pokja-tender-detail-html').render('view_pokja.tender.detail')
Route.on('pokja-tender-detail-filled-html').render('view_pokja.tender.detail-filled')
Route.on('pokja-tender-detail-uploaded-html').render('view_pokja.tender.detail-uploaded')

Route.on('pokja-tender-detail-pertanyaan-html').render('view_pokja.tender.detail-pertanyaan')
Route.on('pokja-tender-detail-pertanyaan-edit-html').render('view_pokja.tender.detail-pertanyaan-edit')
Route.on('pokja-tender-detail-pertanyaan-pembukaan-html').render('view_pokja.tender.detail-pertanyaan-pembukaan')
Route.on('pokja-tender-detail-pertanyaan-jawab-html').render('view_pokja.tender.detail-pertanyaan-jawab')
Route.on('pokja-tender-detail-pertanyaan-jawaban-html').render('view_pokja.tender.detail-pertanyaan-jawaban')



Route.on('pokja-tender-detail-penawaran-html').render('view_pokja.tender.detail-penawaran.index')
Route.on('pokja-tender-detail-penawaran-kualifikasi-html').render('view_pokja.tender.detail-penawaran.kualifikasi')
Route.on('pokja-tender-detail-penawaran-administrasi-html').render('view_pokja.tender.detail-penawaran.administrasi')
Route.on('pokja-tender-detail-penawaran-harga-html').render('view_pokja.tender.detail-penawaran.harga')



Route.on('pokja-tender-detail-reverse-auction-html').render('view_pokja.tender.detail-reverse-auction.index')
Route.on('pokja-tender-detail-reverse-auction-scheduled-html').render('view_pokja.tender.detail-reverse-auction.scheduled')
Route.on('pokja-tender-detail-reverse-auction-done-html').render('view_pokja.tender.detail-reverse-auction.done')
Route.on('pokja-tender-detail-reverse-auction-ubah-jadwal-html').render('view_pokja.tender.detail-reverse-auction.ubah-jadwal')
Route.on('pokja-tender-detail-reverse-auction-rincian-penawaran-html').render('view_pokja.tender.detail-reverse-auction.rincian-penawaran')



Route.on('pokja-tender-detail-evaluasi-html').render('view_pokja.tender.detail-evaluasi.index')
// Route.on('pokja-tender-detail-evaluasi--html').render('view_pokja.tender.detail-evaluasi.')
Route.on('pokja-tender-detail-evaluasi-administrasi-html').render('view_pokja.tender.detail-evaluasi.administrasi')
Route.on('pokja-tender-detail-evaluasi-kualifikasi-html').render('view_pokja.tender.detail-evaluasi.kualifikasi')
Route.on('pokja-tender-detail-evaluasi-pesan-html').render('view_pokja.tender.detail-evaluasi.pesan')
Route.on('pokja-tender-detail-evaluasi-pesan-pembuktian-html').render('view_pokja.tender.detail-evaluasi.pesan-pembuktian')
Route.on('pokja-tender-detail-evaluasi-teknis-html').render('view_pokja.tender.detail-evaluasi.teknis')
Route.on('pokja-tender-detail-evaluasi-harga-html').render('view_pokja.tender.detail-evaluasi.harga')
Route.on('pokja-tender-detail-evaluasi-pembuktian-html').render('view_pokja.tender.detail-evaluasi.pembuktian')
Route.on('pokja-tender-detail-evaluasi-konfirmasi-html').render('view_pokja.tender.detail-evaluasi.konfirmasi')
Route.on('pokja-tender-detail-evaluasi-konfirmasi-failed-html').render('view_pokja.tender.detail-evaluasi.konfirmasi-failed')
Route.on('pokja-tender-detail-evaluasi-checked-html').render('view_pokja.tender.detail-evaluasi.checked')
Route.on('pokja-tender-detail-evaluasi-jadwal-html').render('view_pokja.tender.detail-evaluasi.jadwal')
Route.on('pokja-tender-detail-evaluasi-jadwal-filled-html').render('view_pokja.tender.detail-evaluasi.jadwal-filled')
Route.on('pokja-tender-detail-evaluasi-nego-html').render('view_pokja.tender.detail-evaluasi.nego')
Route.on('pokja-tender-detail-evaluasi-nego-filled-html').render('view_pokja.tender.detail-evaluasi.nego-filled')
Route.on('pokja-tender-detail-evaluasi-input-nego-html').render('view_pokja.tender.detail-evaluasi.input-nego')
Route.on('pokja-tender-detail-evaluasi-penetapan-pemenang-html').render('view_pokja.tender.detail-evaluasi.penetapan-pemenang')
Route.on('pokja-tender-detail-evaluasi-pemenang-html').render('view_pokja.tender.detail-evaluasi.pemenang')


Route.on('pokja-tender-detail-sanggahan-html').render('view_pokja.tender.detail-sanggahan.index')
Route.on('pokja-tender-detail-sanggahan-ubah-jadwal-html').render('view_pokja.tender.detail-sanggahan.ubah-jadwal')
Route.on('pokja-tender-detail-sanggahan-jawab-html').render('view_pokja.tender.detail-sanggahan.jawab')
Route.on('pokja-tender-detail-sanggahan-jawaban-html').render('view_pokja.tender.detail-sanggahan.jawaban')

Route.on('pokja-tender-pilih-ppk-html').render('view_pokja.tender.pilih-ppk')
Route.on('pokja-tender-pilih-ppk-alasan-html').render('view_pokja.tender.pilih-ppk-alasan')

/*
Route.on('pokja-tender-edit-').render('view_pokja.tender.edit.')
*/
Route.on('pokja-tender-edit-dokumen-pemilihan-html').render('view_pokja.tender.edit.dokumen-pemilihan')
Route.on('pokja-tender-edit-dokumen-penawaran-html').render('view_pokja.tender.edit.dokumen-penawaran')
Route.on('pokja-tender-edit-form-filled-html').render('view_pokja.tender.edit.form-filled')
Route.on('pokja-tender-edit-form-uploaded-html').render('view_pokja.tender.edit.form-uploaded')
Route.on('pokja-tender-edit-form-persetujuan-html').render('view_pokja.tender.edit.form-persetujuan')
Route.on('pokja-tender-edit-form-html').render('view_pokja.tender.edit.form')
Route.on('pokja-tender-edit-jadwal-html').render('view_pokja.tender.edit.jadwal')
Route.on('pokja-tender-edit-jadwal-filled-html').render('view_pokja.tender.edit.jadwal-filled')
Route.on('pokja-tender-edit-jadwal-disabled-html').render('view_pokja.tender.edit.jadwal-disabled')
Route.on('pokja-tender-edit-konfirmasi-pembatalan-dokumen-html').render('view_pokja.tender.edit.konfirmasi-pembatalan-dokumen')
Route.on('pokja-tender-edit-konfirmasi-pembatalan-persetujuan-html').render('view_pokja.tender.edit.konfirmasi-pembatalan-persetujuan')
Route.on('pokja-tender-edit-masa-berlaku-penawaran-html').render('view_pokja.tender.edit.masa-berlaku-penawaran')
Route.on('pokja-tender-edit-metode-pengadaan-html').render('view_pokja.tender.edit.metode-pengadaan')
Route.on('pokja-tender-edit-persyaratan-kualifikasi-html').render('view_pokja.tender.edit.persyaratan-kualifikasi')
Route.on('pokja-tender-edit-rincian-hps-html').render('view_pokja.tender.edit.rincian-hps')
Route.on('pokja-tender-edit-upload-dokumen-pemilihan-html').render('view_pokja.tender.edit.upload-dokumen-pemilihan')
Route.on('pokja-tender-edit-upload-informasi-lain-html').render('view_pokja.tender.edit.upload-informasi-lain')
Route.on('pokja-tender-edit-upload-kak-html').render('view_pokja.tender.edit.upload-kak')
Route.on('pokja-tender-edit-upload-rancangan-kontrak-html').render('view_pokja.tender.edit.upload-rancangan-kontrak')
Route.on('pokja-tender-edit-form-setuju-html').render('view_pokja.tender.edit.form-setuju')
/*
Route.on('pokja-tender-adendum-').render('view_pokja.tender.adendum.')
*/
Route.on('pokja-tender-adendum-dokumen-pemilihan-html').render('view_pokja.tender.adendum.dokumen-pemilihan')
Route.on('pokja-tender-adendum-dokumen-penawaran-html').render('view_pokja.tender.adendum.dokumen-penawaran')
Route.on('pokja-tender-adendum-form-filled-html').render('view_pokja.tender.adendum.form-filled')
Route.on('pokja-tender-adendum-form-uploaded-html').render('view_pokja.tender.adendum.form-uploaded')
Route.on('pokja-tender-adendum-form-persetujuan-html').render('view_pokja.tender.adendum.form-persetujuan')
Route.on('pokja-tender-adendum-form-html').render('view_pokja.tender.adendum.form')
Route.on('pokja-tender-adendum-jadwal-html').render('view_pokja.tender.adendum.jadwal')
Route.on('pokja-tender-adendum-jadwal-filled-html').render('view_pokja.tender.adendum.jadwal-filled')
Route.on('pokja-tender-adendum-jadwal-disabled-html').render('view_pokja.tender.adendum.jadwal-disabled')
Route.on('pokja-tender-adendum-konfirmasi-pembatalan-dokumen-html').render('view_pokja.tender.adendum.konfirmasi-pembatalan-dokumen')
Route.on('pokja-tender-adendum-konfirmasi-pembatalan-persetujuan-html').render('view_pokja.tender.adendum.konfirmasi-pembatalan-persetujuan')
Route.on('pokja-tender-adendum-masa-berlaku-penawaran-html').render('view_pokja.tender.adendum.masa-berlaku-penawaran')
Route.on('pokja-tender-adendum-metode-pengadaan-html').render('view_pokja.tender.adendum.metode-pengadaan')
Route.on('pokja-tender-adendum-persyaratan-kualifikasi-html').render('view_pokja.tender.adendum.persyaratan-kualifikasi')
Route.on('pokja-tender-adendum-rincian-hps-html').render('view_pokja.tender.adendum.rincian-hps')
Route.on('pokja-tender-adendum-upload-dokumen-pemilihan-html').render('view_pokja.tender.adendum.upload-dokumen-pemilihan')
Route.on('pokja-tender-adendum-upload-informasi-lain-html').render('view_pokja.tender.adendum.upload-informasi-lain')
Route.on('pokja-tender-adendum-upload-kak-html').render('view_pokja.tender.adendum.upload-kak')
Route.on('pokja-tender-adendum-upload-rancangan-kontrak-html').render('view_pokja.tender.adendum.upload-rancangan-kontrak')
Route.on('pokja-tender-adendum-form-setuju-html').render('view_pokja.tender.adendum.form-setuju')

Route.on('pokja-tender-detail-upload-berita-acara-html').render('view_pokja.tender.upload-berita-acara')
Route.on('pokja-tender-detail-cetak-berita-acara-html').render('view_pokja.tender.cetak-berita-acara')
Route.on('pokja-tender-detail-upload-berita-acara-lain-html').render('view_pokja.tender.upload-berita-acara-lain')



Route.on('pokja-tender-detail-pengumuman-kirim-html').render('view_pokja.tender.detail-pengumuman.kirim')
Route.on('pokja-tender-detail-pengumuman-sent-html').render('view_pokja.tender.detail-pengumuman.sent')

Route.on('pokja-tender-detail-pengumuman-html').render('view_pokja.tender.detail-pengumuman.index')
Route.on('pokja-tender-detail-pengumuman-tender-gagal-html').render('view_pokja.tender.detail-pengumuman.tender-gagal')
Route.on('pokja-tender-detail-pengumuman-konfirmasi-tender-gagal-html').render('view_pokja.tender.detail-pengumuman.konfirmasi-tender-gagal')
Route.on('pokja-tender-detail-pengumuman-konfirmasi-tender-ulang-html').render('view_pokja.tender.detail-pengumuman.konfirmasi-tender-ulang')
Route.on('pokja-tender-detail-pengumuman-konfirmasi-evaluasi-ulang-html').render('view_pokja.tender.detail-pengumuman.konfirmasi-evaluasi-ulang')
Route.on('pokja-tender-detail-pengumuman-konfirmasi-tender-batal-html').render('view_pokja.tender.detail-pengumuman.konfirmasi-tender-batal')

Route.on('pokja-tender-detail-pengumuman-tender-ulang-html').render('view_pokja.tender.detail-pengumuman.tender-ulang')
Route.on('pokja-tender-detail-pengumuman-tender-batal-html').render('view_pokja.tender.detail-pengumuman.tender-batal')

Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.index')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-tender-gagal-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.tender-gagal')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-gagal-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-gagal')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-ulang-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-ulang')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-konfirmasi-evaluasi-ulang-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.konfirmasi-evaluasi-ulang')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-batal-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-batal')

Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-tender-ulang-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.tender-ulang')
Route.on('pokja-tender-detail-pengumuman-pascakualifikasi-tender-batal-html').render('view_pokja.tender.detail-pengumuman-pascakualifikasi.tender-batal')

// ppk
/*
Route.on('ppk-').render('view_ppk.')
*/
Route.on('ppk-html').render('view_ppk.index')
Route.on('ppk-tender-detail-evaluasi-penetapan-pemenang-cadangan-html').render('view_ppk.tender.detail-evaluasi.penetapan-pemenang-cadangan')
Route.on('ppk-tender-detail-evaluasi-undangan-kontrak-html').render('view_ppk.tender.detail-evaluasi.undangan-kontrak')

Route.on('ppk-daftar-paket-html').render('view_ppk.daftar-paket.index')
Route.on('ppk-daftar-paket-pilih-rup-html').render('view_ppk.daftar-paket.pilih-rup')
Route.on('ppk-daftar-paket-rencana-pengadaan-html').render('view_ppk.daftar-paket.rencana-pengadaan')
Route.on('ppk-daftar-paket-konfirmasi-hapus-html').render('view_ppk.daftar-paket.konfirmasi-hapus')
Route.on('ppk-daftar-paket-konfirmasi-buat-paket-html').render('view_ppk.daftar-paket.konfirmasi-buat-paket')
Route.on('ppk-daftar-paket-form-data-paket-html').render('view_ppk.daftar-paket.form-data-paket')
Route.on('ppk-daftar-paket-form-dokumen-persiapan-html').render('view_ppk.daftar-paket.form-dokumen-persiapan')
Route.on('ppk-daftar-paket-rincian-hps-html').render('view_ppk.daftar-paket.rincian-hps')
Route.on('ppk-daftar-paket-pembulatan-nilai-hps-html').render('view_ppk.daftar-paket.pembulatan-nilai-hps')
Route.on('ppk-daftar-paket-pilih-ukpbj-html').render('view_ppk.daftar-paket.pilih-ukpbj')
Route.on('ppk-daftar-paket-upload-kak-html').render('view_ppk.daftar-paket.upload-kak')
Route.on('ppk-daftar-paket-upload-rancangan-kontrak-html').render('view_ppk.daftar-paket.upload-rancangan-kontrak')
Route.on('ppk-daftar-paket-upload-informasi-lain-html').render('view_ppk.daftar-paket.upload-informasi-lain')

Route.on('ppk-non-tender-html').render('view_ppk.non-tender.index')
Route.on('ppk-non-tender-detail-html').render('view_ppk.non-tender.detail')
Route.on('ppk-non-tender-detail-evaluasi-html').render('view_ppk.non-tender.detail-evaluasi.index')
Route.on('ppk-non-tender-detail-penawaran-html').render('view_ppk.non-tender.detail-penawaran.index')

Route.on('ppk-non-tender-e-kontrak-html').render('view_ppk.non-tender.e-kontrak.index')
Route.on('ppk-non-tender-e-kontrak-kontrak-html').render('view_ppk.non-tender.e-kontrak.kontrak')
Route.on('ppk-non-tender-e-kontrak-sppbj-html').render('view_ppk.non-tender.e-kontrak.sppbj')


Route.on('ppk-pencatatan-non-tender-html').render('view_ppk.pencatatan-non-tender.index')
Route.on('ppk-pencatatan-non-tender-form-html').render('view_ppk.pencatatan-non-tender.form')
Route.on('ppk-pencatatan-non-tender-detail-html').render('view_ppk.pencatatan-non-tender.detail')
Route.on('ppk-pencatatan-non-tender-form-realisasi-html').render('view_ppk.pencatatan-non-tender.form-realisasi')
Route.on('ppk-pencatatan-non-tender-pilih-penyedia-html').render('view_ppk.pencatatan-non-tender.pilih-penyedia')
Route.on('ppk-pencatatan-non-tender-tambah-penyedia-html').render('view_ppk.pencatatan-non-tender.tambah-penyedia')

Route.on('ppk-pencatatan-swakelola-html').render('view_ppk.pencatatan-swakelola.index')
Route.on('ppk-pencatatan-pengadaan-darurat-html').render('view_ppk.pencatatan-pengadaan-darurat.index')


Route.on('ppk-e-kontrak-html').render('view_ppk.e-kontrak.index')
Route.on('ppk-e-kontrak-sppbj-html').render('view_ppk.e-kontrak.sppbj')
Route.on('ppk-e-kontrak-rincian-harga-penawaran-html').render('view_ppk.e-kontrak.rincian-harga-penawaran')
Route.on('ppk-e-kontrak-rincian-barang-html').render('view_ppk.e-kontrak.rincian-barang')

Route.on('ppk-e-kontrak-kontrak-html').render('view_ppk.e-kontrak.kontrak')
Route.on('ppk-e-kontrak-pembayaran-html').render('view_ppk.e-kontrak.pembayaran')
Route.on('ppk-e-kontrak-penilaian-html').render('view_ppk.e-kontrak.penilaian')
Route.on('ppk-e-kontrak-sskk-html').render('view_ppk.e-kontrak.sskk')
Route.on('ppk-e-kontrak-surat-pesanan-html').render('view_ppk.e-kontrak.surat-pesanan')

Route.on('ppk-e-kontrak-kontrak-form-html').render('view_ppk.e-kontrak.kontrak-form')
Route.on('ppk-e-kontrak-pembayaran-form-html').render('view_ppk.e-kontrak.pembayaran-form')
Route.on('ppk-e-kontrak-penilaian-form-html').render('view_ppk.e-kontrak.penilaian-form')
Route.on('ppk-e-kontrak-sskk-form-html').render('view_ppk.e-kontrak.sskk-form')
Route.on('ppk-e-kontrak-surat-pesanan-form-html').render('view_ppk.e-kontrak.surat-pesanan-form')

Route.on('ppk-tender-detail-html').render('view_ppk.tender.detail')
Route.on('ppk-tender-detail-filled-html').render('view_ppk.tender.detail-filled')
Route.on('ppk-tender-detail-uploaded-html').render('view_ppk.tender.detail-uploaded')

Route.on('ppk-tender-detail-pertanyaan-html').render('view_ppk.tender.detail-pertanyaan')
Route.on('ppk-tender-detail-pertanyaan-edit-html').render('view_ppk.tender.detail-pertanyaan-edit')
Route.on('ppk-tender-detail-pertanyaan-pembukaan-html').render('view_ppk.tender.detail-pertanyaan-pembukaan')
Route.on('ppk-tender-detail-pertanyaan-jawab-html').render('view_ppk.tender.detail-pertanyaan-jawab')
Route.on('ppk-tender-detail-pertanyaan-jawaban-html').render('view_ppk.tender.detail-pertanyaan-jawaban')



Route.on('ppk-tender-detail-penawaran-html').render('view_ppk.tender.detail-penawaran.index')
Route.on('ppk-tender-detail-penawaran-kualifikasi-html').render('view_ppk.tender.detail-penawaran.kualifikasi')
Route.on('ppk-tender-detail-penawaran-administrasi-html').render('view_ppk.tender.detail-penawaran.administrasi')
Route.on('ppk-tender-detail-penawaran-harga-html').render('view_ppk.tender.detail-penawaran.harga')



Route.on('ppk-tender-detail-reverse-auction-html').render('view_ppk.tender.detail-reverse-auction.index')
Route.on('ppk-tender-detail-reverse-auction-scheduled-html').render('view_ppk.tender.detail-reverse-auction.scheduled')
Route.on('ppk-tender-detail-reverse-auction-done-html').render('view_ppk.tender.detail-reverse-auction.done')
Route.on('ppk-tender-detail-reverse-auction-ubah-jadwal-html').render('view_ppk.tender.detail-reverse-auction.ubah-jadwal')
Route.on('ppk-tender-detail-reverse-auction-rincian-penawaran-html').render('view_ppk.tender.detail-reverse-auction.rincian-penawaran')



Route.on('ppk-tender-detail-evaluasi-html').render('view_ppk.tender.detail-evaluasi.index')
// Route.on('ppk-tender-detail-evaluasi--html').render('view_ppk.tender.detail-evaluasi.')
Route.on('ppk-tender-detail-evaluasi-administrasi-html').render('view_ppk.tender.detail-evaluasi.administrasi')
Route.on('ppk-tender-detail-evaluasi-kualifikasi-html').render('view_ppk.tender.detail-evaluasi.kualifikasi')
Route.on('ppk-tender-detail-evaluasi-pesan-html').render('view_ppk.tender.detail-evaluasi.pesan')
Route.on('ppk-tender-detail-evaluasi-pesan-pembuktian-html').render('view_ppk.tender.detail-evaluasi.pesan-pembuktian')
Route.on('ppk-tender-detail-evaluasi-teknis-html').render('view_ppk.tender.detail-evaluasi.teknis')
Route.on('ppk-tender-detail-evaluasi-harga-html').render('view_ppk.tender.detail-evaluasi.harga')
Route.on('ppk-tender-detail-evaluasi-pembuktian-html').render('view_ppk.tender.detail-evaluasi.pembuktian')
Route.on('ppk-tender-detail-evaluasi-konfirmasi-html').render('view_ppk.tender.detail-evaluasi.konfirmasi')
Route.on('ppk-tender-detail-evaluasi-konfirmasi-failed-html').render('view_ppk.tender.detail-evaluasi.konfirmasi-failed')
Route.on('ppk-tender-detail-evaluasi-checked-html').render('view_ppk.tender.detail-evaluasi.checked')
Route.on('ppk-tender-detail-evaluasi-jadwal-html').render('view_ppk.tender.detail-evaluasi.jadwal')
Route.on('ppk-tender-detail-evaluasi-jadwal-filled-html').render('view_ppk.tender.detail-evaluasi.jadwal-filled')
Route.on('ppk-tender-detail-evaluasi-nego-html').render('view_ppk.tender.detail-evaluasi.nego')
Route.on('ppk-tender-detail-evaluasi-nego-filled-html').render('view_ppk.tender.detail-evaluasi.nego-filled')
Route.on('ppk-tender-detail-evaluasi-input-nego-html').render('view_ppk.tender.detail-evaluasi.input-nego')
Route.on('ppk-tender-detail-evaluasi-penetapan-pemenang-html').render('view_ppk.tender.detail-evaluasi.penetapan-pemenang')
Route.on('ppk-tender-detail-evaluasi-pemenang-html').render('view_ppk.tender.detail-evaluasi.pemenang')


Route.on('ppk-tender-detail-sanggahan-html').render('view_ppk.tender.detail-sanggahan.index')
Route.on('ppk-tender-detail-sanggahan-ubah-jadwal-html').render('view_ppk.tender.detail-sanggahan.ubah-jadwal')
Route.on('ppk-tender-detail-sanggahan-jawab-html').render('view_ppk.tender.detail-sanggahan.jawab')
Route.on('ppk-tender-detail-sanggahan-jawaban-html').render('view_ppk.tender.detail-sanggahan.jawaban')

Route.on('ppk-tender-pilih-ppk-html').render('view_ppk.tender.pilih-ppk')
Route.on('ppk-tender-pilih-ppk-alasan-html').render('view_ppk.tender.pilih-ppk-alasan')

/*
Route.on('ppk-tender-edit-').render('view_ppk.tender.edit.')
*/
Route.on('ppk-tender-edit-dokumen-pemilihan-html').render('view_ppk.tender.edit.dokumen-pemilihan')
Route.on('ppk-tender-edit-dokumen-penawaran-html').render('view_ppk.tender.edit.dokumen-penawaran')
Route.on('ppk-tender-edit-form-filled-html').render('view_ppk.tender.edit.form-filled')
Route.on('ppk-tender-edit-form-uploaded-html').render('view_ppk.tender.edit.form-uploaded')
Route.on('ppk-tender-edit-form-persetujuan-html').render('view_ppk.tender.edit.form-persetujuan')
Route.on('ppk-tender-edit-form-html').render('view_ppk.tender.edit.form')
Route.on('ppk-tender-edit-jadwal-html').render('view_ppk.tender.edit.jadwal')
Route.on('ppk-tender-edit-jadwal-filled-html').render('view_ppk.tender.edit.jadwal-filled')
Route.on('ppk-tender-edit-jadwal-disabled-html').render('view_ppk.tender.edit.jadwal-disabled')
Route.on('ppk-tender-edit-konfirmasi-pembatalan-dokumen-html').render('view_ppk.tender.edit.konfirmasi-pembatalan-dokumen')
Route.on('ppk-tender-edit-konfirmasi-pembatalan-persetujuan-html').render('view_ppk.tender.edit.konfirmasi-pembatalan-persetujuan')
Route.on('ppk-tender-edit-masa-berlaku-penawaran-html').render('view_ppk.tender.edit.masa-berlaku-penawaran')
Route.on('ppk-tender-edit-metode-pengadaan-html').render('view_ppk.tender.edit.metode-pengadaan')
Route.on('ppk-tender-edit-persyaratan-kualifikasi-html').render('view_ppk.tender.edit.persyaratan-kualifikasi')
Route.on('ppk-tender-edit-rincian-hps-html').render('view_ppk.tender.edit.rincian-hps')
Route.on('ppk-tender-edit-upload-dokumen-pemilihan-html').render('view_ppk.tender.edit.upload-dokumen-pemilihan')
Route.on('ppk-tender-edit-upload-informasi-lain-html').render('view_ppk.tender.edit.upload-informasi-lain')
Route.on('ppk-tender-edit-upload-kak-html').render('view_ppk.tender.edit.upload-kak')
Route.on('ppk-tender-edit-upload-rancangan-kontrak-html').render('view_ppk.tender.edit.upload-rancangan-kontrak')
Route.on('ppk-tender-edit-form-setuju-html').render('view_ppk.tender.edit.form-setuju')
/*
Route.on('ppk-tender-adendum-').render('view_ppk.tender.adendum.')
*/
Route.on('ppk-tender-adendum-dokumen-pemilihan-html').render('view_ppk.tender.adendum.dokumen-pemilihan')
Route.on('ppk-tender-adendum-dokumen-penawaran-html').render('view_ppk.tender.adendum.dokumen-penawaran')
Route.on('ppk-tender-adendum-form-filled-html').render('view_ppk.tender.adendum.form-filled')
Route.on('ppk-tender-adendum-form-uploaded-html').render('view_ppk.tender.adendum.form-uploaded')
Route.on('ppk-tender-adendum-form-persetujuan-html').render('view_ppk.tender.adendum.form-persetujuan')
Route.on('ppk-tender-adendum-form-html').render('view_ppk.tender.adendum.form')
Route.on('ppk-tender-adendum-jadwal-html').render('view_ppk.tender.adendum.jadwal')
Route.on('ppk-tender-adendum-jadwal-filled-html').render('view_ppk.tender.adendum.jadwal-filled')
Route.on('ppk-tender-adendum-jadwal-disabled-html').render('view_ppk.tender.adendum.jadwal-disabled')
Route.on('ppk-tender-adendum-konfirmasi-pembatalan-dokumen-html').render('view_ppk.tender.adendum.konfirmasi-pembatalan-dokumen')
Route.on('ppk-tender-adendum-konfirmasi-pembatalan-persetujuan-html').render('view_ppk.tender.adendum.konfirmasi-pembatalan-persetujuan')
Route.on('ppk-tender-adendum-masa-berlaku-penawaran-html').render('view_ppk.tender.adendum.masa-berlaku-penawaran')
Route.on('ppk-tender-adendum-metode-pengadaan-html').render('view_ppk.tender.adendum.metode-pengadaan')
Route.on('ppk-tender-adendum-persyaratan-kualifikasi-html').render('view_ppk.tender.adendum.persyaratan-kualifikasi')
Route.on('ppk-tender-adendum-rincian-hps-html').render('view_ppk.tender.adendum.rincian-hps')
Route.on('ppk-tender-adendum-upload-dokumen-pemilihan-html').render('view_ppk.tender.adendum.upload-dokumen-pemilihan')
Route.on('ppk-tender-adendum-upload-informasi-lain-html').render('view_ppk.tender.adendum.upload-informasi-lain')
Route.on('ppk-tender-adendum-upload-kak-html').render('view_ppk.tender.adendum.upload-kak')
Route.on('ppk-tender-adendum-upload-rancangan-kontrak-html').render('view_ppk.tender.adendum.upload-rancangan-kontrak')
Route.on('ppk-tender-adendum-form-setuju-html').render('view_ppk.tender.adendum.form-setuju')

Route.on('ppk-tender-detail-upload-berita-acara-html').render('view_ppk.tender.upload-berita-acara')
Route.on('ppk-tender-detail-cetak-berita-acara-html').render('view_ppk.tender.cetak-berita-acara')
Route.on('ppk-tender-detail-upload-berita-acara-lain-html').render('view_ppk.tender.upload-berita-acara-lain')



Route.on('ppk-tender-detail-pengumuman-kirim-html').render('view_ppk.tender.detail-pengumuman.kirim')
Route.on('ppk-tender-detail-pengumuman-sent-html').render('view_ppk.tender.detail-pengumuman.sent')

Route.on('ppk-tender-detail-pengumuman-html').render('view_ppk.tender.detail-pengumuman.index')
Route.on('ppk-tender-detail-pengumuman-tender-gagal-html').render('view_ppk.tender.detail-pengumuman.tender-gagal')
Route.on('ppk-tender-detail-pengumuman-konfirmasi-tender-gagal-html').render('view_ppk.tender.detail-pengumuman.konfirmasi-tender-gagal')
Route.on('ppk-tender-detail-pengumuman-konfirmasi-tender-ulang-html').render('view_ppk.tender.detail-pengumuman.konfirmasi-tender-ulang')
Route.on('ppk-tender-detail-pengumuman-konfirmasi-evaluasi-ulang-html').render('view_ppk.tender.detail-pengumuman.konfirmasi-evaluasi-ulang')
Route.on('ppk-tender-detail-pengumuman-konfirmasi-tender-batal-html').render('view_ppk.tender.detail-pengumuman.konfirmasi-tender-batal')

Route.on('ppk-tender-detail-pengumuman-tender-ulang-html').render('view_ppk.tender.detail-pengumuman.tender-ulang')
Route.on('ppk-tender-detail-pengumuman-tender-batal-html').render('view_ppk.tender.detail-pengumuman.tender-batal')

Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.index')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-tender-gagal-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.tender-gagal')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-gagal-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-gagal')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-ulang-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-ulang')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-konfirmasi-evaluasi-ulang-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.konfirmasi-evaluasi-ulang')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-batal-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-batal')

Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-tender-ulang-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.tender-ulang')
Route.on('ppk-tender-detail-pengumuman-pascakualifikasi-tender-batal-html').render('view_ppk.tender.detail-pengumuman-pascakualifikasi.tender-batal')

// kuppbj
/*
Route.on('kuppbj-').render('view_kuppbj.')
*/
Route.on('kuppbj-html').render('view_kuppbj.ukpbj.index')
Route.on('kuppbj-tender-detail-evaluasi-penetapan-pemenang-cadangan-html').render('view_kuppbj.tender.detail-evaluasi.penetapan-pemenang-cadangan')
Route.on('kuppbj-tender-detail-evaluasi-undangan-kontrak-html').render('view_kuppbj.tender.detail-evaluasi.undangan-kontrak')

Route.on('kuppbj-daftar-paket-html').render('view_kuppbj.daftar-paket.index')
Route.on('kuppbj-daftar-paket-pilih-rup-html').render('view_kuppbj.daftar-paket.pilih-rup')
Route.on('kuppbj-daftar-paket-rencana-pengadaan-html').render('view_kuppbj.daftar-paket.rencana-pengadaan')
Route.on('kuppbj-daftar-paket-konfirmasi-hapus-html').render('view_kuppbj.daftar-paket.konfirmasi-hapus')
Route.on('kuppbj-daftar-paket-konfirmasi-buat-paket-html').render('view_kuppbj.daftar-paket.konfirmasi-buat-paket')
Route.on('kuppbj-daftar-paket-form-data-paket-html').render('view_kuppbj.daftar-paket.form-data-paket')
Route.on('kuppbj-daftar-paket-form-dokumen-persiapan-html').render('view_kuppbj.daftar-paket.form-dokumen-persiapan')
Route.on('kuppbj-daftar-paket-rincian-hps-html').render('view_kuppbj.daftar-paket.rincian-hps')
Route.on('kuppbj-daftar-paket-pembulatan-nilai-hps-html').render('view_kuppbj.daftar-paket.pembulatan-nilai-hps')
Route.on('kuppbj-daftar-paket-pilih-pokja-pemilihan-html').render('view_kuppbj.daftar-paket.pilih-pokja-pemilihan')
Route.on('kuppbj-daftar-paket-upload-kak-html').render('view_kuppbj.daftar-paket.upload-kak')
Route.on('kuppbj-daftar-paket-upload-rancangan-kontrak-html').render('view_kuppbj.daftar-paket.upload-rancangan-kontrak')
Route.on('kuppbj-daftar-paket-upload-informasi-lain-html').render('view_kuppbj.daftar-paket.upload-informasi-lain')

Route.on('kuppbj-non-tender-html').render('view_kuppbj.non-tender.index')
Route.on('kuppbj-non-tender-detail-html').render('view_kuppbj.non-tender.detail')
Route.on('kuppbj-non-tender-detail-evaluasi-html').render('view_kuppbj.non-tender.detail-evaluasi.index')
Route.on('kuppbj-non-tender-detail-penawaran-html').render('view_kuppbj.non-tender.detail-penawaran.index')

Route.on('kuppbj-non-tender-e-kontrak-html').render('view_kuppbj.non-tender.e-kontrak.index')
Route.on('kuppbj-non-tender-e-kontrak-kontrak-html').render('view_kuppbj.non-tender.e-kontrak.kontrak')
Route.on('kuppbj-non-tender-e-kontrak-sppbj-html').render('view_kuppbj.non-tender.e-kontrak.sppbj')


Route.on('kuppbj-pencatatan-non-tender-html').render('view_kuppbj.pencatatan-non-tender.index')
Route.on('kuppbj-pencatatan-non-tender-form-html').render('view_kuppbj.pencatatan-non-tender.form')
Route.on('kuppbj-pencatatan-non-tender-detail-html').render('view_kuppbj.pencatatan-non-tender.detail')
Route.on('kuppbj-pencatatan-non-tender-form-realisasi-html').render('view_kuppbj.pencatatan-non-tender.form-realisasi')
Route.on('kuppbj-pencatatan-non-tender-pilih-penyedia-html').render('view_kuppbj.pencatatan-non-tender.pilih-penyedia')
Route.on('kuppbj-pencatatan-non-tender-tambah-penyedia-html').render('view_kuppbj.pencatatan-non-tender.tambah-penyedia')

Route.on('kuppbj-pencatatan-swakelola-html').render('view_kuppbj.pencatatan-swakelola.index')
Route.on('kuppbj-pencatatan-pengadaan-darurat-html').render('view_kuppbj.pencatatan-pengadaan-darurat.index')


Route.on('kuppbj-e-kontrak-html').render('view_kuppbj.e-kontrak.index')
Route.on('kuppbj-e-kontrak-sppbj-html').render('view_kuppbj.e-kontrak.sppbj')
Route.on('kuppbj-e-kontrak-rincian-harga-penawaran-html').render('view_kuppbj.e-kontrak.rincian-harga-penawaran')
Route.on('kuppbj-e-kontrak-rincian-barang-html').render('view_kuppbj.e-kontrak.rincian-barang')

Route.on('kuppbj-e-kontrak-kontrak-html').render('view_kuppbj.e-kontrak.kontrak')
Route.on('kuppbj-e-kontrak-pembayaran-html').render('view_kuppbj.e-kontrak.pembayaran')
Route.on('kuppbj-e-kontrak-penilaian-html').render('view_kuppbj.e-kontrak.penilaian')
Route.on('kuppbj-e-kontrak-sskk-html').render('view_kuppbj.e-kontrak.sskk')
Route.on('kuppbj-e-kontrak-surat-pesanan-html').render('view_kuppbj.e-kontrak.surat-pesanan')

Route.on('kuppbj-e-kontrak-kontrak-form-html').render('view_kuppbj.e-kontrak.kontrak-form')
Route.on('kuppbj-e-kontrak-pembayaran-form-html').render('view_kuppbj.e-kontrak.pembayaran-form')
Route.on('kuppbj-e-kontrak-penilaian-form-html').render('view_kuppbj.e-kontrak.penilaian-form')
Route.on('kuppbj-e-kontrak-sskk-form-html').render('view_kuppbj.e-kontrak.sskk-form')
Route.on('kuppbj-e-kontrak-surat-pesanan-form-html').render('view_kuppbj.e-kontrak.surat-pesanan-form')

Route.on('kuppbj-tender-detail-html').render('view_kuppbj.tender.detail')
Route.on('kuppbj-tender-detail-filled-html').render('view_kuppbj.tender.detail-filled')
Route.on('kuppbj-tender-detail-uploaded-html').render('view_kuppbj.tender.detail-uploaded')

Route.on('kuppbj-tender-detail-pertanyaan-html').render('view_kuppbj.tender.detail-pertanyaan')
Route.on('kuppbj-tender-detail-pertanyaan-edit-html').render('view_kuppbj.tender.detail-pertanyaan-edit')
Route.on('kuppbj-tender-detail-pertanyaan-pembukaan-html').render('view_kuppbj.tender.detail-pertanyaan-pembukaan')
Route.on('kuppbj-tender-detail-pertanyaan-jawab-html').render('view_kuppbj.tender.detail-pertanyaan-jawab')
Route.on('kuppbj-tender-detail-pertanyaan-jawaban-html').render('view_kuppbj.tender.detail-pertanyaan-jawaban')



Route.on('kuppbj-tender-detail-penawaran-html').render('view_kuppbj.tender.detail-penawaran.index')
Route.on('kuppbj-tender-detail-penawaran-kualifikasi-html').render('view_kuppbj.tender.detail-penawaran.kualifikasi')
Route.on('kuppbj-tender-detail-penawaran-administrasi-html').render('view_kuppbj.tender.detail-penawaran.administrasi')
Route.on('kuppbj-tender-detail-penawaran-harga-html').render('view_kuppbj.tender.detail-penawaran.harga')



Route.on('kuppbj-tender-detail-reverse-auction-html').render('view_kuppbj.tender.detail-reverse-auction.index')
Route.on('kuppbj-tender-detail-reverse-auction-scheduled-html').render('view_kuppbj.tender.detail-reverse-auction.scheduled')
Route.on('kuppbj-tender-detail-reverse-auction-done-html').render('view_kuppbj.tender.detail-reverse-auction.done')
Route.on('kuppbj-tender-detail-reverse-auction-ubah-jadwal-html').render('view_kuppbj.tender.detail-reverse-auction.ubah-jadwal')
Route.on('kuppbj-tender-detail-reverse-auction-rincian-penawaran-html').render('view_kuppbj.tender.detail-reverse-auction.rincian-penawaran')



Route.on('kuppbj-tender-detail-evaluasi-html').render('view_kuppbj.tender.detail-evaluasi.index')
// Route.on('kuppbj-tender-detail-evaluasi--html').render('view_kuppbj.tender.detail-evaluasi.')
Route.on('kuppbj-tender-detail-evaluasi-administrasi-html').render('view_kuppbj.tender.detail-evaluasi.administrasi')
Route.on('kuppbj-tender-detail-evaluasi-kualifikasi-html').render('view_kuppbj.tender.detail-evaluasi.kualifikasi')
Route.on('kuppbj-tender-detail-evaluasi-pesan-html').render('view_kuppbj.tender.detail-evaluasi.pesan')
Route.on('kuppbj-tender-detail-evaluasi-pesan-pembuktian-html').render('view_kuppbj.tender.detail-evaluasi.pesan-pembuktian')
Route.on('kuppbj-tender-detail-evaluasi-teknis-html').render('view_kuppbj.tender.detail-evaluasi.teknis')
Route.on('kuppbj-tender-detail-evaluasi-harga-html').render('view_kuppbj.tender.detail-evaluasi.harga')
Route.on('kuppbj-tender-detail-evaluasi-pembuktian-html').render('view_kuppbj.tender.detail-evaluasi.pembuktian')
Route.on('kuppbj-tender-detail-evaluasi-konfirmasi-html').render('view_kuppbj.tender.detail-evaluasi.konfirmasi')
Route.on('kuppbj-tender-detail-evaluasi-konfirmasi-failed-html').render('view_kuppbj.tender.detail-evaluasi.konfirmasi-failed')
Route.on('kuppbj-tender-detail-evaluasi-checked-html').render('view_kuppbj.tender.detail-evaluasi.checked')
Route.on('kuppbj-tender-detail-evaluasi-jadwal-html').render('view_kuppbj.tender.detail-evaluasi.jadwal')
Route.on('kuppbj-tender-detail-evaluasi-jadwal-filled-html').render('view_kuppbj.tender.detail-evaluasi.jadwal-filled')
Route.on('kuppbj-tender-detail-evaluasi-nego-html').render('view_kuppbj.tender.detail-evaluasi.nego')
Route.on('kuppbj-tender-detail-evaluasi-nego-filled-html').render('view_kuppbj.tender.detail-evaluasi.nego-filled')
Route.on('kuppbj-tender-detail-evaluasi-input-nego-html').render('view_kuppbj.tender.detail-evaluasi.input-nego')
Route.on('kuppbj-tender-detail-evaluasi-penetapan-pemenang-html').render('view_kuppbj.tender.detail-evaluasi.penetapan-pemenang')
Route.on('kuppbj-tender-detail-evaluasi-pemenang-html').render('view_kuppbj.tender.detail-evaluasi.pemenang')


Route.on('kuppbj-tender-detail-sanggahan-html').render('view_kuppbj.tender.detail-sanggahan.index')
Route.on('kuppbj-tender-detail-sanggahan-ubah-jadwal-html').render('view_kuppbj.tender.detail-sanggahan.ubah-jadwal')
Route.on('kuppbj-tender-detail-sanggahan-jawab-html').render('view_kuppbj.tender.detail-sanggahan.jawab')
Route.on('kuppbj-tender-detail-sanggahan-jawaban-html').render('view_kuppbj.tender.detail-sanggahan.jawaban')

Route.on('kuppbj-tender-pilih-kuppbj-html').render('view_kuppbj.tender.pilih-kuppbj')
Route.on('kuppbj-tender-pilih-kuppbj-alasan-html').render('view_kuppbj.tender.pilih-kuppbj-alasan')

/*
Route.on('kuppbj-tender-edit-').render('view_kuppbj.tender.edit.')
*/
Route.on('kuppbj-tender-edit-dokumen-pemilihan-html').render('view_kuppbj.tender.edit.dokumen-pemilihan')
Route.on('kuppbj-tender-edit-dokumen-penawaran-html').render('view_kuppbj.tender.edit.dokumen-penawaran')
Route.on('kuppbj-tender-edit-form-filled-html').render('view_kuppbj.tender.edit.form-filled')
Route.on('kuppbj-tender-edit-form-uploaded-html').render('view_kuppbj.tender.edit.form-uploaded')
Route.on('kuppbj-tender-edit-form-persetujuan-html').render('view_kuppbj.tender.edit.form-persetujuan')
Route.on('kuppbj-tender-edit-form-html').render('view_kuppbj.tender.edit.form')
Route.on('kuppbj-tender-edit-jadwal-html').render('view_kuppbj.tender.edit.jadwal')
Route.on('kuppbj-tender-edit-jadwal-filled-html').render('view_kuppbj.tender.edit.jadwal-filled')
Route.on('kuppbj-tender-edit-jadwal-disabled-html').render('view_kuppbj.tender.edit.jadwal-disabled')
Route.on('kuppbj-tender-edit-konfirmasi-pembatalan-dokumen-html').render('view_kuppbj.tender.edit.konfirmasi-pembatalan-dokumen')
Route.on('kuppbj-tender-edit-konfirmasi-pembatalan-persetujuan-html').render('view_kuppbj.tender.edit.konfirmasi-pembatalan-persetujuan')
Route.on('kuppbj-tender-edit-masa-berlaku-penawaran-html').render('view_kuppbj.tender.edit.masa-berlaku-penawaran')
Route.on('kuppbj-tender-edit-metode-pengadaan-html').render('view_kuppbj.tender.edit.metode-pengadaan')
Route.on('kuppbj-tender-edit-persyaratan-kualifikasi-html').render('view_kuppbj.tender.edit.persyaratan-kualifikasi')
Route.on('kuppbj-tender-edit-rincian-hps-html').render('view_kuppbj.tender.edit.rincian-hps')
Route.on('kuppbj-tender-edit-upload-dokumen-pemilihan-html').render('view_kuppbj.tender.edit.upload-dokumen-pemilihan')
Route.on('kuppbj-tender-edit-upload-informasi-lain-html').render('view_kuppbj.tender.edit.upload-informasi-lain')
Route.on('kuppbj-tender-edit-upload-kak-html').render('view_kuppbj.tender.edit.upload-kak')
Route.on('kuppbj-tender-edit-upload-rancangan-kontrak-html').render('view_kuppbj.tender.edit.upload-rancangan-kontrak')
Route.on('kuppbj-tender-edit-form-setuju-html').render('view_kuppbj.tender.edit.form-setuju')
/*
Route.on('kuppbj-tender-adendum-').render('view_kuppbj.tender.adendum.')
*/
Route.on('kuppbj-tender-adendum-dokumen-pemilihan-html').render('view_kuppbj.tender.adendum.dokumen-pemilihan')
Route.on('kuppbj-tender-adendum-dokumen-penawaran-html').render('view_kuppbj.tender.adendum.dokumen-penawaran')
Route.on('kuppbj-tender-adendum-form-filled-html').render('view_kuppbj.tender.adendum.form-filled')
Route.on('kuppbj-tender-adendum-form-uploaded-html').render('view_kuppbj.tender.adendum.form-uploaded')
Route.on('kuppbj-tender-adendum-form-persetujuan-html').render('view_kuppbj.tender.adendum.form-persetujuan')
Route.on('kuppbj-tender-adendum-form-html').render('view_kuppbj.tender.adendum.form')
Route.on('kuppbj-tender-adendum-jadwal-html').render('view_kuppbj.tender.adendum.jadwal')
Route.on('kuppbj-tender-adendum-jadwal-filled-html').render('view_kuppbj.tender.adendum.jadwal-filled')
Route.on('kuppbj-tender-adendum-jadwal-disabled-html').render('view_kuppbj.tender.adendum.jadwal-disabled')
Route.on('kuppbj-tender-adendum-konfirmasi-pembatalan-dokumen-html').render('view_kuppbj.tender.adendum.konfirmasi-pembatalan-dokumen')
Route.on('kuppbj-tender-adendum-konfirmasi-pembatalan-persetujuan-html').render('view_kuppbj.tender.adendum.konfirmasi-pembatalan-persetujuan')
Route.on('kuppbj-tender-adendum-masa-berlaku-penawaran-html').render('view_kuppbj.tender.adendum.masa-berlaku-penawaran')
Route.on('kuppbj-tender-adendum-metode-pengadaan-html').render('view_kuppbj.tender.adendum.metode-pengadaan')
Route.on('kuppbj-tender-adendum-persyaratan-kualifikasi-html').render('view_kuppbj.tender.adendum.persyaratan-kualifikasi')
Route.on('kuppbj-tender-adendum-rincian-hps-html').render('view_kuppbj.tender.adendum.rincian-hps')
Route.on('kuppbj-tender-adendum-upload-dokumen-pemilihan-html').render('view_kuppbj.tender.adendum.upload-dokumen-pemilihan')
Route.on('kuppbj-tender-adendum-upload-informasi-lain-html').render('view_kuppbj.tender.adendum.upload-informasi-lain')
Route.on('kuppbj-tender-adendum-upload-kak-html').render('view_kuppbj.tender.adendum.upload-kak')
Route.on('kuppbj-tender-adendum-upload-rancangan-kontrak-html').render('view_kuppbj.tender.adendum.upload-rancangan-kontrak')
Route.on('kuppbj-tender-adendum-form-setuju-html').render('view_kuppbj.tender.adendum.form-setuju')

Route.on('kuppbj-tender-detail-upload-berita-acara-html').render('view_kuppbj.tender.upload-berita-acara')
Route.on('kuppbj-tender-detail-cetak-berita-acara-html').render('view_kuppbj.tender.cetak-berita-acara')
Route.on('kuppbj-tender-detail-upload-berita-acara-lain-html').render('view_kuppbj.tender.upload-berita-acara-lain')



Route.on('kuppbj-tender-detail-pengumuman-kirim-html').render('view_kuppbj.tender.detail-pengumuman.kirim')
Route.on('kuppbj-tender-detail-pengumuman-sent-html').render('view_kuppbj.tender.detail-pengumuman.sent')

Route.on('kuppbj-tender-detail-pengumuman-html').render('view_kuppbj.tender.detail-pengumuman.index')
Route.on('kuppbj-tender-detail-pengumuman-tender-gagal-html').render('view_kuppbj.tender.detail-pengumuman.tender-gagal')
Route.on('kuppbj-tender-detail-pengumuman-konfirmasi-tender-gagal-html').render('view_kuppbj.tender.detail-pengumuman.konfirmasi-tender-gagal')
Route.on('kuppbj-tender-detail-pengumuman-konfirmasi-tender-ulang-html').render('view_kuppbj.tender.detail-pengumuman.konfirmasi-tender-ulang')
Route.on('kuppbj-tender-detail-pengumuman-konfirmasi-evaluasi-ulang-html').render('view_kuppbj.tender.detail-pengumuman.konfirmasi-evaluasi-ulang')
Route.on('kuppbj-tender-detail-pengumuman-konfirmasi-tender-batal-html').render('view_kuppbj.tender.detail-pengumuman.konfirmasi-tender-batal')

Route.on('kuppbj-tender-detail-pengumuman-tender-ulang-html').render('view_kuppbj.tender.detail-pengumuman.tender-ulang')
Route.on('kuppbj-tender-detail-pengumuman-tender-batal-html').render('view_kuppbj.tender.detail-pengumuman.tender-batal')

Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.index')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-tender-gagal-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.tender-gagal')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-gagal-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-gagal')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-ulang-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-ulang')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-konfirmasi-evaluasi-ulang-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.konfirmasi-evaluasi-ulang')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-konfirmasi-tender-batal-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.konfirmasi-tender-batal')

Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-tender-ulang-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.tender-ulang')
Route.on('kuppbj-tender-detail-pengumuman-pascakualifikasi-tender-batal-html').render('view_kuppbj.tender.detail-pengumuman-pascakualifikasi.tender-batal')

Route.on('kuppbj-pokja-pemilihan-html').render('view_kuppbj.pokja-pemilihan.index')
Route.on('kuppbj-pokja-pemilihan-form-html').render('view_kuppbj.pokja-pemilihan.form')
Route.on('kuppbj-ukpbj-html').render('view_kuppbj.ukpbj.index')
Route.on('kuppbj-ukpbj-form-html').render('view_kuppbj.ukpbj.form')

// General
/*
Route.on('general-').render('view_general.')
*/
Route.on('general-konfirmasi-hapus-dokumen-html').render('view_general.konfirmasi-hapus-dokumen')
Route.on('general-ganti-password-html').render('view_general.ganti-password')
Route.on('general-log-akses-html').render('view_general.log-akses')

// PP
/*
Route.on('pp-').render('view_pp.')
*/
// PPK
/*
Route.on('ppk-').render('view_ppk.')
*/
// Verifikator
/*
Route.on('verifikator-').render('view_verifikator.')
*/