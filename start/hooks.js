const { hooks } = require('@adonisjs/ignitor')
const { ioc } = require('@adonisjs/fold')


hooks.after.providersBooted(() => {
    const View = use('View');
    const Encryption = use('Encryption')

    const monthName = [
        'January','February','March','April','May','June','July',
        'August','September','October','November','December'
    ]

    View.global('currentTime', function () {
        return new Date().getTime()
    })

    View.global('random',function(n){
        return Math.random(n);
    })

    View.global('getMonthYear', function (date) {
        if(!date) return 'N/A'
        const temp = new Date(date)

        const formatted = `${monthName[temp.getMonth()]} ${temp.getFullYear()}`
        
        return formatted
    })
    
    View.global('formatToInputDate', function (date) {
        const temp = new Date(date)

        const formatted = `${temp.getDate()}/${temp.getMonth()+1}/${temp.getFullYear()}`

        return formatted
    })

    View.global('encrypt',function (string) {
        const enc = encodeURIComponent(
            Encryption.encrypt(string)
        )

        return enc
    })

    View.global('fromTotalFund',function (totalFund, milestoneFund) {
        let percentage = ((milestoneFund/totalFund)*100).toFixed(2)        

        return percentage
    })

    View.global('receiptFilename',function (payment_id, path) {
        if(!payment_id || !path) return false 
        
        let filename = path.split(`/${payment_id}/`)
        filename = filename[1]

        return filename
    })

    View.global('remaining',function ( target ) {
        let remaining = new Date(target) - new Date()
        remaining = Math.round(remaining/1000/60/60/24)
        
        if(remaining < 0) remaining = '0'

        return remaining
    })

    View.global('convertToMoney',function( plainNumber ){
        var money = '';
        var numberRev = plainNumber.toString().split('').reverse().join('');
        for(var i = 0; i < numberRev.length; i++) if(i%3 == 0) money += numberRev.substr(i,3)+'.';
        return money.split('',money.length-1).reverse().join('');
    })

    View.global('parseInt',function(theString){
        return parseInt(theString);
    })

    View.global('getPercentage',function(portion,total){
        return Math.round(portion*10000/total)/100;
    })

    View.global('getRelativeTime',function(date_str) {
        if (!date_str) {
            return 'at unspecific time';
        }
        else{
            date_str = date_str.trim();
            date_str = date_str.replace(/\.\d\d\d+/,""); // remove the milliseconds
            date_str = date_str.replace(/-/,"/").replace(/-/,"/"); //substitute - with /
            date_str = date_str.replace(/T/," ").replace(/Z/," UTC"); //remove T and substitute Z with UTC
            date_str = date_str.replace(/([\+\-]\d\d)\:?(\d\d)/," $1$2"); // +08:00 -> +0800
            var parsed_date = new Date(date_str);
            var relative_to = (arguments.length > 1) ? arguments[1] : new Date(); //defines relative to what ..default is now
            var delta = parseInt((relative_to.getTime()-parsed_date)/1000);
            delta=(delta<2)?2:delta;
            var r = '';
            if (delta < 60) {
            r = delta + ' seconds ago';
            } else if(delta < 120) {
            r = 'a minute ago';
            } else if(delta < (45*60)) {
            r = (parseInt(delta / 60, 10) || 'some').toString() + ' minutes ago';
            } else if(delta < (2*60*60)) {
            r = 'an hour ago';
            } else if(delta < (24*60*60)) {
            r = '' + (parseInt(delta / 3600, 10) || 'some').toString() + ' hours ago';
            } else if(delta < (48*60*60)) {
            r = 'a day ago';
            } else {
            r = (parseInt(delta / 86400, 10) || 'some').toString() + ' days ago';
            }
            return 'about ' + r;
        }
        
    })

    ioc.singleton('test', function (date) {        
        return true           
          
    })


    
})