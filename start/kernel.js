'use strict'

/** @type {import('@adonisjs/framework/src/Server')} */
const Server = use('Server')

/*
|--------------------------------------------------------------------------
| Global Middleware
|--------------------------------------------------------------------------
|
| Global middleware are executed on each http request only when the routes
| match.
|
*/
const globalMiddleware = [
  'Adonis/Middleware/BodyParser',
  'Adonis/Middleware/Session',
  'Adonis/Middleware/Shield',
  'Adonis/Middleware/AuthInit',
  'App/Middleware/ConvertEmptyStringsToNull',
]

/*
|--------------------------------------------------------------------------
| Named Middleware
|--------------------------------------------------------------------------
|
| Named middleware is key/value object to conditionally add middleware on
| specific routes or group of routes.
|
| // define
| {
|   auth: 'Adonis/Middleware/Auth'
| }
|
| // use
| Route.get().middleware('auth')
|
*/
const namedMiddleware = {
  auth: 'Adonis/Middleware/Auth',
  guest: 'Adonis/Middleware/AllowGuestOnly',
  project: 'App/Middleware/Project',
  projectCreation: 'App/Middleware/ProjectCreation',
  product: 'App/Middleware/Product',
  productCreation: 'App/Middleware/ProductCreation',
  user:'App/Middleware/User',
  role:'App/Middleware/MdRole',
  researchField:'App/Middleware/MdResearchField',
  studyArea:'App/Middleware/MdStudyArea',
  permission:'App/Middleware/MdPermission',
  rolePermission:'App/Middleware/MdRolePermission',
  bank:'App/Middleware/MdBank',
  country:'App/Middleware/MdCountry',
  province:'App/Middleware/MdProvince',
  city:'App/Middleware/MdCity',
  district:'App/Middleware/MdDistrict',
  industry:'App/Middleware/MdIndustry',
  industryCategory:'App/Middleware/MdIndustryCategory',
  degree:'App/Middleware/MdDegree',
  userBankAccount:'App/Middleware/UserBankAccount',
  view:'App/Middleware/View',
  milestone:'App/Middleware/Milestone',
  projectMember:'App/Middleware/ProjectMember',
  admin:'App/Middleware/Admin',
  notification:'App/Middleware/Notification'
}

/*
|--------------------------------------------------------------------------
| Server Middleware
|--------------------------------------------------------------------------
|
| Server level middleware are executed even when route for a given URL is
| not registered. Features like `static assets` and `cors` needs better
| control over request lifecycle.
|
*/
const serverMiddleware = [
  'Adonis/Middleware/Static',
  'Adonis/Middleware/Cors'
]

Server
  .registerGlobal(globalMiddleware)
  .registerNamed(namedMiddleware)
  .use(serverMiddleware)
